const withSass = require('@zeit/next-sass');
const withCss = require('@zeit/next-css');
const withImages = require('next-images');
const withFonts = require('next-fonts');

module.exports = withFonts(withImages(withCss(withSass({
    webpack(config, options) {
        return config
    },
    devIndicators: {
        autoPrerender: false,
    },
}))));