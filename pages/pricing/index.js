import React, { Component } from 'react'
import Head from 'next/head'
import ScrollAnimation from 'react-animate-on-scroll';
import { Discount, Pricingfitness } from "../../components/common/index"
import Primarylayout from '../../layout/primarylayout';
class Pricing extends Component {
    render() {
        return (
            <section className="Pricing-page">
                <Head>
                    <title>Pricing</title>
                </Head>
                <Primarylayout>
                    <div className="price-container">
                        <ScrollAnimation animateIn="slideInLeft" duration={1} animateOnce={true}>
                            <h4 className="price-heading">Pricing</h4>
                        </ScrollAnimation>
                    </div>
                    <Discount />
                    <Pricingfitness />
                </Primarylayout>
            </section>
        );
    }
}

export default Pricing;