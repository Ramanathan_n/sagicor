import React, { Component } from 'react'
import Head from 'next/head'
import ScrollAnimation from 'react-animate-on-scroll';
import { Whatweoffer, Featuredproduct, Otherprograms } from "../../components/common/index"
import Primarylayout from '../../layout/primarylayout';
class Features extends Component {
    render() {
        return (
            <div>
                <Head>
                    <title>Features</title>
                </Head>
                <Primarylayout>
                    <div className="feature-container">
                        <ScrollAnimation animateIn="slideInLeft" duration={1} animateOnce={true}>
                            <h4 className="feature-heading">Features</h4>
                        </ScrollAnimation>
                    </div>
                    <Whatweoffer />
                    <Featuredproduct />
                    <Otherprograms />
                </Primarylayout>
            </div>
        );
    }
}

export default Features;