import React from 'react'
import Head from 'next/head'

import { Banner ,Abouthome, Featuredproduct, Otherprograms, Offer ,Testimonial} from "../components/common"
import Primarylayout from '../layout/primarylayout'

const Home = () => (
  <div>

    <Head>
      <title>Home</title>
    </Head>
    <Primarylayout>
      <Banner />
      <Abouthome />
      <Featuredproduct />
      <Otherprograms />
      <Offer />
      <Testimonial />
    </Primarylayout>

  </div>
)

export default Home
