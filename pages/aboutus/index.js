import React, { Component } from "react";
import Head from 'next/head'
import ScrollAnimation from 'react-animate-on-scroll';
import image1 from "../../static/images/image2.png"
import image2 from "../../static/images/productBox.png"
import image3 from "../../static/images/calenderIcon.png"
import Primarylayout from '../../layout/primarylayout'
class Aboutus extends Component {
    render() {
        return (
            <section>
                <Head>
                    <title>Aboutus</title>
                </Head>
                <Primarylayout>
                    <div>
                        <div className="aboutus-container d-flex">
                            <ScrollAnimation animateIn="slideInLeft" duration={1} animateOnce={true}>
                                <h4 className="aboutus-heading">AboutUs</h4>
                            </ScrollAnimation>
                        </div>
                        <div className="row no-gutters">
                            <div className="col-lg-6 p-0">
                                <div className="bg-primarys">
                                    <img src={image1} alt="" />
                                </div>
                            </div>
                            <div className="col-lg-6 p-0">
                                <div className="aboutus-sub-menu">
                                    <a className="extra"></a>
                                    <h4 className="aboutus-sub-heading py-2">ABOUT<span className="Text-primary pl-1">SAGICOR 365</span></h4>
                                    <div className="d-flex flex-wrap">
                                        <ScrollAnimation animateIn="slideInRight" duration={1.5} animateOnce={true}>
                                            <div className="card aboutus-card my-sm-2 my-xs-2">
                                                <div className="aboutus-body d-flex flex-row">
                                                    <div><img className="aboutus-image-list" src={image2} alt="" /></div>
                                                    <div><p className="aboutus-text mb-0">Sagicor 365 is a partnership between Express Fitness and Sagicor to offer Sagicor customers the option to become members of Express Fitness gyms island-wide</p></div>
                                                </div>
                                            </div>
                                            <div className="card aboutus-card my-sm-2 my-xs-2">
                                                <div className="aboutus-body d-flex flex-row">
                                                    <div className="aboutus-body d-flex flex-row">
                                                        <div><img className="aboutus-image-list" src={image3} alt="" /></div>
                                                        <div><p className="aboutus-text mb-0 pl-4">Discounted price for a wide range of services and products.</p></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card aboutus-card my-sm-2 my-xs-2">
                                                <div className="aboutus-body d-flex flex-row">
                                                    <div className="aboutus-body d-flex flex-row">
                                                        <div><img className="aboutus-image-list" src={image2} alt="" /></div>
                                                        <div><p className="aboutus-text mb-0">In addition to discounted fees, customers would be able to benefit from various specialized health and fitness products which Express may offer periodically. </p></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card aboutus-card my-sm-2 my-xs-2">
                                                <div className="aboutus-body d-flex flex-row">
                                                    <div className="aboutus-body d-flex flex-row">
                                                        <div><img className="aboutus-image-list" src={image3} alt="" /></div>
                                                        <div><p className="aboutus-text mb-0 pl-4">The partnership aims to redefine health and fitness in Jamaica with offerings including discounted rates, exclusive facilities & events, and health company premium rebates.</p></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ScrollAnimation>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Primarylayout>
            </section>
        )
    }
}

export default Aboutus
