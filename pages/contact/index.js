import React, { Component } from 'react'
import Head from 'next/head'
import ScrollAnimation from 'react-animate-on-scroll';
import { Getintouch, Message } from "../../components/common/index"
import Primarylayout from '../../layout/primarylayout';
class Contact extends Component {
    render() {
        return (
            <div>
                <Head>
                    <title>Contact</title>
                </Head>
                <Primarylayout>
                    <div className="contact-container">
                        <ScrollAnimation animateIn="slideInLeft" duration={1} animateOnce={true}>
                            <h4 className="contact-heading">Contact</h4>
                        </ScrollAnimation>
                    </div>
                    <Getintouch />
                    <Message />
                </Primarylayout>
            </div>
        );
    }
}

export default Contact;