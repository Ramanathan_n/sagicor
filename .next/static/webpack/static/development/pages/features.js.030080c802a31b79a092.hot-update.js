webpackHotUpdate("static\\development\\pages\\features.js",{

/***/ "./components/common/abouthome/abouthome.js":
/*!**************************************************!*\
  !*** ./components/common/abouthome/abouthome.js ***!
  \**************************************************/
/*! exports provided: Abouthome */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Abouthome", function() { return Abouthome; });
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _abouthome_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./abouthome.scss */ "./components/common/abouthome/abouthome.scss");
/* harmony import */ var _abouthome_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_abouthome_scss__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-animate-on-scroll */ "./node_modules/react-animate-on-scroll/dist/scrollAnimation.min.js");
/* harmony import */ var react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _static_images_image3_png__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../static/images/image3.png */ "./static/images/image3.png");
/* harmony import */ var _static_images_image3_png__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_static_images_image3_png__WEBPACK_IMPORTED_MODULE_8__);





var __jsx = react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement;




var Abouthome =
/*#__PURE__*/
function (_Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(Abouthome, _Component);

  function Abouthome() {
    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Abouthome);

    return Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(Abouthome).apply(this, arguments));
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Abouthome, [{
    key: "render",
    value: function render() {
      return __jsx("section", {
        className: "about"
      }, __jsx("div", {
        className: "my-5 py-3 about-container"
      }, __jsx("div", {
        className: "container"
      }, __jsx("div", {
        className: "row"
      }, __jsx("div", {
        className: "col-md-7"
      }, __jsx(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_7___default.a, {
        animateIn: "slideInLeft",
        duration: 1,
        animateOnce: true
      }, __jsx("h3", {
        className: "about-text"
      }, "About", __jsx("span", {
        className: "Text-primary pl-2"
      }, "Sagicor 365")), __jsx("p", {
        className: "about-para"
      }, "Stay healthier with Jamaica\u2019s leader in fitness"), __jsx("p", null, "Sagicor 365 is a partnership between Express Fitness and Sagicor to offer Sagicor customers the option to become members of Express Fitness gyms island-wide, for a discounted price. In addition to discounted fees, customers would be able to benefit from various specialized health and fitness products which Express may offer periodically."), __jsx("p", {
        className: "py-3 mb-0"
      }, __jsx("button", {
        className: "btn btn-blue"
      }, "Know More")))), __jsx("div", {
        className: "col-md-4 offset-md-1"
      }, __jsx(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_7___default.a, {
        animateIn: "zoomIn",
        duration: 1,
        animateOnce: true
      }, __jsx("img", {
        className: "about-pic",
        src: _static_images_image3_png__WEBPACK_IMPORTED_MODULE_8___default.a,
        alt: ""
      })))))));
    }
  }]);

  return Abouthome;
}(react__WEBPACK_IMPORTED_MODULE_5__["Component"]);

/***/ }),

/***/ "./static/images/image3.png":
/*!**********************************!*\
  !*** ./static/images/image3.png ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/image3-37cbbf0cee8b29b825fd40a4089963f0.png";

/***/ }),

/***/ "./static/images/image8.png":
false

})
//# sourceMappingURL=features.js.030080c802a31b79a092.hot-update.js.map