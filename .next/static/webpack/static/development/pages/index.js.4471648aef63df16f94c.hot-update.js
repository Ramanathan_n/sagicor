webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/common/featuredProduct/featuredProduct.js":
/*!**************************************************************!*\
  !*** ./components/common/featuredProduct/featuredProduct.js ***!
  \**************************************************************/
/*! exports provided: Featuredproduct */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Featuredproduct", function() { return Featuredproduct; });
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _FeauturedProduct_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./FeauturedProduct.scss */ "./components/common/featuredProduct/FeauturedProduct.scss");
/* harmony import */ var _FeauturedProduct_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_FeauturedProduct_scss__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _static_images_Rectangle4_png__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../static/images/Rectangle4.png */ "./static/images/Rectangle4.png");
/* harmony import */ var _static_images_Rectangle4_png__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_static_images_Rectangle4_png__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _static_images_Rectangle5_png__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../static/images/Rectangle5.png */ "./static/images/Rectangle5.png");
/* harmony import */ var _static_images_Rectangle5_png__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_static_images_Rectangle5_png__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _static_images_Rectangle6_png__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../static/images/Rectangle6.png */ "./static/images/Rectangle6.png");
/* harmony import */ var _static_images_Rectangle6_png__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_static_images_Rectangle6_png__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _static_images_Rectangle7_png__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../static/images/Rectangle7.png */ "./static/images/Rectangle7.png");
/* harmony import */ var _static_images_Rectangle7_png__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_static_images_Rectangle7_png__WEBPACK_IMPORTED_MODULE_10__);





var __jsx = react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement;






var Featuredproduct =
/*#__PURE__*/
function (_Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(Featuredproduct, _Component);

  function Featuredproduct() {
    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Featuredproduct);

    return Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(Featuredproduct).apply(this, arguments));
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Featuredproduct, [{
    key: "render",
    value: function render() {
      return __jsx("section", {
        className: "featured-products"
      }, __jsx("div", {
        className: "mb-5"
      }, __jsx("h3", {
        className: "text-center featured-products-heading py-4"
      }, __jsx("span", {
        className: "Text-primary"
      }, "Featured"), " Programs"), __jsx("div", {
        className: "card-group"
      }, __jsx("div", {
        className: "card card-product"
      }, __jsx("div", {
        className: "position-relative"
      }, __jsx("img", {
        className: "card-img-top",
        src: _static_images_Rectangle4_png__WEBPACK_IMPORTED_MODULE_7___default.a,
        alt: ""
      })), __jsx("p", {
        className: "fitness-catagory text-center"
      }, "Express Fitness"), __jsx("p", {
        className: " button-transparent"
      }, __jsx("button", {
        className: "btn btn-transparents"
      }, "Join Now"))), __jsx("div", {
        className: "card card-product"
      }, __jsx("div", {
        className: "position-relative"
      }, __jsx("img", {
        className: "card-img-top",
        src: _static_images_Rectangle5_png__WEBPACK_IMPORTED_MODULE_8___default.a,
        alt: ""
      })), __jsx("p", {
        className: "fitness-catagory text-center"
      }, "365 fitness"), __jsx("p", {
        className: " button-transparent"
      }, __jsx("button", {
        className: "btn btn-transparents"
      }, "Join Now"))), __jsx("div", {
        className: "card card-product"
      }, __jsx("div", {
        className: "position-relative"
      }, __jsx("img", {
        className: "card-img-top",
        src: _static_images_Rectangle6_png__WEBPACK_IMPORTED_MODULE_9___default.a,
        alt: ""
      })), __jsx("p", {
        className: "fitness-catagory text-center"
      }, "Fitplex"), __jsx("p", {
        className: " button-transparent"
      }, __jsx("button", {
        className: "btn btn-transparents"
      }, "Join Now"))), __jsx("div", {
        className: "card card-product"
      }, __jsx("div", {
        className: "position-relative"
      }, __jsx("img", {
        className: "card-img-top",
        src: _static_images_Rectangle7_png__WEBPACK_IMPORTED_MODULE_10___default.a,
        alt: ""
      })), __jsx("p", {
        className: "fitness-catagory text-center"
      }, "Fitbox"), __jsx("p", {
        className: "button-transparent"
      }, __jsx("button", {
        className: "btn btn-transparents"
      }, "Join Now"))))));
    }
  }]);

  return Featuredproduct;
}(react__WEBPACK_IMPORTED_MODULE_5__["Component"]);

/***/ }),

/***/ "./static/images/Rectangle 11.png":
false,

/***/ "./static/images/Rectangle 12.png":
false,

/***/ "./static/images/Rectangle 13.png":
false,

/***/ "./static/images/Rectangle10.png":
false,

/***/ "./static/images/Rectangle4.png":
/*!**************************************!*\
  !*** ./static/images/Rectangle4.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/Rectangle4-69c667d777723cf1ef52a77beca15ce0.png";

/***/ }),

/***/ "./static/images/Rectangle5.png":
/*!**************************************!*\
  !*** ./static/images/Rectangle5.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/Rectangle5-3535a33bca6059a4c57db225782f882d.png";

/***/ }),

/***/ "./static/images/Rectangle6.png":
/*!**************************************!*\
  !*** ./static/images/Rectangle6.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/Rectangle6-82319746845e31755224662c0e9e7c2b.png";

/***/ }),

/***/ "./static/images/Rectangle7.png":
/*!**************************************!*\
  !*** ./static/images/Rectangle7.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/Rectangle7-00f05d1ef7c1bc8c5720fc4e7acfb21f.png";

/***/ })

})
//# sourceMappingURL=index.js.4471648aef63df16f94c.hot-update.js.map