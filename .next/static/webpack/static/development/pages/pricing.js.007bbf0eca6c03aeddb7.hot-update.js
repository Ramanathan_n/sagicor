webpackHotUpdate("static\\development\\pages\\pricing.js",{

/***/ "./components/common/getintouch/getintouch.js":
/*!****************************************************!*\
  !*** ./components/common/getintouch/getintouch.js ***!
  \****************************************************/
/*! exports provided: Getintouch */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Getintouch", function() { return Getintouch; });
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _getintouch_scss__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./getintouch.scss */ "./components/common/getintouch/getintouch.scss");
/* harmony import */ var _getintouch_scss__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_getintouch_scss__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var google_map_react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! google-map-react */ "./node_modules/google-map-react/lib/index.js");
/* harmony import */ var google_map_react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(google_map_react__WEBPACK_IMPORTED_MODULE_8__);






var __jsx = react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement;



var Getintouch =
/*#__PURE__*/
function (_Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(Getintouch, _Component);

  function Getintouch() {
    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Getintouch);

    return Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(Getintouch).apply(this, arguments));
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Getintouch, [{
    key: "render",
    value: function render() {
      return __jsx("div", {
        className: "getintouch-container"
      }, __jsx("div", {
        className: "container"
      }, __jsx("h4", {
        className: "getintouch-heading"
      }, "Get in Touch"), __jsx("div", {
        className: "row"
      }, __jsx("div", {
        className: "col-md-5 p-3"
      }, __jsx("div", {
        className: "d-flex flex-row py-3"
      }, __jsx("div", {
        className: "pr-4 t-icons"
      }, __jsx("i", {
        className: "fas fa-map-marker-alt"
      })), __jsx("div", {
        className: "flex-column"
      }, __jsx("p", null, "Location"), __jsx("p", {
        className: "getintouch-para"
      }, "24, Park Streeet, Austin, Texas, Dallas"))), __jsx("div", {
        className: "d-flex flex-row py-3"
      }, __jsx("div", {
        className: "pr-3 t-icons"
      }, __jsx("i", {
        className: "fas fa-envelope"
      })), __jsx("div", {
        className: "flex-column"
      }, __jsx("p", null, "Have any Questions?"), __jsx("p", {
        className: "getintouch-para"
      }, "info@profinch.com"))), __jsx("div", {
        className: "d-flex flex-row py-3"
      }, __jsx("div", {
        className: "pr-3 t-icons"
      }, __jsx("i", {
        className: "fas fa-phone-alt"
      })), __jsx("div", {
        className: "flex-column"
      }, __jsx("p", null, "Call Us"), __jsx("p", {
        className: "getintouch-para"
      }, "080 4256 4256")))), __jsx("div", {
        className: "col-md-6 col-sm-12"
      }, __jsx(google_map_react__WEBPACK_IMPORTED_MODULE_8___default.a, {
        bootstrapURLKeys: {
          key: "AIzaSyAcaQwH9WxvTZgDwXCKAT73mjbRrg0Gj7w"
        },
        defaultCenter: this.props.center,
        defaultZoom: this.props.zoom
      })))));
    }
  }]);

  return Getintouch;
}(react__WEBPACK_IMPORTED_MODULE_6__["Component"]);

Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_5__["default"])(Getintouch, "defaultProps", {
  center: {
    lat: 13.0619,
    lng: 80.2468
  },
  zoom: 15
});

/***/ })

})
//# sourceMappingURL=pricing.js.007bbf0eca6c03aeddb7.hot-update.js.map