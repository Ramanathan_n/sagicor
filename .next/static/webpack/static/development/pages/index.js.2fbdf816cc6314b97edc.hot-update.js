webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/common/header/header.js":
/*!********************************************!*\
  !*** ./components/common/header/header.js ***!
  \********************************************/
/*! exports provided: Header */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Header", function() { return Header; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _header_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./header.scss */ "./components/common/header/header.scss");
/* harmony import */ var _header_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_header_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _static_images_logo2_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../static/images/logo2.png */ "./static/images/logo2.png");
/* harmony import */ var _static_images_logo2_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_static_images_logo2_png__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _static_images_logo1_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../static/images/logo1.png */ "./static/images/logo1.png");
/* harmony import */ var _static_images_logo1_png__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_static_images_logo1_png__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






var Header = function Header() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      isOpen = _useState[0],
      setIsOpen = _useState[1];

  var toggle = function toggle() {
    return setIsOpen(!isOpen);
  };

  return __jsx("header", null, __jsx("div", {
    className: "head"
  }, __jsx("div", {
    className: "container py-0 px-0"
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Navbar"], {
    light: true,
    expand: "lg"
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/",
    prefetch: false
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavbarBrand"], {
    className: "m-0 nav-brand"
  }, __jsx("img", {
    src: _static_images_logo1_png__WEBPACK_IMPORTED_MODULE_4___default.a,
    width: "140px",
    height: "45px"
  }), __jsx("img", {
    src: _static_images_logo2_png__WEBPACK_IMPORTED_MODULE_3___default.a,
    width: "50px",
    height: "45px"
  }))), __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavbarToggler"], {
    onClick: toggle
  }), __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Collapse"], {
    isOpen: isOpen,
    navbar: true
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Nav"], {
    className: "ml-auto",
    navbar: true
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavItem"], {
    className: "nav-item active margin"
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/aboutus",
    prefetch: false
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavLink"], {
    className: "nav-link"
  }, "About Us"))), __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavItem"], {
    className: "nav-item"
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/features",
    prefetch: false
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavLink"], {
    className: "nav-link"
  }, "Features"))), __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavItem"], {
    className: "nav-item"
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/pricing",
    prefetch: false
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavLink"], {
    className: "nav-link"
  }, "Pricing"))), __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavItem"], {
    className: "nav-item"
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/contact",
    prefetch: false
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavLink"], {
    className: "nav-link"
  }, "Contact")))), __jsx("div", {
    className: "header-button"
  }, __jsx("button", {
    className: "btn btn-blue ml-3"
  }, "Log In")))))));
};

/***/ }),

/***/ "./static/images/image10.png":
false,

/***/ "./static/images/image9.png":
false,

/***/ "./static/images/logo1.png":
/*!*********************************!*\
  !*** ./static/images/logo1.png ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/logo1-64e09c0e3e1261a673a3ea516fa6f112.png";

/***/ }),

/***/ "./static/images/logo2.png":
/*!*********************************!*\
  !*** ./static/images/logo2.png ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADkAAAAyCAYAAADm33NGAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAA3vSURBVHgB5VoJVFTX+b/3vTc7s7DMDDuiiIpE/ggapREEtTmxLjEqMS5Ne0yinhib2JiYf5s67Wkbu2ljmpjFGBO1GtSImhKjBjVVYimg4LBKBGQZYGCYGWZ98967vW+QBAwwg6jxtD/Pk8fMu9+9v/d999suAPwPAILvEQgBmF+3K7rGsE8uFgaE2FxNwMWYAcO4AKAoIKZUIEAcCViXqWtC+AqHgog2JscuMoNh4nshyZM7Urkpzm6vf9rONMywuTujIKAjIOp9ggPo5vIgJPE91aKSRNuEUFkUpkjOFRjQ0cxMHePvfPec5LGyl9NbHBfXdjsbF3HILuY4FlCkDJCElMbErBCwCFOUAY6WelgnEJASPIoFHGLxy+G8Kw4NmP7FhPAljz8Y+WSnP3NS4B7h2rU8URVz8dma9pwXacahxdoiKELMBgeMvaqQxORGSTMqSo3vFgg5FxoTvmaixVU1rctRn9VmLUkAgNEQWKMAUpgoAgZbYYbMFLMAi/3An7n7kSy58XF6WevH6WGy1NNyi7gsLW2jE9wBfFnxTlip69TLXxuPbICAgxTWhpAKtAdLk7ZEhM38MDNiTQcAe/qMWNuC/zutb8/ZXtsRkd5hq9rU0X11JolJQggBwXGUzV2fiAnjX7818sFA9P3Fg1zTW6wFv75uzf3HdWHJztOVv3kgJ2cpCUYIE1u9sbb9yAYC4kVhe0OQMo8K+tHGFSnPvN5DcGAkarJtjya8maegorMDJNEnWeT2EkJ4z8oEgXObu6uC/Jm/3578qHjRhDZL4QUEmCCGcwORINgtpaIvRijT3hqtTDqbGJVtAsPE6aqtC0tad+6EHB0GIeGdMFyZvmv55ANPD0dOYcPh0VdbPzjRYb+cQGDdBIijmmPE85PnTv5/o6+x/TQZLksTCAUqgt/gAlIKGNYu6nZfm1nbeXRfYevuw7n6nz+Vo38hiPeOwA8gpBca7EWvIs4VxpsZws6DIEQGbUDKXjBMTIle3DAqcPZvCSDE0zPA6ekECqlW4M/YfiSTgn9SpxSNaurhgG2ff/OQI1jOLG6zFmZWt+9/u9XyzzP7i7PX6BuPxfkSfrb2i0cM1oIUAvRYPDY3ECJN1MdL0vRgmMAvidXSc45o5UlXEPa2+JWFSKWBC/wZ24+kWq3uBqRwG0GK0K2qwp6QJ0263E3JeN/u/Gfj1qtHy9a/kVe+OXkgwQWN2yQ3rPkZLA4D8KYwAgoBg7izUVFpwzZ7HomJiXSAdNxfOATsHs4Br1tO+WVRxK0fTNWsuxAoGqNnkGfAAd59BVlgdV0XV3ccWF/RkZP/zldZb+SUrs8qqt8b1vucnJgY4qK75pCEsO9oIJeEC8EIkBz+yEWFKLaNZRlgMOtvj+SESPN1uXjM297AOyh4UyYBCUWAY20qs1O/vsWaf6Ki85MD56rfmoGQjrC7LeNZzj3RG9/6jAPcUHJ9Y5RqZoNMHP0JSUpgYIDPHePFd5IBCLPZS9d27HN6OrJauwsfpQgKr5IBgyVHkI9d+KeH6ZI2mc9mGG1l+RVGTbFUUnHezXT2G4SwdXgYRyCObyK8x9zgNsDHxdLWj3Rt5rSPGZK62j++DjJmsC92FbwQ5EQX8+3uhiQS+h8q0c1IhqDAg0cJEPg2xeQdj1qeejk5YsOiyeE/agD3CMRgXzyVtt0UH/TYqypxwtcszi/9TXN5J+PNSgDTjyAP3rxNtoqEBtPJ+eAeghjqy4cnbD4xTvvEWoVkbLuHc4E7AQ7RIoO5cOVJ/Ta/spU7gQHV89ei5ujGbrl0HpDXZmZCpqnr8v/llq9+xepueYyCBAVHWLtwOM6pZVMKNOLEVfMmbb0O7jIG1KTHLY+oMDhzdnYaf6jTISIyMPmKMijh+digh/fhGOrk0Mg8JJ+/dtiK0prsF177vPb1FD7RBncRAwp/6ZhR/nmrZ4/RjjKXTQz4sNGiePFQNmQbGwsklzp2Pdbhqnnb7KgMEBAi4LMEGAIc9rYigaZNKZmwXatM3zl37AYruAsYkCR+s8T2fzue+t1503YLB0QpYYJ8gqI2FDwRXM278JyiZ6JtntZnTe6K5xiuW0J4a4teDzwc2tD7OINcIFiWWh8dkLKlnaWP/Djpz3ZwBzGomRQ0Ismazzreq+ryrOCXrRKi9unhog+iZODDt+aFVPLPHLv8yg/anJefxwVuOoAeNZ+49+xXzp8p+gEhBhCkhAuSTKzQSMYfJKDiaOFhUKXT6Ua2N3yt4NnPu9IP6B0HTBwK57evFC8lRkVemqIlfz9aHnJSh53S8aJnpGZom2GnG1+1uxun0Ww3KcR57rf6HJ5B8yZMQjHAHr1UIRz9DkNL9q+c9saIzNjna/5pXse6I5X0Diu6mR3h96rAljkhhCx8QC16f/poz4HV49XdRUXPCEyKmIkGyxWdlb42zeE2avkMhyRur8PCeXNniDt22jqpKHSXRpm8Z+H4P7aA24BPkjvyronea1K8XtPFrHETsGcEVg7EpYBKRDApIWSRUiHaES+Fp16breg8q38zwMgWR9Cs50mcxD/icLcmsMAu5HNW4jYI8z0d7M1puXh0daTyoX0mo3HHTzP3DCto+7VhjlXZw3Mq3YcO1TjT6F6i3hXgW7wIpZBwh0qJKwnB1KEHQ4R5L6UHVAFwiMitr5CTDkuGy2NdbHbWpVtctaE4qRDxaaK3Yh1GwGWxZgVkACLJwHdT1Cv/OGPcRr/jq9+zfKh3RO8vdXyQ3+TOYsgBhmHNKnCHaoyKbNNIiHfHq7jDCWpN5ZpU6EEohyxtAqGt3RWzjK7y+Q7aOM3mvhHpYWzevmpPpcLLZH2sggAsR4MQecoVlfLBxUvG6fwiOqwgfLzaGvL5dfZdTHiRDcCBR/N+hkVAKyG4+CDy6zGB5EERYvcuS1Q3ZsZCr5np9Xphl6wwua4zb46DMcxx0J0PMLg64YALByISAB8aZjgGBErG3UgN+1l2auySf4E7SZLH+VpH1JZLjt8XtriXuLBf4AZbEMd3AgCvXUYugnWRcqogM0L4pckDP+20mLuXgkgagGwgS5io7qDLY1XCiEfbbeULLK5roQxnV/HNKr6MG9g7Q75jx4Yq0r9cNfngLF9tydtKp5YuzSGVT85anVvjeq2DBkGA8iEG9VxSErBaKWEKFMOSh0LJkikxksOrJkgvg5uLvGB8X25oKZ0KOPdzLbav5jrcjQK+7TKQZnExh02XYcepfzLrsUl/Og/uNMlerPrUlNhmYd8/d8M9laagTzP7BticKUwrREY4Y+Rkk0QE/05C+I8YCaravRD3mTD+VftRVr31i5/VW07NAoiTEQNold+f4aofVKVFPT8/TpNZO9h0I06Md1zqVBS2gl/mN7qy250oiiEIwu/BN9fM9xUlAsIcLEI34gKpkwlKcv+YuMDq5+LOsZ+UHXu0zVa4w0Y3h0Hg+Y4ACCX2cZrs1fMTtn482DT+L2gQbJgWbN27MOjlqVrh4tlRwjMy/BlkEM5a8OUr2+npfAIPvqweTlXXjSZdaPZsOt1A59bVm5ZnZxvR4qS/Hh4buHgzLs+ciGO+I4DhbLIWS4nW1zR3DGcRok7mW+dcNLg3XDcxWQYHEiISDn8WHI6kJHTGKKhtjz+l1ukgZA6Xrdta13niJfxlP2kMLuYjFRnnx3g2PZKWljbg2c2INdkXmXgxf5il/Gx2hP2J5YmiVXNjhWfDKKJV5E1bgP9pLHbLDg5IGruZFxuOd63lP0pSL90rl4yp5I/w+oJPKOx0G447hwYXB+4CdJmx5j9nBee8kgQWToki58+JFv42TgHKcc5LQ+x0er3tkMD6sjFA9JWB/kXuVUdUUyisDhZP3M3eShJToPG5TRP+NxjuCslePIQT9+NL1EWfPq7+lRKiqfPipc9nRVIlo0SA9e5XzgdTvLoqExtabmHWnf/1TE6tmFQmxAe2CPUdh/c+wWdMkUOJuTcoXhPh+Ps81c4zyzWpK5OIsSvGS14aH0yd0VLQQfJkezV8K/CePlrjjOBvxwUvxHFT5o2RveBbMTKhGlOMGnTue3bS3Iub2Ukdvv607Jhxv10GkyQCcnVJG5vRZEEhbhJTIPr4Fvy4GTtVHb7dAES4kiFvkYczDJa9MH36Czhl3DjgnPecZF8cXKjm60P++mznZeeoCqNz7YUGZtXlTk/4N1kUfiWhEhKsxLdu1ghuDSMEFDAqSfTXQ6V298xcfWFdsqT+jR8GbX58PDV9YZwgT8g3fvhgj/3M7Fhhy5YtAFV1nadp1u7t9vHgzzulgvAbkFBdHEr2fUOyF5tnBN14Ol69Mj2KukjSHIoPIZsTtPJtxcXFlMVel4obZ9/UoSyigUaeeoIKeqhhKJn3HUke8ybBrvhA8qO4ANKZESV+PXssNLbIzykN9oJZBD5q6OnycQAX0I6Y4JlvLYhY4BhK3ve6J4fCTE3wMYLsLPzbw6qqHFx0u8ryVpidNTMpPlHHBDnIeRJDl7/3YOSyGl+y7luS2anQgn/wF+D0Z0Z12Mo2EYjj/8YF9w88HoUo7s2YoEmv+iPrviXJg9dgUGVdVnHrm1sBoiPwaT6Oi4xHLNDuC+iW/zJRk+1XE/q+3JM8+L8cIcvP/fyqcU8OAM7JfP8H4iMTrWLartGStPU/fviU3132+1aTpYbyzHrTuVdoj0nFtzIDpfFlCirutWWp7x8Ew8R9S1JJaUo10sm73cLmbJU0MSdanbE77x19Jfhvgw7piObm41IwQvwHcy3zX+V6V60AAAAASUVORK5CYII="

/***/ })

})
//# sourceMappingURL=index.js.2fbdf816cc6314b97edc.hot-update.js.map