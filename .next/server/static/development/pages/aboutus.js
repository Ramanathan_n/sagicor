module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/scss/main.scss":
/*!*******************************!*\
  !*** ./assets/scss/main.scss ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/abouthome/abouthome.js":
/*!**************************************************!*\
  !*** ./components/common/abouthome/abouthome.js ***!
  \**************************************************/
/*! exports provided: Abouthome */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Abouthome", function() { return Abouthome; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _abouthome_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./abouthome.scss */ "./components/common/abouthome/abouthome.scss");
/* harmony import */ var _abouthome_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_abouthome_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-animate-on-scroll */ "react-animate-on-scroll");
/* harmony import */ var react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _static_images_image3_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../static/images/image3.png */ "./static/images/image3.png");
/* harmony import */ var _static_images_image3_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_static_images_image3_png__WEBPACK_IMPORTED_MODULE_3__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




class Abouthome extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return __jsx("section", {
      className: "about"
    }, __jsx("div", {
      className: "my-5 py-3 about-container"
    }, __jsx("div", {
      className: "container"
    }, __jsx("div", {
      className: "row"
    }, __jsx("div", {
      className: "col-md-7"
    }, __jsx(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2___default.a, {
      animateIn: "slideInLeft",
      duration: 1,
      animateOnce: true
    }, __jsx("h3", {
      className: "about-text"
    }, "About", __jsx("span", {
      className: "Text-primary pl-2"
    }, "Sagicor 365")), __jsx("p", {
      className: "about-para"
    }, "Stay healthier with Jamaica\u2019s leader in fitness"), __jsx("p", null, "Sagicor 365 is a partnership between Express Fitness and Sagicor to offer Sagicor customers the option to become members of Express Fitness gyms island-wide, for a discounted price. In addition to discounted fees, customers would be able to benefit from various specialized health and fitness products which Express may offer periodically."), __jsx("p", {
      className: "py-3 mb-0"
    }, __jsx("button", {
      className: "btn btn-blue"
    }, "Know More")))), __jsx("div", {
      className: "col-md-4 offset-md-1"
    }, __jsx(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2___default.a, {
      animateIn: "zoomIn",
      duration: 1,
      animateOnce: true
    }, __jsx("img", {
      className: "about-pic",
      src: _static_images_image3_png__WEBPACK_IMPORTED_MODULE_3___default.a,
      alt: ""
    })))))));
  }

}

/***/ }),

/***/ "./components/common/abouthome/abouthome.scss":
/*!****************************************************!*\
  !*** ./components/common/abouthome/abouthome.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/banner/banner.js":
/*!********************************************!*\
  !*** ./components/common/banner/banner.js ***!
  \********************************************/
/*! exports provided: Banner */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Banner", function() { return Banner; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _banner_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./banner.scss */ "./components/common/banner/banner.scss");
/* harmony import */ var _banner_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_banner_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-id-swiper */ "react-id-swiper");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var swiper_css_swiper_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! swiper/css/swiper.css */ "./node_modules/swiper/css/swiper.css");
/* harmony import */ var swiper_css_swiper_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(swiper_css_swiper_css__WEBPACK_IMPORTED_MODULE_3__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const params = {
  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable: true
  },
  spaceBetween: 0
};
class Banner extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return __jsx("section", {
      className: "banner"
    }, __jsx("div", {
      className: "banner-card"
    }, __jsx(react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default.a, params, __jsx("div", null, __jsx("div", {
      className: "banner-list d-flex zoom-in"
    }, __jsx("div", {
      className: "bannerText align-items-center slide-in-left"
    }, __jsx("h1", {
      className: ""
    }, "Jamaica\u2019s Largest Health and Wellness Platform "), __jsx("p", {
      className: "py-2 "
    }, " Lorem ipsum dolor sit amet, consectetur adipiscing sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."), __jsx("p", {
      className: "mb-0 py-3"
    }, __jsx("button", {
      className: "btn btn-white"
    }, "Join Now"))))), __jsx("div", null, __jsx("div", {
      className: "banner-list d-flex zoom-in"
    }, __jsx("div", {
      className: "bannerText mx-auto slide-in-down"
    }, __jsx("h1", null, "Jamaica\u2019s Largest Health and Wellness Platform "), __jsx("p", {
      className: "py-2"
    }, " Lorem ipsum dolor sit amet, consectetur adipiscing sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."), __jsx("p", {
      className: "py-3 mb-0"
    }, __jsx("button", {
      className: "btn btn-white"
    }, "Join Now"))))), __jsx("div", null, __jsx("div", {
      className: "banner-list d-flex zoom-in"
    }, __jsx("div", {
      className: "bannerText mx-auto slide-in-right"
    }, __jsx("h1", null, "Jamaica\u2019s Largest Health and Wellness Platform "), __jsx("p", {
      className: "py-2"
    }, " Lorem ipsum dolor sit amet, consectetur adipiscing sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."), __jsx("p", {
      className: "py-3 mb-0"
    }, __jsx("button", {
      className: "btn btn-white"
    }, "Join Now"))))))));
  }

}

/***/ }),

/***/ "./components/common/banner/banner.scss":
/*!**********************************************!*\
  !*** ./components/common/banner/banner.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/discount/discount.js":
/*!************************************************!*\
  !*** ./components/common/discount/discount.js ***!
  \************************************************/
/*! exports provided: Discount */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Discount", function() { return Discount; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _discount_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./discount.scss */ "./components/common/discount/discount.scss");
/* harmony import */ var _discount_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_discount_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-animate-on-scroll */ "react-animate-on-scroll");
/* harmony import */ var react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



class Discount extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return __jsx("section", {
      className: "discount"
    }, __jsx("div", {
      className: "my-5"
    }, __jsx("div", {
      className: "container"
    }, __jsx("div", {
      className: "discount-heading"
    }, __jsx("h4", {
      className: "py-3"
    }, "Employer Group Discount"), __jsx("p", {
      className: "discount-para"
    }, "Under the Corporate Express program, all employees that opt into the program gain access to our gym facilities at significantly discounted rates. Most importantly, they get free access to all our fitness events, including our weekend boot camps, trail walks, running club and curated events exclusive to the Express Fitness community.")), __jsx("div", {
      className: "d-flex justify-content-md-around justify-content-lg-around justify-content-sm-center justify-content-xs-center flex-wrap"
    }, __jsx(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2___default.a, {
      animateIn: "slideInLeft",
      duration: 1,
      animateOnce: true
    }, __jsx("div", {
      className: "card d-offer-card my-4 mx-1"
    }, __jsx("div", {
      className: "d-card-body d-flex flex-row"
    }, __jsx("h5", {
      className: "d-card-title"
    }, __jsx("svg", {
      width: "80",
      height: "80",
      viewBox: "0 0 80 80",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M80 33.5484H77.1006L72.7791 35.8222C72.2927 35.5538 71.7868 35.3264 71.2412 35.1896L65.8052 33.8294V33.2113C68.2101 31.5329 69.6774 28.7601 69.6774 25.8064V23.5509C69.6774 18.5383 65.7497 14.3435 60.9224 14.1973C60.6773 14.191 60.436 14.1998 60.1934 14.2118L61.6028 10.9123L54.77 4.0814L50.7422 6.703C49.2118 5.8411 47.5932 5.17011 45.9123 4.69947L44.9175 0H35.0838L34.089 4.70073C32.408 5.17137 30.7882 5.84236 29.2591 6.70426L25.2312 4.08266L18.4041 10.9098L19.7896 14.2118C19.7366 14.2093 19.6862 14.1998 19.6333 14.1986C17.1623 14.1268 14.8349 15.0284 13.067 16.7446C11.2966 18.4596 10.3226 20.7611 10.3226 23.2258V25.8064C10.3226 28.7601 11.7887 31.5316 14.1935 33.2101V33.8294L10.6477 34.7159L4.22064 30.9677H0V57.7048L12.6941 61.9355H15.1663L15.475 60.8543L19.3662 63.1899C19.4298 65.3056 21.1265 67.0079 23.2403 67.0823C23.3146 69.1872 25.0063 70.8789 27.1113 70.9532C27.1856 73.0582 28.8773 74.7499 30.9822 74.8242C31.0591 76.9783 32.8232 78.7097 34.995 78.7097C35.8014 78.7097 36.5921 78.4583 37.2568 78.0103L38.3997 79.0197C39.1161 79.6503 40.0384 80 40.9961 80H41.2349C43.3821 80 45.1292 78.2642 45.1562 76.124C47.2801 76.0969 49.0001 74.3769 49.0272 72.253C51.1523 72.2259 52.8736 70.5034 52.8982 68.3783C55.0428 68.3279 56.7742 66.5757 56.7742 64.4191C56.7742 64.2893 56.7679 64.1614 56.7547 64.0323L80 52.4105V33.5484ZM67.0968 23.5509V25.8064C67.0968 28.0966 65.8594 30.2363 63.8672 31.3899L63.222 31.7629L63.2271 35.8461L69.6736 37.4565L55.4839 44.925V41.4491C55.4839 39.7146 54.7833 38.1263 53.636 36.9525L58.0645 35.8461V31.7597L57.4206 31.3873C55.4297 30.2363 54.1935 28.0966 54.1935 25.8064V23.2258C54.1935 21.4661 54.8891 19.8204 56.1536 18.595C57.4168 17.3702 59.0927 16.7099 60.8449 16.778C64.2918 16.8813 67.0968 19.92 67.0968 23.5509ZM41.9456 43.9107L36.1045 42.0438C33.4425 41.1908 30.5614 41.0635 27.8364 41.6671L27.0968 41.8309V41.4491C27.0968 39.6699 28.3033 38.125 30.0284 37.6928L37.4194 35.8461V31.7597L36.7755 31.3873C34.7845 30.2363 33.5484 28.0966 33.5484 25.8064V23.2258C33.5484 21.4661 34.2439 19.8204 35.5084 18.595C36.7717 17.3702 38.4583 16.7099 40.1997 16.778C43.6467 16.8813 46.4516 19.92 46.4516 23.5509V25.8064C46.4516 28.0966 45.2142 30.2363 43.222 31.3899L42.5769 31.7629L42.5819 35.8461L49.9716 37.6928C51.6967 38.125 52.9032 39.6699 52.9032 41.4491V46.284L52.5844 46.4516H51.7937L43.8609 44.1847C43.2359 44.0077 42.592 43.9352 41.9456 43.9107ZM27.386 19.312C29.8091 14.6169 34.6862 11.6129 40 11.6129C45.2634 11.6129 50.1197 14.5766 52.5611 19.208C51.9468 20.4398 51.6129 21.8038 51.6129 23.2258V25.8064C51.6129 28.7601 53.079 31.5316 55.4839 33.2101V33.8294L50.3226 35.1216L45.16 33.8294V33.2113C47.5649 31.5329 49.0323 28.7601 49.0323 25.8064V23.5509C49.0323 18.5383 45.1046 14.3435 40.2772 14.1973C37.8075 14.1249 35.4788 15.0271 33.7109 16.7433C31.9418 18.4596 30.9677 20.7611 30.9677 23.2258V25.8064C30.9677 28.7601 32.4338 31.5316 34.8387 33.2101V33.8294L29.6774 35.1216L24.5149 33.8294V33.2113C26.9197 31.5329 28.3871 28.7601 28.3871 25.8064V23.5509C28.3871 22.0325 28.0217 20.591 27.386 19.312ZM21.4516 11.5083L25.574 7.38596L29.2024 9.74735L29.8967 9.32145C31.6362 8.25542 33.5163 7.4748 35.4839 7.00416L36.2777 6.81389L37.1755 2.58065H42.8257L43.721 6.81263L44.5149 7.00227C46.4825 7.47354 48.3625 8.25416 50.1021 9.32019L50.7964 9.74609L54.4248 7.3847L58.5421 11.5033L57.0785 14.9301C56.0824 15.3585 55.1588 15.9614 54.3548 16.7408C54.3019 16.7925 54.2565 16.8479 54.2049 16.9008C51.1649 12.0684 45.7907 9.03226 40 9.03226C34.1677 9.03226 28.7784 12.0968 25.746 16.9783C24.9345 16.129 23.9693 15.441 22.8994 14.9603L21.4516 11.5083ZM16.7742 35.8461V31.7597L16.1303 31.3873C14.1394 30.2363 12.9032 28.0966 12.9032 25.8064V23.2258C12.9032 21.4661 13.5988 19.8204 14.8633 18.595C16.1265 17.3702 17.8062 16.7099 19.5546 16.778C23.0015 16.8813 25.8064 19.92 25.8064 23.5509V25.8064C25.8064 28.0966 24.5691 30.2363 22.5769 31.3899L21.9317 31.7629L21.9367 35.8461L26.364 36.9525C25.2167 38.1263 24.5161 39.7146 24.5161 41.4491V42.4049L20.4914 43.2995L20.596 42.9341L20.6452 40.5494L13.8401 36.5795L16.7742 35.8461ZM2.58065 33.5484H3.5213L12.8012 38.9611L8.1187 57.6903L2.58065 55.8436V33.5484ZM13.1124 59.3548L10.5727 58.5087L15.1216 40.315L18.0645 42.0312V42.3998L13.2208 59.3548H13.1124ZM21.9355 63.0696C21.9355 62.689 22.0892 62.316 22.3589 62.0464L24.627 59.7782C24.8967 59.5086 25.2697 59.3548 25.6502 59.3548C26.4478 59.3548 27.0968 60.0038 27.0968 60.8014C27.0968 61.182 26.943 61.5549 26.6734 61.8246L24.4052 64.0927C24.1356 64.3624 23.7626 64.5161 23.3821 64.5161C22.5844 64.5161 21.9355 63.8672 21.9355 63.0696ZM25.8064 66.9405C25.8064 66.56 25.9602 66.187 26.2298 65.9173L28.498 63.6492C28.7676 63.3795 29.1406 63.2258 29.5212 63.2258C30.3188 63.2258 30.9677 63.8747 30.9677 64.6724C30.9677 65.0529 30.814 65.4259 30.5444 65.6956L28.2762 67.9637C28.0066 68.2334 27.6336 68.3871 27.253 68.3871C26.4554 68.3871 25.8064 67.7382 25.8064 66.9405ZM29.6774 70.8115C29.6774 70.4309 29.8311 70.058 30.1008 69.7883L32.3689 67.5202C32.6386 67.2505 33.0116 67.0968 33.3921 67.0968C34.1898 67.0968 34.8387 67.7457 34.8387 68.5433C34.8387 68.9239 34.685 69.2969 34.4153 69.5665L32.1472 71.8347C31.8775 72.1043 31.5045 72.2581 31.124 72.2581C30.3264 72.2581 29.6774 71.6091 29.6774 70.8115ZM34.995 76.129C34.1973 76.129 33.5484 75.4801 33.5484 74.6825C33.5484 74.3019 33.7021 73.9289 33.9718 73.6593L36.2399 71.3911C36.5096 71.1215 36.8826 70.9677 37.2631 70.9677C38.0607 70.9677 38.7097 71.6167 38.7097 72.4143C38.7097 72.7949 38.5559 73.1678 38.2863 73.4375L36.0181 75.7056C35.7485 75.9753 35.3755 76.129 34.995 76.129ZM41.2349 77.4193H40.9961C40.6672 77.4193 40.3522 77.2996 40.1071 77.0823L39.1419 76.2311L40.1109 75.2621C40.4568 74.9162 40.7252 74.5048 40.9236 74.0631L42.0968 75.0397C42.4036 75.2955 42.5806 75.6722 42.5806 76.0736C42.5806 76.8158 41.9771 77.4193 41.2349 77.4193ZM52.8062 65.8064H52.5832C52.2593 65.8064 51.943 65.6918 51.6954 65.4839L44.6982 59.6528L43.045 61.6362L49.84 67.2978C50.1455 67.5536 50.3226 67.9303 50.3226 68.3316C50.3226 69.0738 49.719 69.6774 48.9768 69.6774H48.6952C48.3821 69.6774 48.0759 69.5665 47.8345 69.3649L40.826 63.5251L39.1728 65.5084L45.9677 71.1706C46.2746 71.4245 46.4516 71.8013 46.4516 72.2026C46.4516 72.9448 45.848 73.5484 45.1058 73.5484C44.6106 73.5484 44.128 73.3726 43.7481 73.057L40.8682 70.6565C40.2337 69.3599 38.9302 68.4558 37.4036 68.4016C37.3293 66.2966 35.6376 64.605 33.5326 64.5306C33.4583 62.4257 31.7666 60.734 29.6617 60.6596C29.5861 58.5055 27.8219 56.7742 25.6502 56.7742C24.5898 56.7742 23.5522 57.2039 22.8024 57.9536L20.5343 60.2218C20.3988 60.3572 20.2867 60.511 20.1726 60.6634L16.209 58.285L19.6837 46.124L28.3947 44.1872C30.6798 43.6763 33.0891 43.7872 35.3175 44.5004L37.2184 45.1084L30.7743 48.3304C29.3019 49.0682 28.3871 50.5494 28.3871 52.1963V52.4515C28.3871 54.8349 30.3264 56.7742 32.7098 56.7742C33.4929 56.7742 34.2635 56.5612 34.9332 56.1574L39.7379 53.2762C40.7031 52.6978 41.9985 52.8245 42.832 53.5742L53.7342 63.3858C54.026 63.6505 54.1935 64.026 54.1935 64.4191C54.1935 65.1846 53.5717 65.8064 52.8062 65.8064ZM77.4193 50.8153L55.6811 61.6854C55.6105 61.6104 55.5368 61.5392 55.4593 61.4699L44.559 51.6583C43.6038 50.7964 42.3689 50.3226 41.0824 50.3226C40.1418 50.3226 39.2181 50.5784 38.4091 51.0635L33.6026 53.9459C33.3329 54.1085 33.0248 54.1935 32.7098 54.1935C31.7484 54.1935 30.9677 53.4129 30.9677 52.4515V52.1963C30.9677 51.5316 31.3369 50.9356 31.9304 50.6389L39.128 47.0401C40.3598 46.4233 41.8221 46.2853 43.1496 46.6671L51.2582 48.9831L53.222 49.0323L77.4193 36.2966V50.8153Z",
      fill: "#444444"
    }))), __jsx("p", {
      className: "d-card-text mb-0"
    }, "Minimum of 25 persons from each company")))), __jsx(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2___default.a, {
      animateIn: "slideInRight",
      duration: 1,
      animateOnce: true
    }, __jsx("div", {
      className: "card d-offer-card my-4 mx-1"
    }, __jsx("div", {
      className: "d-card-body d-flex flex-row"
    }, __jsx("h5", {
      className: "d-card-title"
    }, __jsx("svg", {
      width: "80",
      height: "80",
      viewBox: "0 0 80 80",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M78.1369 19.474L77.2831 18.6202C74.8029 16.1399 70.7668 16.1399 68.2865 18.6202L67.452 19.4547L65.8199 21.0866L61.5899 25.3166V16.5327C65.5828 16.1766 68.724 12.8144 68.724 8.73049C68.724 4.41016 65.209 0.895142 60.8888 0.895142H7.83519C3.51501 0.895142 0 4.41 0 8.73033V71.1142H6.68128V79.105H61.5902V45.0173L75.6705 30.9371L76.9036 29.7038L77.3022 29.3053L78.1367 28.4707C79.3383 27.2692 80 25.6718 80 23.9726C80 22.2734 79.3386 20.6754 78.1369 19.474ZM13.3829 3.20312H60.889C63.9368 3.20312 66.4162 5.68266 66.4162 8.73049C66.4162 11.7783 63.9367 14.2577 60.889 14.2577H13.3829C14.7957 12.8397 15.6705 10.8853 15.6705 8.73049C15.6705 6.57532 14.7958 4.62109 13.3829 3.20312ZM6.68128 14.2575V68.8062H2.30798V8.73033C2.30798 5.6825 4.78767 3.20296 7.83534 3.20296C10.883 3.20296 13.3626 5.68266 13.3626 8.73033C13.3626 11.7782 10.8829 14.2575 7.83534 14.2575H6.68128ZM59.2824 76.797H8.9891V16.5657H59.2823V27.6245L54.2788 32.6278H14.3269V34.9357H51.9711L47.1407 39.7662H14.3269V42.0742H44.8328L40.0025 46.9045H14.3269V49.2125H37.6947L37.292 49.6152L35.9794 54.043H14.3269V56.351H35.2953L33.8633 61.1815H14.3269V63.4895H55.101V61.1815H41.3545L47.1424 59.4658L50.2569 56.3513H55.1008V54.0433H52.5649L59.2824 47.3258V76.797ZM38.8702 52.4108L44.3463 57.8869L36.563 60.1943L38.8702 52.4108ZM46.5333 56.8102L39.9469 50.2238L67.452 22.7187L74.0384 29.3051L46.5333 56.8102ZM76.5051 26.8385L75.6706 27.6731L69.0842 21.0866L69.9187 20.2521C71.4992 18.6716 74.0708 18.6716 75.6514 20.2521L76.5051 21.1058C77.2708 21.8715 77.6925 22.8894 77.6925 23.9721C77.6923 25.0549 77.2706 26.0729 76.5051 26.8385Z",
      fill: "#444444"
    }))), __jsx("p", {
      className: "d-card-text mb-0"
    }, "Sign-ups through an auto-charge facility \u2013 either payroll deduction or auto-charge to a credit card")))), __jsx(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2___default.a, {
      animateIn: "slideInLeft",
      duration: 1,
      animateOnce: true
    }, __jsx("div", {
      className: "card d-offer-card my-4 mx-1"
    }, __jsx("div", {
      className: "d-card-body d-flex flex-row"
    }, __jsx("h5", {
      className: "d-card-title"
    }, __jsx("svg", {
      width: "80",
      height: "80",
      viewBox: "0 0 80 80",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("g", {
      "clip-path": "url(#clip0)"
    }, __jsx("path", {
      d: "M70.6511 11.9995C70.6511 11.2632 70.0541 10.6663 69.3178 10.6663H31.9861C31.2498 10.6663 30.6528 11.2632 30.6528 11.9995V17.3326C30.6528 18.0689 31.2498 18.6659 31.9861 18.6659C32.7224 18.6659 33.3194 18.0689 33.3194 17.3326V13.3328H67.9845V21.3325H38.6525C37.9162 21.3325 37.3192 21.9294 37.3192 22.6657C37.3192 23.402 37.9162 23.999 38.6525 23.999H69.3178C70.0541 23.999 70.6511 23.402 70.6511 22.6657V11.9995Z",
      fill: "#444444"
    }), __jsx("path", {
      d: "M54.6518 31.9986C55.3881 31.9986 55.985 31.4016 55.985 30.6653C55.985 29.929 55.3881 29.332 54.6518 29.332H46.6521C45.9158 29.332 45.3188 29.929 45.3188 30.6653V38.665C45.3188 39.4013 45.9158 39.9982 46.6521 39.9982H54.6518C55.3881 39.9982 55.985 39.4013 55.985 38.665C55.985 37.9287 55.3881 37.3317 54.6518 37.3317H47.9854V31.9986H54.6518Z",
      fill: "#444444"
    }), __jsx("path", {
      d: "M70.6511 30.6653C70.6511 29.929 70.0541 29.332 69.3178 29.332H61.3181C60.5818 29.332 59.9849 29.929 59.9849 30.6653V38.665C59.9849 39.4013 60.5818 39.9982 61.3181 39.9982H69.3178C70.0541 39.9982 70.6511 39.4013 70.6511 38.665V30.6653ZM67.9845 37.3317H62.6514V31.9986H67.9845V37.3317Z",
      fill: "#444444"
    }), __jsx("path", {
      d: "M55.985 45.3314C55.985 44.5951 55.3881 43.9982 54.6518 43.9982H46.6521C45.9158 43.9982 45.3188 44.5951 45.3188 45.3314V53.3311C45.3188 54.0674 45.9158 54.6644 46.6521 54.6644H54.6518C55.3881 54.6644 55.985 54.0674 55.985 53.3311V45.3314ZM53.3185 51.9978H47.9854V46.6647H53.3185V51.9978Z",
      fill: "#444444"
    }), __jsx("path", {
      d: "M54.6518 58.6642H46.6521C45.9158 58.6642 45.3188 59.2612 45.3188 59.9975V67.9971C45.3188 68.7334 45.9158 69.3304 46.6521 69.3304H54.6518C55.3881 69.3304 55.985 68.7334 55.985 67.9971V59.9975C55.985 59.2612 55.3881 58.6642 54.6518 58.6642ZM53.3185 66.6638H47.9854V61.3307H53.3185V66.6638Z",
      fill: "#444444"
    }), __jsx("path", {
      d: "M69.3178 43.9982H61.3181C60.5818 43.9982 59.9849 44.5951 59.9849 45.3314V67.9971C59.9849 68.7334 60.5818 69.3304 61.3181 69.3304H69.3178C70.0541 69.3304 70.6511 68.7334 70.6511 67.9971V45.3314C70.6511 44.5951 70.0541 43.9982 69.3178 43.9982ZM67.9845 66.6639H62.6514V46.6647H67.9845V66.6639Z",
      fill: "#444444"
    }), __jsx("path", {
      d: "M63.9847 18.6658C64.721 18.6658 65.318 18.0688 65.318 17.3325C65.318 16.5962 64.721 15.9993 63.9847 15.9993H58.6516C57.9153 15.9993 57.3184 16.5962 57.3184 17.3325C57.3184 18.0688 57.9153 18.6658 58.6516 18.6658H63.9847Z",
      fill: "#444444"
    }), __jsx("path", {
      d: "M53.3182 18.6658C54.0545 18.6658 54.6515 18.0688 54.6515 17.3325C54.6515 16.5962 54.0545 15.9993 53.3182 15.9993H47.9851C47.2488 15.9993 46.6519 16.5962 46.6519 17.3325C46.6519 18.0688 47.2488 18.6658 47.9851 18.6658H53.3182Z",
      fill: "#444444"
    }), __jsx("path", {
      d: "M73.3176 0H27.9862C24.3041 0 21.3198 2.9849 21.3198 6.66638V17.3326C21.3198 18.0689 21.9168 18.6659 22.6531 18.6659C23.3894 18.6659 23.9864 18.0689 23.9864 17.3326V6.66638C23.9864 4.45749 25.7773 2.66655 27.9862 2.66655H73.3176C75.5265 2.66655 77.3174 4.45749 77.3174 6.66638V73.3302C77.3174 75.539 75.5265 77.33 73.3176 77.33H37.3191C36.5828 77.33 35.9859 77.927 35.9859 78.6633C35.9859 79.3996 36.5828 79.9965 37.3191 79.9965H73.3176C76.9991 79.9965 79.984 77.0116 79.984 73.3302V6.66638C79.984 2.9849 76.9991 0 73.3176 0Z",
      fill: "#444444"
    }), __jsx("path", {
      d: "M38.6523 30.6653V38.665C38.6523 39.4013 39.2493 39.9982 39.9856 39.9982C40.7219 39.9982 41.3189 39.4013 41.3189 38.665V30.6653C41.3189 29.929 40.7219 29.332 39.9856 29.332C39.2493 29.332 38.6523 29.929 38.6523 30.6653Z",
      fill: "#444444"
    }), __jsx("path", {
      d: "M21.3199 51.9977C22.7925 51.9977 23.9865 53.1916 23.9865 54.6642C23.9865 55.4005 24.5834 55.9975 25.3197 55.9975C26.056 55.9975 26.653 55.4005 26.653 54.6642C26.6511 52.234 25.0066 50.1123 22.6532 49.5043V46.6646C22.6532 45.9283 22.0562 45.3313 21.3199 45.3313C20.5836 45.3313 19.9866 45.9283 19.9866 46.6646V49.5043C17.3937 50.182 15.7069 52.678 16.0448 55.3367C16.382 57.9948 18.6403 59.9895 21.3199 59.9973C22.7925 59.9973 23.9865 61.1913 23.9865 62.6639C23.9865 64.1365 22.7925 65.3304 21.3199 65.3304C19.8473 65.3304 18.6534 64.1365 18.6534 62.6639C18.6534 61.9276 18.0564 61.3306 17.3201 61.3306C16.5838 61.3306 15.9868 61.9276 15.9868 62.6639C15.9888 65.0941 17.6332 67.2158 19.9866 67.8238V70.6636C19.9866 71.3998 20.5836 71.9968 21.3199 71.9968C22.0562 71.9968 22.6532 71.3998 22.6532 70.6636V67.8238C25.2455 67.1461 26.933 64.6501 26.5951 61.9914C26.2572 59.3333 23.9995 57.3386 21.3199 57.3308C19.8473 57.3308 18.6534 56.1368 18.6534 54.6642C18.6534 53.1916 19.8473 51.9977 21.3199 51.9977Z",
      fill: "#444444"
    }), __jsx("path", {
      d: "M39.8124 69.3303C40.1803 68.6897 39.9596 67.872 39.319 67.5036C38.6784 67.1357 37.8607 67.3564 37.4922 67.997C33.5933 74.7689 25.8879 78.395 18.1845 77.0845C10.481 75.7734 4.40968 69.8023 2.96963 62.1216C1.53024 54.441 5.02749 46.6763 11.7336 42.6648H30.9059C36.5326 46.0325 39.9797 52.1064 39.9856 58.6641C39.9856 59.4004 40.5826 59.9974 41.3189 59.9974C42.0552 59.9974 42.6522 59.4004 42.6522 58.6641C42.6469 52.085 39.6067 45.8763 34.4123 41.838C35.5945 40.9286 36.1726 39.4371 35.9122 37.969C35.6512 36.5004 34.5952 35.2986 33.1727 34.852C34.1297 33.6665 34.6519 32.1887 34.6525 30.6653V26.6655C34.6492 24.4488 33.2756 22.4652 31.2015 21.6833C29.1274 20.9008 26.7857 21.4834 25.3196 23.1455C24.3072 21.9977 22.8503 21.3402 21.3197 21.3402C19.7892 21.3402 18.3322 21.9977 17.3199 23.1455C15.8538 21.4834 13.5121 20.9008 11.438 21.6833C9.36389 22.4652 7.99025 24.4488 7.98699 26.6655V30.6653C7.98765 32.1887 8.50976 33.6665 9.46675 34.852C8.04428 35.2986 6.98769 36.5004 6.72728 37.969C6.46688 39.4371 7.04498 40.9286 8.22722 41.838C1.43454 47.1288 -1.51129 55.9845 0.75749 64.2895C3.02692 72.5951 10.0663 78.7225 18.605 79.8246C27.1444 80.9268 35.5086 76.7877 39.8124 69.3303ZM10.6535 26.6655C10.6535 25.1929 11.8475 23.999 13.3201 23.999C14.7927 23.999 15.9866 25.1929 15.9866 26.6655C15.9866 27.4018 16.5836 27.9988 17.3199 27.9988C18.0562 27.9988 18.6532 27.4018 18.6532 26.6655C18.6532 25.1929 19.8472 23.999 21.3197 23.999C22.7923 23.999 23.9863 25.1929 23.9863 26.6655C23.9863 27.4018 24.5833 27.9988 25.3196 27.9988C26.0559 27.9988 26.6528 27.4018 26.6528 26.6655C26.6528 25.1929 27.8468 23.999 29.3194 23.999C30.792 23.999 31.986 25.1929 31.986 26.6655V30.6653C31.986 32.8742 30.195 34.6652 27.9861 34.6652H14.6534C12.4445 34.6652 10.6535 32.8742 10.6535 30.6653V26.6655ZM10.6535 37.3317H31.986C32.7222 37.3317 33.3192 37.9287 33.3192 38.665C33.3192 39.4013 32.7222 39.9983 31.986 39.9983H10.6535C9.91725 39.9983 9.32027 39.4013 9.32027 38.665C9.32027 37.9287 9.91725 37.3317 10.6535 37.3317Z",
      fill: "#444444"
    })), __jsx("defs", null, __jsx("clipPath", {
      id: "clip0"
    }, __jsx("rect", {
      width: "80",
      height: "80",
      fill: "white"
    }))))), __jsx("p", {
      className: "d-card-text mb-0"
    }, "Introductory rate of $3,500 per month per person for the first 2 months")))), __jsx(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2___default.a, {
      animateIn: "slideInRight",
      duration: 1,
      animateOnce: true
    }, __jsx("div", {
      className: "card d-offer-card my-4 mx-1"
    }, __jsx("div", {
      className: "d-card-body d-flex flex-row"
    }, __jsx("h5", {
      className: "d-card-title"
    }, __jsx("svg", {
      width: "80",
      height: "80",
      viewBox: "0 0 80 80",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M78.1369 19.474L77.2831 18.6202C74.8029 16.1399 70.7668 16.1399 68.2865 18.6202L67.452 19.4547L65.8199 21.0866L61.5899 25.3166V16.5327C65.5828 16.1766 68.724 12.8144 68.724 8.73049C68.724 4.41016 65.209 0.895142 60.8888 0.895142H7.83519C3.51501 0.895142 0 4.41 0 8.73033V71.1142H6.68128V79.105H61.5902V45.0173L75.6705 30.9371L76.9036 29.7038L77.3022 29.3053L78.1367 28.4707C79.3383 27.2692 80 25.6718 80 23.9726C80 22.2734 79.3386 20.6754 78.1369 19.474ZM13.3829 3.20312H60.889C63.9368 3.20312 66.4162 5.68266 66.4162 8.73049C66.4162 11.7783 63.9367 14.2577 60.889 14.2577H13.3829C14.7957 12.8397 15.6705 10.8853 15.6705 8.73049C15.6705 6.57532 14.7958 4.62109 13.3829 3.20312ZM6.68128 14.2575V68.8062H2.30798V8.73033C2.30798 5.6825 4.78767 3.20296 7.83534 3.20296C10.883 3.20296 13.3626 5.68266 13.3626 8.73033C13.3626 11.7782 10.8829 14.2575 7.83534 14.2575H6.68128ZM59.2824 76.797H8.9891V16.5657H59.2823V27.6245L54.2788 32.6278H14.3269V34.9357H51.9711L47.1407 39.7662H14.3269V42.0742H44.8328L40.0025 46.9045H14.3269V49.2125H37.6947L37.292 49.6152L35.9794 54.043H14.3269V56.351H35.2953L33.8633 61.1815H14.3269V63.4895H55.101V61.1815H41.3545L47.1424 59.4658L50.2569 56.3513H55.1008V54.0433H52.5649L59.2824 47.3258V76.797ZM38.8702 52.4108L44.3463 57.8869L36.563 60.1943L38.8702 52.4108ZM46.5333 56.8102L39.9469 50.2238L67.452 22.7187L74.0384 29.3051L46.5333 56.8102ZM76.5051 26.8385L75.6706 27.6731L69.0842 21.0866L69.9187 20.2521C71.4992 18.6716 74.0708 18.6716 75.6514 20.2521L76.5051 21.1058C77.2708 21.8715 77.6925 22.8894 77.6925 23.9721C77.6923 25.0549 77.2706 26.0729 76.5051 26.8385Z",
      fill: "#444444"
    }))), __jsx("p", {
      className: "d-card-text mb-0"
    }, "Sign-ups through an auto-charge facility \u2013 either payroll deduction or auto-charge to a credit card"))))))));
  }

}

/***/ }),

/***/ "./components/common/discount/discount.scss":
/*!**************************************************!*\
  !*** ./components/common/discount/discount.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/featuredProduct/FeauturedProduct.scss":
/*!*****************************************************************!*\
  !*** ./components/common/featuredProduct/FeauturedProduct.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/featuredProduct/featuredProduct.js":
/*!**************************************************************!*\
  !*** ./components/common/featuredProduct/featuredProduct.js ***!
  \**************************************************************/
/*! exports provided: Featuredproduct */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Featuredproduct", function() { return Featuredproduct; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _FeauturedProduct_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FeauturedProduct.scss */ "./components/common/featuredProduct/FeauturedProduct.scss");
/* harmony import */ var _FeauturedProduct_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_FeauturedProduct_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _static_images_Rectangle4_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../static/images/Rectangle4.png */ "./static/images/Rectangle4.png");
/* harmony import */ var _static_images_Rectangle4_png__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_static_images_Rectangle4_png__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _static_images_Rectangle5_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../static/images/Rectangle5.png */ "./static/images/Rectangle5.png");
/* harmony import */ var _static_images_Rectangle5_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_static_images_Rectangle5_png__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _static_images_Rectangle6_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../static/images/Rectangle6.png */ "./static/images/Rectangle6.png");
/* harmony import */ var _static_images_Rectangle6_png__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_static_images_Rectangle6_png__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _static_images_Rectangle7_png__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../static/images/Rectangle7.png */ "./static/images/Rectangle7.png");
/* harmony import */ var _static_images_Rectangle7_png__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_static_images_Rectangle7_png__WEBPACK_IMPORTED_MODULE_5__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






class Featuredproduct extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return __jsx("section", {
      className: "featured-products"
    }, __jsx("div", {
      className: "mb-5"
    }, __jsx("h3", {
      className: "text-center featured-products-heading py-4"
    }, __jsx("span", {
      className: "Text-primary"
    }, "Featured"), " Programs"), __jsx("div", {
      className: "card-group"
    }, __jsx("div", {
      className: "card card-product"
    }, __jsx("div", {
      className: "position-relative"
    }, __jsx("img", {
      className: "card-img-top",
      src: _static_images_Rectangle4_png__WEBPACK_IMPORTED_MODULE_2___default.a,
      alt: ""
    })), __jsx("p", {
      className: "fitness-catagory text-center"
    }, "Express Fitness"), __jsx("p", {
      className: " button-transparent"
    }, __jsx("button", {
      className: "btn btn-transparents"
    }, "Join Now"))), __jsx("div", {
      className: "card card-product"
    }, __jsx("div", {
      className: "position-relative"
    }, __jsx("img", {
      className: "card-img-top",
      src: _static_images_Rectangle5_png__WEBPACK_IMPORTED_MODULE_3___default.a,
      alt: ""
    })), __jsx("p", {
      className: "fitness-catagory text-center"
    }, "365 fitness"), __jsx("p", {
      className: " button-transparent"
    }, __jsx("button", {
      className: "btn btn-transparents"
    }, "Join Now"))), __jsx("div", {
      className: "card card-product"
    }, __jsx("div", {
      className: "position-relative"
    }, __jsx("img", {
      className: "card-img-top",
      src: _static_images_Rectangle6_png__WEBPACK_IMPORTED_MODULE_4___default.a,
      alt: ""
    })), __jsx("p", {
      className: "fitness-catagory text-center"
    }, "Fitplex"), __jsx("p", {
      className: " button-transparent"
    }, __jsx("button", {
      className: "btn btn-transparents"
    }, "Join Now"))), __jsx("div", {
      className: "card card-product"
    }, __jsx("div", {
      className: "position-relative"
    }, __jsx("img", {
      className: "card-img-top",
      src: _static_images_Rectangle7_png__WEBPACK_IMPORTED_MODULE_5___default.a,
      alt: ""
    })), __jsx("p", {
      className: "fitness-catagory text-center"
    }, "Fitbox"), __jsx("p", {
      className: "button-transparent"
    }, __jsx("button", {
      className: "btn btn-transparents"
    }, "Join Now"))))));
  }

}

/***/ }),

/***/ "./components/common/footer/footer.js":
/*!********************************************!*\
  !*** ./components/common/footer/footer.js ***!
  \********************************************/
/*! exports provided: Footer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Footer", function() { return Footer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _footer_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./footer.scss */ "./components/common/footer/footer.scss");
/* harmony import */ var _footer_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_footer_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _static_images_footer_logo_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../static/images/footer-logo.png */ "./static/images/footer-logo.png");
/* harmony import */ var _static_images_footer_logo_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_static_images_footer_logo_png__WEBPACK_IMPORTED_MODULE_3__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




class Footer extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return __jsx("footer", null, __jsx("div", {
      className: "footer-page"
    }, __jsx("div", {
      className: "footer-background"
    }, __jsx("div", {
      className: "container"
    }, __jsx("div", {
      className: "row"
    }, __jsx("div", {
      className: "col-md-5"
    }, __jsx("div", {
      className: "footer-logo"
    }, __jsx("img", {
      src: _static_images_footer_logo_png__WEBPACK_IMPORTED_MODULE_3___default.a,
      alt: ""
    })), __jsx("p", {
      className: "footer-para"
    }, " Sagicor 365 is a partnership between Express Fitness and Sagicor to offer Sagicor customers the option to become members of Express Fitness gyms island-wide."), __jsx("p", {
      className: "footer-para"
    }, " 24, Street, New york, United states")), __jsx("div", {
      className: "col-md-3"
    }), __jsx("div", {
      className: "col-md-4"
    }, __jsx("div", {
      className: "footer-links-heading"
    }, __jsx("h6", null, "Useful Links"), __jsx("div", {
      className: "footer-links-list"
    }, __jsx("li", null, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
      href: "/aboutus",
      prefetch: false
    }, __jsx("a", {
      href: ""
    }, "About Us"))), __jsx("li", null, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
      href: "/features",
      prefetch: false
    }, __jsx("a", {
      href: ""
    }, "Features"))), __jsx("li", null, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
      href: "/pricing",
      prefetch: false
    }, __jsx("a", {
      href: ""
    }, "Pricing"))), __jsx("li", null, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
      href: "/contact",
      prefetch: false
    }, __jsx("a", {
      href: ""
    }, "Contact"))))))), __jsx("hr", null), __jsx("div", {
      className: "row pl-3 pr-4"
    }, __jsx("p", {
      className: "footer-copy-rights"
    }, " \xA9 Copyright Sagicor 2019"), __jsx("div", {
      className: "ml-auto"
    }, __jsx("a", {
      href: ""
    }, __jsx("i", {
      className: "fab fa-facebook-f mx-1 faIcons"
    })), __jsx("a", {
      href: ""
    }, __jsx("i", {
      className: "fab fa-twitter faIcons mx-1"
    })), __jsx("a", {
      href: ""
    }, __jsx("i", {
      className: "fab fa-google-plus-g faIcons mx-1"
    })), __jsx("a", {
      href: ""
    }, __jsx("i", {
      className: "fab fa-linkedin-in faIcons mx-1"
    }))))))));
  }

}

/***/ }),

/***/ "./components/common/footer/footer.scss":
/*!**********************************************!*\
  !*** ./components/common/footer/footer.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/getintouch/getintouch.js":
/*!****************************************************!*\
  !*** ./components/common/getintouch/getintouch.js ***!
  \****************************************************/
/*! exports provided: Getintouch */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Getintouch", function() { return Getintouch; });
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _getintouch_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./getintouch.scss */ "./components/common/getintouch/getintouch.scss");
/* harmony import */ var _getintouch_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_getintouch_scss__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var google_map_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! google-map-react */ "google-map-react");
/* harmony import */ var google_map_react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(google_map_react__WEBPACK_IMPORTED_MODULE_3__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;



class Getintouch extends react__WEBPACK_IMPORTED_MODULE_1__["Component"] {
  render() {
    return __jsx("div", {
      className: "getintouch-container"
    }, __jsx("div", {
      className: "container"
    }, __jsx("h4", {
      className: "getintouch-heading"
    }, "Get in Touch"), __jsx("div", {
      className: "row"
    }, __jsx("div", {
      className: "col-md-5 p-3"
    }, __jsx("div", {
      className: "d-flex flex-row py-3"
    }, __jsx("div", {
      className: "pr-4 t-icons"
    }, __jsx("i", {
      className: "fas fa-map-marker-alt"
    })), __jsx("div", {
      className: "flex-column"
    }, __jsx("p", null, "Location"), __jsx("p", {
      className: "getintouch-para"
    }, "24, Park Streeet, Austin, Texas, Dallas"))), __jsx("div", {
      className: "d-flex flex-row py-3"
    }, __jsx("div", {
      className: "pr-3 t-icons"
    }, __jsx("i", {
      className: "fas fa-envelope"
    })), __jsx("div", {
      className: "flex-column"
    }, __jsx("p", null, "Have any Questions?"), __jsx("p", {
      className: "getintouch-para"
    }, "info@profinch.com"))), __jsx("div", {
      className: "d-flex flex-row py-3"
    }, __jsx("div", {
      className: "pr-3 t-icons"
    }, __jsx("i", {
      className: "fas fa-phone-alt"
    })), __jsx("div", {
      className: "flex-column"
    }, __jsx("p", null, "Call Us"), __jsx("p", {
      className: "getintouch-para"
    }, "080 4256 4256")))), __jsx("div", {
      className: "offset-md-1 col-md-6 col-xs-12 map"
    }, __jsx(google_map_react__WEBPACK_IMPORTED_MODULE_3___default.a, {
      bootstrapURLKeys: {
        key: "AIzaSyAcaQwH9WxvTZgDwXCKAT73mjbRrg0Gj7w"
      },
      defaultCenter: this.props.center,
      defaultZoom: this.props.zoom
    })))));
  }

}

Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(Getintouch, "defaultProps", {
  center: {
    lat: 13.0619,
    lng: 80.2468
  },
  zoom: 15
});

/***/ }),

/***/ "./components/common/getintouch/getintouch.scss":
/*!******************************************************!*\
  !*** ./components/common/getintouch/getintouch.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/header/header.js":
/*!********************************************!*\
  !*** ./components/common/header/header.js ***!
  \********************************************/
/*! exports provided: Header */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Header", function() { return Header; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _header_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./header.scss */ "./components/common/header/header.scss");
/* harmony import */ var _header_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_header_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _static_images_logo2_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../static/images/logo2.png */ "./static/images/logo2.png");
/* harmony import */ var _static_images_logo2_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_static_images_logo2_png__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _static_images_logo1_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../static/images/logo1.png */ "./static/images/logo1.png");
/* harmony import */ var _static_images_logo1_png__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_static_images_logo1_png__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! reactstrap */ "reactstrap");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(reactstrap__WEBPACK_IMPORTED_MODULE_5__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






const Header = () => {
  const {
    0: isOpen,
    1: setIsOpen
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);

  const toggle = () => setIsOpen(!isOpen);

  return __jsx("header", null, __jsx("div", {
    className: "head"
  }, __jsx("div", {
    className: "container py-0 px-0"
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Navbar"], {
    light: true,
    expand: "lg"
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/",
    prefetch: false
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavbarBrand"], {
    className: "m-0 nav-brand"
  }, __jsx("img", {
    src: _static_images_logo1_png__WEBPACK_IMPORTED_MODULE_4___default.a,
    width: "140px",
    height: "45px"
  }), __jsx("img", {
    src: _static_images_logo2_png__WEBPACK_IMPORTED_MODULE_3___default.a,
    width: "50px",
    height: "45px"
  }))), __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavbarToggler"], {
    onClick: toggle
  }), __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Collapse"], {
    isOpen: isOpen,
    navbar: true
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Nav"], {
    className: "ml-auto",
    navbar: true
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavItem"], {
    className: "nav-item active margin"
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/aboutus",
    prefetch: false
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavLink"], {
    className: "nav-link"
  }, "About Us"))), __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavItem"], {
    className: "nav-item"
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/features",
    prefetch: false
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavLink"], {
    className: "nav-link"
  }, "Features"))), __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavItem"], {
    className: "nav-item"
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/pricing",
    prefetch: false
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavLink"], {
    className: "nav-link"
  }, "Pricing"))), __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavItem"], {
    className: "nav-item"
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/contact",
    prefetch: false
  }, __jsx(reactstrap__WEBPACK_IMPORTED_MODULE_5__["NavLink"], {
    className: "nav-link"
  }, "Contact")))), __jsx("div", {
    className: "header-button"
  }, __jsx("button", {
    className: "btn btn-blue ml-3"
  }, "Log In")))))));
};

/***/ }),

/***/ "./components/common/header/header.scss":
/*!**********************************************!*\
  !*** ./components/common/header/header.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/index.js":
/*!************************************!*\
  !*** ./components/common/index.js ***!
  \************************************/
/*! exports provided: Header, Infoheader, Footer, Banner, Featuredproduct, Otherprograms, Offer, Testimonial, Abouthome, Getintouch, Message, Whatweoffer, Discount, Pricingfitness */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _header_header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header/header */ "./components/common/header/header.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Header", function() { return _header_header__WEBPACK_IMPORTED_MODULE_0__["Header"]; });

/* harmony import */ var _infoheader_infoheader__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./infoheader/infoheader */ "./components/common/infoheader/infoheader.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Infoheader", function() { return _infoheader_infoheader__WEBPACK_IMPORTED_MODULE_1__["Infoheader"]; });

/* harmony import */ var _footer_footer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./footer/footer */ "./components/common/footer/footer.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Footer", function() { return _footer_footer__WEBPACK_IMPORTED_MODULE_2__["Footer"]; });

/* harmony import */ var _banner_banner__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./banner/banner */ "./components/common/banner/banner.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Banner", function() { return _banner_banner__WEBPACK_IMPORTED_MODULE_3__["Banner"]; });

/* harmony import */ var _featuredProduct_featuredProduct__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./featuredProduct/featuredProduct */ "./components/common/featuredProduct/featuredProduct.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Featuredproduct", function() { return _featuredProduct_featuredProduct__WEBPACK_IMPORTED_MODULE_4__["Featuredproduct"]; });

/* harmony import */ var _otherprograms_otherprograms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./otherprograms/otherprograms */ "./components/common/otherprograms/otherprograms.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Otherprograms", function() { return _otherprograms_otherprograms__WEBPACK_IMPORTED_MODULE_5__["Otherprograms"]; });

/* harmony import */ var _offers_offer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./offers/offer */ "./components/common/offers/offer.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Offer", function() { return _offers_offer__WEBPACK_IMPORTED_MODULE_6__["Offer"]; });

/* harmony import */ var _testimonials_testimonial__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./testimonials/testimonial */ "./components/common/testimonials/testimonial.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Testimonial", function() { return _testimonials_testimonial__WEBPACK_IMPORTED_MODULE_7__["Testimonial"]; });

/* harmony import */ var _abouthome_abouthome__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./abouthome/abouthome */ "./components/common/abouthome/abouthome.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Abouthome", function() { return _abouthome_abouthome__WEBPACK_IMPORTED_MODULE_8__["Abouthome"]; });

/* harmony import */ var _getintouch_getintouch__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./getintouch/getintouch */ "./components/common/getintouch/getintouch.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Getintouch", function() { return _getintouch_getintouch__WEBPACK_IMPORTED_MODULE_9__["Getintouch"]; });

/* harmony import */ var _message_message__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./message/message */ "./components/common/message/message.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Message", function() { return _message_message__WEBPACK_IMPORTED_MODULE_10__["Message"]; });

/* harmony import */ var _whatweoffer_whatweoffer__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./whatweoffer/whatweoffer */ "./components/common/whatweoffer/whatweoffer.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Whatweoffer", function() { return _whatweoffer_whatweoffer__WEBPACK_IMPORTED_MODULE_11__["Whatweoffer"]; });

/* harmony import */ var _discount_discount__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./discount/discount */ "./components/common/discount/discount.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Discount", function() { return _discount_discount__WEBPACK_IMPORTED_MODULE_12__["Discount"]; });

/* harmony import */ var _pricingfitness_pricingfitness__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pricingfitness/pricingfitness */ "./components/common/pricingfitness/pricingfitness.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Pricingfitness", function() { return _pricingfitness_pricingfitness__WEBPACK_IMPORTED_MODULE_13__["Pricingfitness"]; });
















/***/ }),

/***/ "./components/common/infoheader/infoheader.js":
/*!****************************************************!*\
  !*** ./components/common/infoheader/infoheader.js ***!
  \****************************************************/
/*! exports provided: Infoheader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Infoheader", function() { return Infoheader; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _infoheader_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./infoheader.scss */ "./components/common/infoheader/infoheader.scss");
/* harmony import */ var _infoheader_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_infoheader_scss__WEBPACK_IMPORTED_MODULE_1__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


class Infoheader extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return __jsx("section", {
      className: "infoHeader"
    }, __jsx("div", {
      className: "sc-head"
    }, __jsx("div", {
      className: "container"
    }, __jsx("div", {
      className: "row"
    }, __jsx("div", {
      className: " row-head"
    }, __jsx("i", {
      className: "ml-1 faIcons fas fa-tablet-alt"
    }), __jsx("span", {
      className: "px-2"
    }, "123-456-7899"), __jsx("i", {
      className: "ml-1 faIcons fas fa-print"
    }), __jsx("span", {
      className: "pl-2"
    }, "123-456-7896")), __jsx("div", {
      className: "ml-auto"
    }, __jsx("a", {
      href: ""
    }, __jsx("i", {
      className: "fab fa-facebook-f mx-1 faIcons"
    })), __jsx("a", {
      href: ""
    }, __jsx("i", {
      className: "fab fa-twitter faIcons mx-1"
    })), __jsx("a", {
      href: ""
    }, __jsx("i", {
      className: "fab fa-google-plus-g faIcons mx-1"
    })), __jsx("a", {
      href: ""
    }, __jsx("i", {
      className: "fab fa-linkedin-in faIcons ml-1"
    })))))));
  }

}

/***/ }),

/***/ "./components/common/infoheader/infoheader.scss":
/*!******************************************************!*\
  !*** ./components/common/infoheader/infoheader.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/message/message.js":
/*!**********************************************!*\
  !*** ./components/common/message/message.js ***!
  \**********************************************/
/*! exports provided: Message */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Message", function() { return Message; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _message_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./message.scss */ "./components/common/message/message.scss");
/* harmony import */ var _message_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_message_scss__WEBPACK_IMPORTED_MODULE_1__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


class Message extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return __jsx("section", {
      className: "message"
    }, __jsx("div", {
      className: "my-5"
    }, __jsx("div", {
      className: "message-heading"
    }, __jsx("h4", null, "Send Message")), __jsx("div", {
      className: "container"
    }, __jsx("form", null, __jsx("div", {
      className: "form-row"
    }, __jsx("div", {
      className: "col"
    }, __jsx("input", {
      type: "text",
      className: "form-control",
      placeholder: "Full name"
    })), __jsx("div", {
      className: "col"
    }, __jsx("input", {
      type: "text",
      className: "form-control",
      placeholder: "Email Address"
    }))), __jsx("div", {
      className: "form-group pt-4 pb-3"
    }, __jsx("input", {
      type: "text",
      className: "form-control",
      placeholder: "Subject"
    })), __jsx("div", {
      className: "form-group"
    }, __jsx("textarea", {
      className: "form-control",
      rows: "3",
      placeholder: "Message"
    }))), __jsx("div", {
      className: "d-flex justify-content-center pt-4"
    }, __jsx("button", {
      className: "btn btn-black"
    }, "SEND MESSAGE")))));
  }

}

/***/ }),

/***/ "./components/common/message/message.scss":
/*!************************************************!*\
  !*** ./components/common/message/message.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/offers/offer.js":
/*!*******************************************!*\
  !*** ./components/common/offers/offer.js ***!
  \*******************************************/
/*! exports provided: Offer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Offer", function() { return Offer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _offer_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./offer.scss */ "./components/common/offers/offer.scss");
/* harmony import */ var _offer_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_offer_scss__WEBPACK_IMPORTED_MODULE_1__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


class Offer extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return __jsx("section", {
      className: "sagicore-offers"
    }, __jsx("div", {
      className: "offer-container pb-5"
    }, __jsx("div", {
      className: "container"
    }, __jsx("div", {
      className: "offer-heading"
    }, __jsx("h4", {
      className: "text-center py-4"
    }, "Sagicore Offers")), __jsx("div", {
      className: "py-3"
    }, __jsx("div", {
      className: "d-flex justify-content-md-around justify-content-lg-around justify-content-sm-center justify-content-center flex-wrap"
    }, __jsx("div", {
      className: "card offer-card my-4"
    }, __jsx("div", {
      className: "card-body"
    }, __jsx("h5", {
      className: "card-title"
    }, __jsx("svg", {
      width: "70",
      height: "66",
      viewBox: "0 0 70 66",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M55.0841 26.9716V0.376465H14.9158V26.9716H0L35.0001 65.6234L70 26.9716H55.0841ZM9.22445 31.063H19.0073V4.46792H50.9926V31.063H60.7754L35.0001 59.5278L9.22445 31.063Z",
      fill: "white"
    }), __jsx("path", {
      d: "M39.3585 29.5516C38.3861 28.8153 37.8137 28.518 36.7216 27.9722L35.2378 27.2026C34.9791 27.061 34.6593 26.8412 34.2553 26.5632L34.2162 26.5364C33.5039 25.9799 33.3639 25.4963 33.3639 24.8515C33.3639 23.8882 34.0979 23.1047 35.0002 23.1047C35.9516 23.1047 36.6605 23.3864 37.469 24.8265L37.8247 25.4602L40.9091 23.4618L40.631 22.9145C39.7788 21.2371 38.5613 20.1601 37.0068 19.7057V17.1873H33.4353V19.6851C31.1339 20.3935 29.5713 22.5225 29.5713 25.0449C29.5713 26.6995 30.1002 27.94 31.2436 28.9561C32.2675 29.838 33.0344 30.3071 34.6518 31.0422C37.085 32.201 37.9061 32.993 37.9061 34.1802C37.9061 34.9342 37.7073 35.5597 37.3238 36.0287C36.8213 36.6181 36.1533 36.8928 35.221 36.8928C33.4648 36.8928 32.4881 35.8633 31.737 33.221L31.4926 32.3612L28.2185 34.3427L28.3157 34.8145C28.8948 37.6234 30.8319 39.7025 33.4351 40.3629V42.838H37.0067V40.3419C38.1813 40.0528 39.1962 39.4457 40.0273 38.534C41.1362 37.3325 41.6985 35.8491 41.6985 34.1248L41.6978 34.0956C41.6109 32.0671 40.9101 30.7073 39.3585 29.5516Z",
      fill: "white"
    }))), __jsx("p", {
      className: "card-text"
    }, "Lowest Cost to Access Premiere Fitness Solutions"))), __jsx("div", {
      className: "card offer-card my-4"
    }, __jsx("div", {
      className: "card-body"
    }, __jsx("h5", {
      className: "card-title"
    }, __jsx("svg", {
      width: "70",
      height: "70",
      viewBox: "0 0 70 70",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, "F", __jsx("path", {
      d: "M59.7613 10.242C46.107 -3.41361 23.8917 -3.41361 10.2401 10.2387C-3.41423 23.891 -3.41291 46.1069 10.2414 59.7619C23.8917 73.4135 46.107 73.4135 59.7599 59.7592C73.4129 46.1069 73.4123 23.8923 59.7613 10.242ZM55.9405 55.9411C44.3943 67.4873 25.6064 67.4886 14.0595 55.9424C2.51068 44.3942 2.512 25.6044 14.0595 14.0582C25.6057 2.51262 44.3929 2.5113 55.9418 14.0595C67.488 25.6057 67.4867 44.3955 55.9405 55.9411ZM21.9387 25.5232C21.9387 23.2633 23.7716 21.4305 26.0315 21.4305C28.2907 21.4305 30.1235 23.2627 30.1235 25.5232C30.1235 27.7844 28.2907 29.6166 26.0315 29.6166C23.7716 29.6166 21.9387 27.7844 21.9387 25.5232ZM40.4962 25.5232C40.4962 23.2633 42.3304 21.4305 44.5903 21.4305C46.8495 21.4305 48.6823 23.2627 48.6823 25.5232C48.6823 27.7844 46.8502 29.6166 44.5903 29.6166C42.3304 29.6166 40.4962 27.7844 40.4962 25.5232ZM50.172 42.2855C47.6356 48.151 41.6955 51.9408 35.0399 51.9408C28.2412 51.9408 22.2661 48.1319 19.8175 42.2366C19.3977 41.2275 19.8762 40.0685 20.8867 39.6487C21.1348 39.5464 21.3922 39.4976 21.6457 39.4976C22.4219 39.4976 23.1584 39.9563 23.4746 40.7186C25.3074 45.1314 29.847 47.9814 35.0399 47.9814C40.1154 47.9814 44.6286 45.1295 46.5373 40.714C46.9716 39.7101 48.1372 39.2475 49.1404 39.6824C50.1436 40.1174 50.6063 41.2823 50.172 42.2855Z",
      fill: "white"
    }))), __jsx("p", {
      className: "card-text"
    }, "First-Class Trainers and Classes"))), __jsx("div", {
      className: "card offer-card my-4"
    }, __jsx("div", {
      className: "card-body"
    }, __jsx("h5", {
      className: "card-title"
    }, __jsx("svg", {
      width: "70",
      height: "70",
      viewBox: "0 0 70 70",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M59.7613 10.242C46.107 -3.41361 23.8917 -3.41361 10.2401 10.2387C-3.41423 23.891 -3.41291 46.1069 10.2414 59.7619C23.8917 73.4135 46.107 73.4135 59.7599 59.7592C73.4129 46.1069 73.4123 23.8923 59.7613 10.242ZM55.9405 55.9411C44.3943 67.4873 25.6064 67.4886 14.0595 55.9424C2.51068 44.3942 2.512 25.6044 14.0595 14.0582C25.6057 2.51262 44.3929 2.5113 55.9418 14.0595C67.488 25.6057 67.4867 44.3955 55.9405 55.9411ZM21.9387 25.5232C21.9387 23.2633 23.7716 21.4305 26.0315 21.4305C28.2907 21.4305 30.1235 23.2627 30.1235 25.5232C30.1235 27.7844 28.2907 29.6166 26.0315 29.6166C23.7716 29.6166 21.9387 27.7844 21.9387 25.5232ZM40.4962 25.5232C40.4962 23.2633 42.3304 21.4305 44.5903 21.4305C46.8495 21.4305 48.6823 23.2627 48.6823 25.5232C48.6823 27.7844 46.8502 29.6166 44.5903 29.6166C42.3304 29.6166 40.4962 27.7844 40.4962 25.5232ZM50.172 42.2855C47.6356 48.151 41.6955 51.9408 35.0399 51.9408C28.2412 51.9408 22.2661 48.1319 19.8175 42.2366C19.3977 41.2275 19.8762 40.0685 20.8867 39.6487C21.1348 39.5464 21.3922 39.4976 21.6457 39.4976C22.4219 39.4976 23.1584 39.9563 23.4746 40.7186C25.3074 45.1314 29.847 47.9814 35.0399 47.9814C40.1154 47.9814 44.6286 45.1295 46.5373 40.714C46.9716 39.7101 48.1372 39.2475 49.1404 39.6824C50.1436 40.1174 50.6063 41.2823 50.172 42.2855Z",
      fill: "white"
    }))), __jsx("p", {
      className: "card-text"
    }, "Multiple Fitness Options"))), __jsx("div", {
      className: "card offer-card my-4"
    }, __jsx("div", {
      className: "card-body"
    }, __jsx("h5", {
      className: "card-title"
    }, __jsx("svg", {
      width: "70",
      height: "70",
      viewBox: "0 0 70 70",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M6.34961 0V70H63.6502V0H6.34961ZM59.5488 65.8984H10.4512V4.10156H22.7408V13.7174H47.2591V4.10156H59.5488V65.8984ZM43.1576 4.10156V9.61584H26.8424V4.10156H43.1576Z",
      fill: "white"
    }), __jsx("path", {
      d: "M26.6372 21.2826H51.7709V25.3842H26.6372V21.2826Z",
      fill: "white"
    }), __jsx("path", {
      d: "M18.229 21.2826H22.5355V25.3842H18.229V21.2826Z",
      fill: "white"
    }), __jsx("path", {
      d: "M26.6372 30.0326H51.7709V34.1342H26.6372V30.0326Z",
      fill: "white"
    }), __jsx("path", {
      d: "M18.229 30.0326H22.5355V34.1342H18.229V30.0326Z",
      fill: "white"
    }), __jsx("path", {
      d: "M18.229 38.7826H22.5355V42.8842H18.229V38.7826Z",
      fill: "white"
    }), __jsx("path", {
      d: "M26.6372 38.7826H51.7709V42.8842H26.6372V38.7826Z",
      fill: "white"
    }), __jsx("path", {
      d: "M18.229 51.9076H29.1665V56.0092H18.229V51.9076Z",
      fill: "white"
    }), __jsx("path", {
      d: "M40.8332 53.9748L37.9083 51.0499L35.0081 53.95L40.8332 59.7752L49.5749 51.0334L46.6748 48.1332L40.8332 53.9748Z",
      fill: "white"
    }))), __jsx("p", {
      className: "card-text"
    }, "Easy Enrollment & 24/7 Access"))), __jsx("div", {
      className: "card offer-card my-4"
    }, __jsx("div", {
      className: "card-body"
    }, __jsx("h5", {
      className: "card-title"
    }, __jsx("svg", {
      width: "70",
      height: "70",
      viewBox: "0 0 70 70",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M69.3111 59.2051L55.3809 45.2744L65.319 39.5337C66.0956 39.085 66.5476 38.2325 66.4828 37.338C66.4185 36.4433 65.8488 35.6644 65.0156 35.3323L20.6983 17.6544C19.8293 17.3074 18.8376 17.5118 18.176 18.1731C17.5144 18.8349 17.3101 19.8267 17.6568 20.6955L35.3321 65.0177C35.6644 65.8506 36.4434 66.4205 37.3381 66.4851C38.2327 66.5502 39.0852 66.0979 39.5338 65.321L45.2735 55.3829L59.2033 69.3139C59.6421 69.7532 60.2377 69.9997 60.8585 69.9997C61.4793 69.9997 62.0747 69.7532 62.5136 69.3139L69.3111 62.5156C70.2253 61.6014 70.2253 60.1194 69.3111 59.2051ZM60.8585 64.3486L46.4275 49.9161C45.9853 49.4739 45.3888 49.2302 44.7722 49.2302C44.6709 49.2302 44.5686 49.2368 44.467 49.2503C43.7451 49.345 43.1087 49.7702 42.745 50.4007L37.8936 58.8012L24.0235 24.0218L58.7993 37.8938L50.3981 42.7462C49.7679 43.1098 49.3427 43.7462 49.248 44.4681C49.153 45.1893 49.3988 45.9138 49.9136 46.4286L64.3453 60.8611L60.8585 64.3486Z",
      fill: "white"
    }), __jsx("path", {
      d: "M10.2786 6.96506C9.36416 6.05152 7.88261 6.05152 6.96766 6.96506C6.05342 7.87954 6.05342 9.36156 6.96766 10.276L12.1691 15.4775C12.6261 15.9343 13.2255 16.1631 13.8246 16.1631C14.4235 16.1631 15.0224 15.9343 15.4799 15.4775C16.3939 14.5632 16.3939 13.081 15.4799 12.1667L10.2786 6.96506Z",
      fill: "white"
    }), __jsx("path", {
      d: "M12.0412 23.1483C12.0412 21.8555 10.9928 20.8076 9.70005 20.8076H2.34416C1.05164 20.8076 0.00305176 21.8553 0.00305176 23.1483C0.00305176 24.4408 1.0514 25.4891 2.34416 25.4891H9.70005C10.9928 25.4889 12.0412 24.4408 12.0412 23.1483Z",
      fill: "white"
    }), __jsx("path", {
      d: "M11.0922 30.3734L5.89033 35.5753C4.97585 36.4891 4.97585 37.9713 5.89033 38.8855C6.34733 39.3426 6.94598 39.5712 7.54558 39.5712C8.14447 39.5712 8.74336 39.3426 9.20036 38.8855L14.4025 33.6839C15.3168 32.7699 15.3168 31.2874 14.4025 30.3734C13.4887 29.4594 12.0067 29.4594 11.0922 30.3734Z",
      fill: "white"
    }), __jsx("path", {
      d: "M23.15 12.0386C24.4425 12.0386 25.4908 10.9902 25.4908 9.69747V2.34088C25.4908 1.04835 24.4427 0 23.15 0C21.8572 0 20.8093 1.04811 20.8093 2.34088V9.69771C20.8093 10.9902 21.857 12.0386 23.15 12.0386Z",
      fill: "white"
    }), __jsx("path", {
      d: "M32.0287 15.0853C32.6283 15.0853 33.2272 14.8571 33.6842 14.3999L38.8854 9.19846C39.7999 8.28422 39.7999 6.80196 38.8854 5.88795C37.9714 4.97371 36.4887 4.97371 35.5751 5.88795L30.3737 11.0894C29.4594 12.0034 29.4594 13.4859 30.3737 14.3999C30.8307 14.8571 31.4298 15.0853 32.0287 15.0853Z",
      fill: "white"
    }))), __jsx("p", {
      className: "card-text"
    }, "No Judgement or Pressure"))), __jsx("div", {
      className: "card offer-card my-4 "
    }, __jsx("div", {
      className: "card-body"
    }, __jsx("h5", {
      className: "card-title"
    }, __jsx("svg", {
      width: "70",
      height: "70",
      viewBox: "0 0 70 70",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M37.0406 34.1183V1.96752C37.0406 0.874454 36.1661 0 35.0731 0C33.98 0 33.1055 0.874454 33.1055 1.96752V34.1183C28.5438 35.0364 25.0897 39.0735 25.0897 43.9121C25.0897 48.7508 28.5438 52.7878 33.1055 53.706V68.0179C33.1055 69.111 33.98 69.9854 35.0731 69.9854C36.1661 69.9854 37.0406 69.111 37.0406 68.0179V53.706C41.6023 52.7878 45.0564 48.7508 45.0564 43.9121C45.0564 39.0881 41.6169 35.0364 37.0406 34.1183ZM35.0731 49.9604C31.7356 49.9604 29.0248 47.2496 29.0248 43.9121C29.0248 40.5746 31.7356 37.8638 35.0731 37.8638C38.4106 37.8638 41.1214 40.5746 41.1214 43.9121C41.1214 47.2496 38.4106 49.9604 35.0731 49.9604Z",
      fill: "white"
    }), __jsx("path", {
      d: "M12.9493 17.6202V1.96752C12.9493 0.874454 12.0748 0 10.9818 0C9.88869 0 9.01424 0.874454 9.01424 1.96752V17.6202C4.4525 18.5384 0.998413 22.5755 0.998413 27.4141C0.998413 32.2528 4.4525 36.2898 9.01424 37.208V68.0179C9.01424 69.111 9.88869 69.9854 10.9818 69.9854C12.0748 69.9854 12.9493 69.111 12.9493 68.0179V37.1934C17.511 36.2752 20.9651 32.2382 20.9651 27.3995C20.9651 22.5609 17.511 18.5384 12.9493 17.6202ZM10.9818 33.4624C7.64426 33.4624 4.93345 30.7516 4.93345 27.4141C4.93345 24.0766 7.64426 21.3658 10.9818 21.3658C14.3193 21.3658 17.0301 24.0766 17.0301 27.4141C17.0301 30.7516 14.3047 33.4624 10.9818 33.4624Z",
      fill: "white"
    }), __jsx("path", {
      d: "M60.9859 17.6202V1.96752C60.9859 0.874454 60.1114 0 59.0184 0C57.9253 0 57.0509 0.874454 57.0509 1.96752V17.6202C52.4891 18.5384 49.035 22.5755 49.035 27.4141C49.035 32.2528 52.4891 36.2898 57.0509 37.208V68.0325C57.0509 69.1255 57.9253 70 59.0184 70C60.1114 70 60.9859 69.1255 60.9859 68.0325V37.1934C65.5476 36.2752 69.0017 32.2382 69.0017 27.3995C69.0017 22.5609 65.5622 18.5384 60.9859 17.6202ZM59.0184 33.4624C55.6809 33.4624 52.9701 30.7516 52.9701 27.4141C52.9701 24.0766 55.6809 21.3658 59.0184 21.3658C62.3559 21.3658 65.0667 24.0766 65.0667 27.4141C65.0667 30.7516 62.3559 33.4624 59.0184 33.4624Z",
      fill: "white"
    }))), __jsx("p", {
      className: "card-text"
    }, "Fun Environment with Peers"))))))));
  }

}

/***/ }),

/***/ "./components/common/offers/offer.scss":
/*!*********************************************!*\
  !*** ./components/common/offers/offer.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/otherprograms/otherprograms.js":
/*!**********************************************************!*\
  !*** ./components/common/otherprograms/otherprograms.js ***!
  \**********************************************************/
/*! exports provided: Otherprograms */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Otherprograms", function() { return Otherprograms; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _otherprograms_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./otherprograms.scss */ "./components/common/otherprograms/otherprograms.scss");
/* harmony import */ var _otherprograms_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_otherprograms_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _static_images_Rectangle1_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../static/images/Rectangle1.png */ "./static/images/Rectangle1.png");
/* harmony import */ var _static_images_Rectangle1_png__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_static_images_Rectangle1_png__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _static_images_Rectangle2_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../static/images/Rectangle2.png */ "./static/images/Rectangle2.png");
/* harmony import */ var _static_images_Rectangle2_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_static_images_Rectangle2_png__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _static_images_Rectangle3_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../static/images/Rectangle3.png */ "./static/images/Rectangle3.png");
/* harmony import */ var _static_images_Rectangle3_png__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_static_images_Rectangle3_png__WEBPACK_IMPORTED_MODULE_4__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;





class Otherprograms extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return __jsx("section", {
      className: "other-programs"
    }, __jsx("div", {
      className: "my-3"
    }, __jsx("div", {
      className: "container"
    }, __jsx("h4", {
      className: "text-center otherPrograms-heading py-4"
    }, "Other Programs"), __jsx("div", {
      className: "card-deck "
    }, __jsx("div", {
      className: "card otherprogram-card",
      id: "card"
    }, __jsx("img", {
      className: "card-img-top",
      src: _static_images_Rectangle1_png__WEBPACK_IMPORTED_MODULE_2___default.a,
      alt: ""
    }), __jsx("div", {
      className: "white-box mx-auto"
    }, __jsx("p", {
      className: "text-center py-4 px-2 mb-0"
    }, " 365 fitness"))), __jsx("div", {
      className: "card otherprogram-card",
      id: "card"
    }, __jsx("img", {
      className: "card-img-top",
      src: _static_images_Rectangle2_png__WEBPACK_IMPORTED_MODULE_3___default.a,
      alt: ""
    }), __jsx("div", {
      className: "white-box mx-auto"
    }, __jsx("p", {
      className: "text-center py-4 px-2 mb-0"
    }, "nutritionisist services"))), __jsx("div", {
      className: "card otherprogram-card",
      id: "card"
    }, __jsx("img", {
      className: "card-img-top",
      src: _static_images_Rectangle3_png__WEBPACK_IMPORTED_MODULE_4___default.a,
      alt: ""
    }), __jsx("div", {
      className: "white-box mx-auto"
    }, __jsx("p", {
      className: "text-center py-4 px-2 mb-0"
    }, "express events")))))));
  }

}

/***/ }),

/***/ "./components/common/otherprograms/otherprograms.scss":
/*!************************************************************!*\
  !*** ./components/common/otherprograms/otherprograms.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/pricingfitness/pricingfitness.js":
/*!************************************************************!*\
  !*** ./components/common/pricingfitness/pricingfitness.js ***!
  \************************************************************/
/*! exports provided: Pricingfitness */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Pricingfitness", function() { return Pricingfitness; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _pricingfitness_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pricingfitness.scss */ "./components/common/pricingfitness/pricingfitness.scss");
/* harmony import */ var _pricingfitness_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_pricingfitness_scss__WEBPACK_IMPORTED_MODULE_1__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


class Pricingfitness extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return __jsx("section", null, __jsx("div", {
      className: "bg-primarys"
    }, __jsx("div", {
      className: "container py-5"
    }, __jsx("div", {
      className: "pricing-heading"
    }, "EXPRESS FITNESS"), __jsx("p", {
      className: "pricing-para py-2"
    }, "Current fee : $10000/person"), __jsx("div", {
      className: "table-responsive"
    }, __jsx("table", {
      className: "table table-bordered table-hover"
    }, __jsx("thead", null, __jsx("tr", null, __jsx("th", {
      scope: "col"
    }, "TIER"), __jsx("th", {
      scope: "col"
    }, "PARTICIPANTS"), __jsx("th", {
      scope: "col"
    }, "FEE/MEMBER"), __jsx("th", {
      scope: "col"
    }, "DISCOUNT"))), __jsx("tbody", null, __jsx("tr", null, __jsx("th", {
      scope: "row"
    }, "I"), __jsx("td", null, "25-50 MEMBERS"), __jsx("td", null, "5000"), __jsx("td", null, "50%")), __jsx("tr", null, __jsx("th", {
      scope: "row"
    }, "II"), __jsx("td", null, "50-200 MEMBERS"), __jsx("td", null, "4750"), __jsx("td", null, "53%")), __jsx("tr", null, __jsx("th", {
      scope: "row"
    }, "III"), __jsx("td", null, "200-400 MEMBERS"), __jsx("td", null, "4500"), __jsx("td", null, "55%")), __jsx("tr", null, __jsx("th", {
      scope: "row"
    }, "IV"), __jsx("td", null, "200 MEMBERS"), __jsx("td", null, "4250"), __jsx("td", null, "58%"))))), __jsx("div", {
      className: "pricing-heading pt-5"
    }, "365 FITNESS"), __jsx("p", {
      className: "pricing-para"
    }, "Current fee : $7000/person"), __jsx("div", {
      className: "table-responsive"
    }, __jsx("table", {
      className: "table table-bordered"
    }, __jsx("thead", null, __jsx("tr", null, __jsx("th", {
      scope: "col"
    }, "TIER"), __jsx("th", {
      scope: "col"
    }, "PARTICIPANTS"), __jsx("th", {
      scope: "col"
    }, "FEE/MEMBER"), __jsx("th", {
      scope: "col"
    }, "DISCOUNT"))), __jsx("tbody", null, __jsx("tr", null, __jsx("th", {
      scope: "row"
    }, "I"), __jsx("td", null, "25-50 MEMBERS"), __jsx("td", null, "4000"), __jsx("td", null, "40%")), __jsx("tr", null, __jsx("th", {
      scope: "row"
    }, "II"), __jsx("td", null, "50-200 MEMBERS"), __jsx("td", null, "3750"), __jsx("td", null, "43%")), __jsx("tr", null, __jsx("th", {
      scope: "row"
    }, "III"), __jsx("td", null, "200-400 MEMBERS"), __jsx("td", null, "3500"), __jsx("td", null, "45%")), __jsx("tr", null, __jsx("th", {
      scope: "row"
    }, "IV"), __jsx("td", null, "200 MEMBERS"), __jsx("td", null, "3250"), __jsx("td", null, "48%"))))))));
  }

}

/***/ }),

/***/ "./components/common/pricingfitness/pricingfitness.scss":
/*!**************************************************************!*\
  !*** ./components/common/pricingfitness/pricingfitness.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/testimonials/testimonial.js":
/*!*******************************************************!*\
  !*** ./components/common/testimonials/testimonial.js ***!
  \*******************************************************/
/*! exports provided: Testimonial */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Testimonial", function() { return Testimonial; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _testimonial_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./testimonial.scss */ "./components/common/testimonials/testimonial.scss");
/* harmony import */ var _testimonial_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_testimonial_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-id-swiper */ "react-id-swiper");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var swiper_css_swiper_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! swiper/css/swiper.css */ "./node_modules/swiper/css/swiper.css");
/* harmony import */ var swiper_css_swiper_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(swiper_css_swiper_css__WEBPACK_IMPORTED_MODULE_3__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const params = {
  effect: 'coverflow',
  grabCursor: true,
  centeredSlides: false,
  loop: true,
  coverflowEffect: {
    rotate: 0,
    stretch: 10,
    depth: 200,
    modifier: 1,
    slideShadows: false
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  },
  breakpoints: {
    640: {
      slidesPerView: 1
    },
    768: {
      slidesPerView: 3
    },
    1024: {
      slidesPerView: 3
    }
  }
};
class Testimonial extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return __jsx("section", {
      className: "testimonial"
    }, __jsx("div", {
      className: "testimonial-main-page"
    }, __jsx("div", {
      className: "testimonial-heading"
    }, __jsx("h3", null, "Testimonials")), __jsx("div", {
      className: "testimonial-page"
    }, __jsx(react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default.a, params, __jsx("div", {
      className: "testimonial-card"
    }, __jsx("h6", {
      className: "text-center"
    }, __jsx("i", {
      className: "fas fa-quote-left p-3"
    })), __jsx("p", {
      className: "testimonial-content"
    }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit eiusmod tempor incididunt ut labore dolore magna aliqua. Ut enim ad sit minim veniam nostrud exercitation ullamco laboris nisi ut commodo consequat."), __jsx("p", {
      className: "testimonial-person-name mb-0"
    }, "John Doe"), __jsx("p", {
      className: "testimonial-person-company-name"
    }, "Amazon")), __jsx("div", {
      className: "testimonial-card"
    }, __jsx("h6", {
      className: "text-center"
    }, __jsx("i", {
      className: "fas fa-quote-left p-3"
    })), __jsx("p", {
      className: "testimonial-content"
    }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit eiusmod tempor incididunt ut labore dolore magna aliqua. Ut enim ad sit minim veniam nostrud exercitation ullamco laboris nisi ut commodo consequat."), __jsx("p", {
      className: "testimonial-person-name mb-0"
    }, "John Doe"), __jsx("p", {
      className: "testimonial-person-company-name"
    }, "Amazon")), __jsx("div", {
      className: "testimonial-card"
    }, __jsx("h6", {
      className: "text-center"
    }, __jsx("i", {
      className: "fas fa-quote-left p-3"
    })), __jsx("p", {
      className: "testimonial-content"
    }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit eiusmod tempor incididunt ut labore dolore magna aliqua. Ut enim ad sit minim veniam nostrud exercitation ullamco laboris nisi ut commodo consequat."), __jsx("p", {
      className: "testimonial-person-name mb-0"
    }, "John Doe"), __jsx("p", {
      className: "testimonial-person-company-name"
    }, "Amazon"))))));
  }

}

/***/ }),

/***/ "./components/common/testimonials/testimonial.scss":
/*!*********************************************************!*\
  !*** ./components/common/testimonials/testimonial.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/common/whatweoffer/whatweoffer.js":
/*!******************************************************!*\
  !*** ./components/common/whatweoffer/whatweoffer.js ***!
  \******************************************************/
/*! exports provided: Whatweoffer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Whatweoffer", function() { return Whatweoffer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _whatweoffer_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./whatweoffer.scss */ "./components/common/whatweoffer/whatweoffer.scss");
/* harmony import */ var _whatweoffer_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_whatweoffer_scss__WEBPACK_IMPORTED_MODULE_1__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


class Whatweoffer extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return __jsx("section", {
      className: "feature-offer-container"
    }, __jsx("div", {
      className: "f-offer-container pb-5"
    }, __jsx("div", {
      className: "container"
    }, __jsx("div", {
      className: "f-offer-heading"
    }, __jsx("h4", {
      className: "text-center pt-5 pb-3"
    }, "What We Offers")), __jsx("div", {
      className: "py-3"
    }, __jsx("div", {
      className: "d-flex justify-content-md-around justify-content-lg-around justify-content-sm-center justify-content-center flex-wrap"
    }, __jsx("div", {
      className: "card f-offer-card my-4"
    }, __jsx("div", {
      className: "card-body"
    }, __jsx("h5", {
      className: "f-card-title"
    }, __jsx("svg", {
      width: "70",
      height: "66",
      viewBox: "0 0 70 66",
      fill: "#008CE6",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M55.0841 26.9716V0.376465H14.9158V26.9716H0L35.0001 65.6234L70 26.9716H55.0841ZM9.22445 31.063H19.0073V4.46792H50.9926V31.063H60.7754L35.0001 59.5278L9.22445 31.063Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M39.3585 29.5516C38.3861 28.8153 37.8137 28.518 36.7216 27.9722L35.2378 27.2026C34.9791 27.061 34.6593 26.8412 34.2553 26.5632L34.2162 26.5364C33.5039 25.9799 33.3639 25.4963 33.3639 24.8515C33.3639 23.8882 34.0979 23.1047 35.0002 23.1047C35.9516 23.1047 36.6605 23.3864 37.469 24.8265L37.8247 25.4602L40.9091 23.4618L40.631 22.9145C39.7788 21.2371 38.5613 20.1601 37.0068 19.7057V17.1873H33.4353V19.6851C31.1339 20.3935 29.5713 22.5225 29.5713 25.0449C29.5713 26.6995 30.1002 27.94 31.2436 28.9561C32.2675 29.838 33.0344 30.3071 34.6518 31.0422C37.085 32.201 37.9061 32.993 37.9061 34.1802C37.9061 34.9342 37.7073 35.5597 37.3238 36.0287C36.8213 36.6181 36.1533 36.8928 35.221 36.8928C33.4648 36.8928 32.4881 35.8633 31.737 33.221L31.4926 32.3612L28.2185 34.3427L28.3157 34.8145C28.8948 37.6234 30.8319 39.7025 33.4351 40.3629V42.838H37.0067V40.3419C38.1813 40.0528 39.1962 39.4457 40.0273 38.534C41.1362 37.3325 41.6985 35.8491 41.6985 34.1248L41.6978 34.0956C41.6109 32.0671 40.9101 30.7073 39.3585 29.5516Z",
      fill: "#666666"
    }))), __jsx("p", {
      className: "f-card-text"
    }, "Lowest Cost to Access Premiere Fitness Solutions"))), __jsx("div", {
      className: "card f-offer-card my-4"
    }, __jsx("div", {
      className: "card-body"
    }, __jsx("h5", {
      className: "f-card-title"
    }, __jsx("svg", {
      width: "70",
      height: "70",
      viewBox: "0 0 70 70",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M6.34961 0V70H63.6502V0H6.34961ZM59.5488 65.8984H10.4512V4.10156H22.7408V13.7174H47.2591V4.10156H59.5488V65.8984ZM43.1576 4.10156V9.61584H26.8424V4.10156H43.1576Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M26.6372 21.2826H51.7709V25.3842H26.6372V21.2826Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M18.229 21.2826H22.5355V25.3842H18.229V21.2826Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M26.6372 30.0326H51.7709V34.1342H26.6372V30.0326Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M18.229 30.0326H22.5355V34.1342H18.229V30.0326Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M18.229 38.7826H22.5355V42.8842H18.229V38.7826Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M26.6372 38.7826H51.7709V42.8842H26.6372V38.7826Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M18.229 51.9076H29.1665V56.0092H18.229V51.9076Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M40.8332 53.9748L37.9083 51.0499L35.0081 53.95L40.8332 59.7752L49.5749 51.0334L46.6748 48.1332L40.8332 53.9748Z",
      fill: "#666666"
    }))), __jsx("p", {
      className: "f-card-text"
    }, "First-Class Trainers and Classes"))), __jsx("div", {
      className: "card f-offer-card my-4"
    }, __jsx("div", {
      className: "card-body"
    }, __jsx("h5", {
      className: "f-card-title"
    }, __jsx("svg", {
      width: "70",
      height: "70",
      viewBox: "0 0 70 70",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M59.7613 10.242C46.107 -3.41361 23.8917 -3.41361 10.2401 10.2387C-3.41423 23.891 -3.41291 46.1069 10.2414 59.7619C23.8917 73.4135 46.107 73.4135 59.7599 59.7592C73.4129 46.1069 73.4123 23.8923 59.7613 10.242ZM55.9405 55.9411C44.3943 67.4873 25.6064 67.4886 14.0595 55.9424C2.51068 44.3942 2.512 25.6044 14.0595 14.0582C25.6057 2.51262 44.3929 2.5113 55.9418 14.0595C67.488 25.6057 67.4867 44.3955 55.9405 55.9411ZM21.9387 25.5232C21.9387 23.2633 23.7716 21.4305 26.0315 21.4305C28.2907 21.4305 30.1235 23.2627 30.1235 25.5232C30.1235 27.7844 28.2907 29.6166 26.0315 29.6166C23.7716 29.6166 21.9387 27.7844 21.9387 25.5232ZM40.4962 25.5232C40.4962 23.2633 42.3304 21.4305 44.5903 21.4305C46.8495 21.4305 48.6823 23.2627 48.6823 25.5232C48.6823 27.7844 46.8502 29.6166 44.5903 29.6166C42.3304 29.6166 40.4962 27.7844 40.4962 25.5232ZM50.172 42.2855C47.6356 48.151 41.6955 51.9408 35.0399 51.9408C28.2412 51.9408 22.2661 48.1319 19.8175 42.2366C19.3977 41.2275 19.8762 40.0685 20.8867 39.6487C21.1348 39.5464 21.3922 39.4976 21.6457 39.4976C22.4219 39.4976 23.1584 39.9563 23.4746 40.7186C25.3074 45.1314 29.847 47.9814 35.0399 47.9814C40.1154 47.9814 44.6286 45.1295 46.5373 40.714C46.9716 39.7101 48.1372 39.2475 49.1404 39.6824C50.1436 40.1174 50.6063 41.2823 50.172 42.2855Z",
      fill: "#666666"
    }))), __jsx("p", {
      className: "f-card-text"
    }, "Multiple Fitness Options"))), __jsx("div", {
      className: "card f-offer-card my-4"
    }, __jsx("div", {
      className: "card-body"
    }, __jsx("h5", {
      className: "f-card-title"
    }, __jsx("svg", {
      width: "70",
      height: "70",
      viewBox: "0 0 70 70",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M6.34961 0V70H63.6502V0H6.34961ZM59.5488 65.8984H10.4512V4.10156H22.7408V13.7174H47.2591V4.10156H59.5488V65.8984ZM43.1576 4.10156V9.61584H26.8424V4.10156H43.1576Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M26.6372 21.2826H51.7709V25.3842H26.6372V21.2826Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M18.229 21.2826H22.5355V25.3842H18.229V21.2826Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M26.6372 30.0326H51.7709V34.1342H26.6372V30.0326Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M18.229 30.0326H22.5355V34.1342H18.229V30.0326Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M18.229 38.7826H22.5355V42.8842H18.229V38.7826Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M26.6372 38.7826H51.7709V42.8842H26.6372V38.7826Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M18.229 51.9076H29.1665V56.0092H18.229V51.9076Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M40.8332 53.9748L37.9083 51.0499L35.0081 53.95L40.8332 59.7752L49.5749 51.0334L46.6748 48.1332L40.8332 53.9748Z",
      fill: "#666666"
    }))), __jsx("p", {
      className: "f-card-text"
    }, "Easy Enrollment & 24/7 Access"))), __jsx("div", {
      className: "card f-offer-card my-4"
    }, __jsx("div", {
      className: "card-body"
    }, __jsx("h5", {
      className: "f-card-title"
    }, __jsx("svg", {
      width: "70",
      height: "70",
      viewBox: "0 0 70 70",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M69.3111 59.2051L55.3809 45.2744L65.319 39.5337C66.0956 39.085 66.5476 38.2325 66.4828 37.338C66.4185 36.4433 65.8488 35.6644 65.0156 35.3323L20.6983 17.6544C19.8293 17.3074 18.8376 17.5118 18.176 18.1731C17.5144 18.8349 17.3101 19.8267 17.6568 20.6955L35.3321 65.0177C35.6644 65.8506 36.4434 66.4205 37.3381 66.4851C38.2327 66.5502 39.0852 66.0979 39.5338 65.321L45.2735 55.3829L59.2033 69.3139C59.6421 69.7532 60.2377 69.9997 60.8585 69.9997C61.4793 69.9997 62.0747 69.7532 62.5136 69.3139L69.3111 62.5156C70.2253 61.6014 70.2253 60.1194 69.3111 59.2051ZM60.8585 64.3486L46.4275 49.9161C45.9853 49.4739 45.3888 49.2302 44.7722 49.2302C44.6709 49.2302 44.5686 49.2368 44.467 49.2503C43.7451 49.345 43.1087 49.7702 42.745 50.4007L37.8936 58.8012L24.0235 24.0218L58.7993 37.8938L50.3981 42.7462C49.7679 43.1098 49.3427 43.7462 49.248 44.4681C49.153 45.1893 49.3988 45.9138 49.9136 46.4286L64.3453 60.8611L60.8585 64.3486Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M10.2786 6.96506C9.36416 6.05152 7.88261 6.05152 6.96766 6.96506C6.05342 7.87954 6.05342 9.36156 6.96766 10.276L12.1691 15.4775C12.6261 15.9343 13.2255 16.1631 13.8246 16.1631C14.4235 16.1631 15.0224 15.9343 15.4799 15.4775C16.3939 14.5632 16.3939 13.081 15.4799 12.1667L10.2786 6.96506Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M12.0412 23.1483C12.0412 21.8555 10.9928 20.8076 9.70005 20.8076H2.34416C1.05164 20.8076 0.00305176 21.8553 0.00305176 23.1483C0.00305176 24.4408 1.0514 25.4891 2.34416 25.4891H9.70005C10.9928 25.4889 12.0412 24.4408 12.0412 23.1483Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M11.0922 30.3734L5.89033 35.5753C4.97585 36.4891 4.97585 37.9713 5.89033 38.8855C6.34733 39.3426 6.94598 39.5712 7.54558 39.5712C8.14447 39.5712 8.74336 39.3426 9.20036 38.8855L14.4025 33.6839C15.3168 32.7699 15.3168 31.2874 14.4025 30.3734C13.4887 29.4594 12.0067 29.4594 11.0922 30.3734Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M23.15 12.0386C24.4425 12.0386 25.4908 10.9902 25.4908 9.69747V2.34088C25.4908 1.04835 24.4427 0 23.15 0C21.8572 0 20.8093 1.04811 20.8093 2.34088V9.69771C20.8093 10.9902 21.857 12.0386 23.15 12.0386Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M32.0287 15.0853C32.6283 15.0853 33.2272 14.8571 33.6842 14.3999L38.8854 9.19846C39.7999 8.28422 39.7999 6.80196 38.8854 5.88795C37.9714 4.97371 36.4887 4.97371 35.5751 5.88795L30.3737 11.0894C29.4594 12.0034 29.4594 13.4859 30.3737 14.3999C30.8307 14.8571 31.4298 15.0853 32.0287 15.0853Z",
      fill: "#666666"
    }))), __jsx("p", {
      className: "f-card-text"
    }, "No Judgement or Pressure"))), __jsx("div", {
      className: "card f-offer-card my-4"
    }, __jsx("div", {
      className: "card-body"
    }, __jsx("h5", {
      className: "f-card-title"
    }, __jsx("svg", {
      width: "70",
      height: "70",
      viewBox: "0 0 70 70",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, __jsx("path", {
      d: "M37.0406 34.1183V1.96752C37.0406 0.874454 36.1661 0 35.0731 0C33.98 0 33.1055 0.874454 33.1055 1.96752V34.1183C28.5438 35.0364 25.0897 39.0735 25.0897 43.9121C25.0897 48.7508 28.5438 52.7878 33.1055 53.706V68.0179C33.1055 69.111 33.98 69.9854 35.0731 69.9854C36.1661 69.9854 37.0406 69.111 37.0406 68.0179V53.706C41.6023 52.7878 45.0564 48.7508 45.0564 43.9121C45.0564 39.0881 41.6169 35.0364 37.0406 34.1183ZM35.0731 49.9604C31.7356 49.9604 29.0248 47.2496 29.0248 43.9121C29.0248 40.5746 31.7356 37.8638 35.0731 37.8638C38.4106 37.8638 41.1214 40.5746 41.1214 43.9121C41.1214 47.2496 38.4106 49.9604 35.0731 49.9604Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M12.9493 17.6202V1.96752C12.9493 0.874454 12.0748 0 10.9818 0C9.88869 0 9.01424 0.874454 9.01424 1.96752V17.6202C4.4525 18.5384 0.998413 22.5755 0.998413 27.4141C0.998413 32.2528 4.4525 36.2898 9.01424 37.208V68.0179C9.01424 69.111 9.88869 69.9854 10.9818 69.9854C12.0748 69.9854 12.9493 69.111 12.9493 68.0179V37.1934C17.511 36.2752 20.9651 32.2382 20.9651 27.3995C20.9651 22.5609 17.511 18.5384 12.9493 17.6202ZM10.9818 33.4624C7.64426 33.4624 4.93345 30.7516 4.93345 27.4141C4.93345 24.0766 7.64426 21.3658 10.9818 21.3658C14.3193 21.3658 17.0301 24.0766 17.0301 27.4141C17.0301 30.7516 14.3047 33.4624 10.9818 33.4624Z",
      fill: "#666666"
    }), __jsx("path", {
      d: "M60.9859 17.6202V1.96752C60.9859 0.874454 60.1114 0 59.0184 0C57.9253 0 57.0509 0.874454 57.0509 1.96752V17.6202C52.4891 18.5384 49.035 22.5755 49.035 27.4141C49.035 32.2528 52.4891 36.2898 57.0509 37.208V68.0325C57.0509 69.1255 57.9253 70 59.0184 70C60.1114 70 60.9859 69.1255 60.9859 68.0325V37.1934C65.5476 36.2752 69.0017 32.2382 69.0017 27.3995C69.0017 22.5609 65.5622 18.5384 60.9859 17.6202ZM59.0184 33.4624C55.6809 33.4624 52.9701 30.7516 52.9701 27.4141C52.9701 24.0766 55.6809 21.3658 59.0184 21.3658C62.3559 21.3658 65.0667 24.0766 65.0667 27.4141C65.0667 30.7516 62.3559 33.4624 59.0184 33.4624Z",
      fill: "#666666"
    }))), __jsx("p", {
      className: "f-card-text"
    }, "Fun Environment with Peers"))))))));
  }

}

/***/ }),

/***/ "./components/common/whatweoffer/whatweoffer.scss":
/*!********************************************************!*\
  !*** ./components/common/whatweoffer/whatweoffer.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./layout/primarylayout.jsx":
/*!**********************************!*\
  !*** ./layout/primarylayout.jsx ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_common___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/common/ */ "./components/common/index.js");
/* harmony import */ var _assets_scss_main_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../assets/scss/main.scss */ "./assets/scss/main.scss");
/* harmony import */ var _assets_scss_main_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_assets_scss_main_scss__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const Primarylayout = ({
  children
}) => __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, __jsx("div", {
  className: "sticky"
}, __jsx(_components_common___WEBPACK_IMPORTED_MODULE_1__["Infoheader"], null), __jsx(_components_common___WEBPACK_IMPORTED_MODULE_1__["Header"], null)), children, __jsx(_components_common___WEBPACK_IMPORTED_MODULE_1__["Footer"], null));

/* harmony default export */ __webpack_exports__["default"] = (Primarylayout);

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/map.js":
/*!************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/map.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/map */ "core-js/library/fn/map");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/assign.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/assign */ "core-js/library/fn/object/assign");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/create.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/create.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/create */ "core-js/library/fn/object/create");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/define-property */ "core-js/library/fn/object/define-property");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js":
/*!*******************************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/get-own-property-descriptor */ "core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js":
/*!********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/keys */ "core-js/library/fn/object/keys");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/promise.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/promise.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/promise */ "core-js/library/fn/promise");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/symbol.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/symbol.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/symbol */ "core-js/library/fn/symbol");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/symbol/iterator.js":
/*!************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/symbol/iterator.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/symbol/iterator */ "core-js/library/fn/symbol/iterator");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/weak-map.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/weak-map.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/weak-map */ "core-js/library/fn/weak-map");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _defineProperty; });
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);

function _defineProperty(obj, key, value) {
  if (key in obj) {
    _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/extends.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/extends.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Object$assign = __webpack_require__(/*! ../core-js/object/assign */ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js");

function _extends() {
  module.exports = _extends = _Object$assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

module.exports = _extends;

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js":
/*!******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Object$getOwnPropertyDescriptor = __webpack_require__(/*! ../core-js/object/get-own-property-descriptor */ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js");

var _Object$defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

var _typeof = __webpack_require__(/*! ../helpers/typeof */ "./node_modules/@babel/runtime-corejs2/helpers/typeof.js");

var _WeakMap = __webpack_require__(/*! ../core-js/weak-map */ "./node_modules/@babel/runtime-corejs2/core-js/weak-map.js");

function _getRequireWildcardCache() {
  if (typeof _WeakMap !== "function") return null;
  var cache = new _WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = _Object$defineProperty && _Object$getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? _Object$getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        _Object$defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/typeof.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/typeof.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Symbol$iterator = __webpack_require__(/*! ../core-js/symbol/iterator */ "./node_modules/@babel/runtime-corejs2/core-js/symbol/iterator.js");

var _Symbol = __webpack_require__(/*! ../core-js/symbol */ "./node_modules/@babel/runtime-corejs2/core-js/symbol.js");

function _typeof2(obj) { if (typeof _Symbol === "function" && typeof _Symbol$iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof _Symbol === "function" && obj.constructor === _Symbol && obj !== _Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _typeof(obj) {
  if (typeof _Symbol === "function" && _typeof2(_Symbol$iterator) === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return _typeof2(obj);
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof _Symbol === "function" && obj.constructor === _Symbol && obj !== _Symbol.prototype ? "symbol" : _typeof2(obj);
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "./node_modules/next/dist/client/link.js":
/*!***********************************************!*\
  !*** ./node_modules/next/dist/client/link.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireWildcard */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.default = void 0;

var _map = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/core-js/map */ "./node_modules/@babel/runtime-corejs2/core-js/map.js"));

var _url = __webpack_require__(/*! url */ "url");

var _react = _interopRequireWildcard(__webpack_require__(/*! react */ "react"));

var _propTypes = _interopRequireDefault(__webpack_require__(/*! prop-types */ "prop-types"));

var _router = _interopRequireDefault(__webpack_require__(/*! ./router */ "./node_modules/next/dist/client/router.js"));

var _rewriteUrlForExport = __webpack_require__(/*! ../next-server/lib/router/rewrite-url-for-export */ "./node_modules/next/dist/next-server/lib/router/rewrite-url-for-export.js");

var _utils = __webpack_require__(/*! ../next-server/lib/utils */ "./node_modules/next/dist/next-server/lib/utils.js");

function isLocal(href) {
  var url = (0, _url.parse)(href, false, true);
  var origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

function memoizedFormatUrl(formatFunc) {
  var lastHref = null;
  var lastAs = null;
  var lastResult = null;
  return (href, as) => {
    if (lastResult && href === lastHref && as === lastAs) {
      return lastResult;
    }

    var result = formatFunc(href, as);
    lastHref = href;
    lastAs = as;
    lastResult = result;
    return result;
  };
}

function formatUrl(url) {
  return url && typeof url === 'object' ? (0, _utils.formatWithValidation)(url) : url;
}

var observer;
var listeners = new _map.default();
var IntersectionObserver = false ? undefined : null;

function getObserver() {
  // Return shared instance of IntersectionObserver if already created
  if (observer) {
    return observer;
  } // Only create shared IntersectionObserver if supported in browser


  if (!IntersectionObserver) {
    return undefined;
  }

  return observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (!listeners.has(entry.target)) {
        return;
      }

      var cb = listeners.get(entry.target);

      if (entry.isIntersecting || entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);
        listeners.delete(entry.target);
        cb();
      }
    });
  }, {
    rootMargin: '200px'
  });
}

var listenToIntersections = (el, cb) => {
  var observer = getObserver();

  if (!observer) {
    return () => {};
  }

  observer.observe(el);
  listeners.set(el, cb);
  return () => {
    try {
      observer.unobserve(el);
    } catch (err) {
      console.error(err);
    }

    listeners.delete(el);
  };
};

class Link extends _react.Component {
  constructor(props) {
    super(props);
    this.p = void 0;

    this.cleanUpListeners = () => {};

    this.formatUrls = memoizedFormatUrl((href, asHref) => {
      return {
        href: formatUrl(href),
        as: asHref ? formatUrl(asHref) : asHref
      };
    });

    this.linkClicked = e => {
      // @ts-ignore target exists on currentTarget
      var {
        nodeName,
        target
      } = e.currentTarget;

      if (nodeName === 'A' && (target && target !== '_self' || e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      var {
        href,
        as
      } = this.formatUrls(this.props.href, this.props.as);

      if (!isLocal(href)) {
        // ignore click if it's outside our scope (e.g. https://google.com)
        return;
      }

      var {
        pathname
      } = window.location;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      var {
        scroll
      } = this.props;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      _router.default[this.props.replace ? 'replace' : 'push'](href, as, {
        shallow: this.props.shallow
      }).then(success => {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      });
    };

    if (true) {
      if (props.prefetch) {
        console.warn('Next.js auto-prefetches automatically based on viewport. The prefetch attribute is no longer needed. More: https://err.sh/zeit/next.js/prefetch-true-deprecated');
      }
    }

    this.p = props.prefetch !== false;
  }

  componentWillUnmount() {
    this.cleanUpListeners();
  }

  handleRef(ref) {
    if (this.p && IntersectionObserver && ref && ref.tagName) {
      this.cleanUpListeners();
      this.cleanUpListeners = listenToIntersections(ref, () => {
        this.prefetch();
      });
    }
  } // The function is memoized so that no extra lifecycles are needed
  // as per https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html


  prefetch() {
    if (!this.p || true) return; // Prefetch the JSON page if asked (only in the client)

    var {
      pathname
    } = window.location;
    var {
      href: parsedHref
    } = this.formatUrls(this.props.href, this.props.as);
    var href = (0, _url.resolve)(pathname, parsedHref);

    _router.default.prefetch(href);
  }

  render() {
    var {
      children
    } = this.props;
    var {
      href,
      as
    } = this.formatUrls(this.props.href, this.props.as); // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

    if (typeof children === 'string') {
      children = _react.default.createElement("a", null, children);
    } // This will return the first child, if multiple are provided it will throw an error


    var child = _react.Children.only(children);

    var props = {
      ref: el => {
        this.handleRef(el);

        if (child && typeof child === 'object' && child.ref) {
          if (typeof child.ref === 'function') child.ref(el);else if (typeof child.ref === 'object') {
            child.ref.current = el;
          }
        }
      },
      onMouseEnter: e => {
        if (child.props && typeof child.props.onMouseEnter === 'function') {
          child.props.onMouseEnter(e);
        }

        this.prefetch();
      },
      onClick: e => {
        if (child.props && typeof child.props.onClick === 'function') {
          child.props.onClick(e);
        }

        if (!e.defaultPrevented) {
          this.linkClicked(e);
        }
      }
    }; // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
    // defined, we specify the current 'href', so that repetition is not needed by the user

    if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
      props.href = as || href;
    } // Add the ending slash to the paths. So, we can serve the
    // "<page>/index.html" directly.


    if (false) {}

    return _react.default.cloneElement(child, props);
  }

}

Link.propTypes = void 0;

if (true) {
  var warn = (0, _utils.execOnce)(console.error); // This module gets removed by webpack.IgnorePlugin

  var exact = __webpack_require__(/*! prop-types-exact */ "prop-types-exact");

  Link.propTypes = exact({
    href: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]).isRequired,
    as: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]),
    prefetch: _propTypes.default.bool,
    replace: _propTypes.default.bool,
    shallow: _propTypes.default.bool,
    passHref: _propTypes.default.bool,
    scroll: _propTypes.default.bool,
    children: _propTypes.default.oneOfType([_propTypes.default.element, (props, propName) => {
      var value = props[propName];

      if (typeof value === 'string') {
        warn("Warning: You're using a string directly inside <Link>. This usage has been deprecated. Please add an <a> tag as child of <Link>");
      }

      return null;
    }]).isRequired
  });
}

var _default = Link;
exports.default = _default;

/***/ }),

/***/ "./node_modules/next/dist/client/router.js":
/*!*************************************************!*\
  !*** ./node_modules/next/dist/client/router.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireWildcard */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _extends2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/helpers/extends */ "./node_modules/@babel/runtime-corejs2/helpers/extends.js"));

var _defineProperty = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js"));

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router2 = _interopRequireWildcard(__webpack_require__(/*! ../next-server/lib/router/router */ "./node_modules/next/dist/next-server/lib/router/router.js"));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__(/*! ../next-server/lib/router-context */ "./node_modules/next/dist/next-server/lib/router-context.js");

var _withRouter = _interopRequireDefault(__webpack_require__(/*! ./with-router */ "./node_modules/next/dist/client/with-router.js"));

exports.withRouter = _withRouter.default;
/* global window */

var singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

var urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components'];
var routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
var coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

(0, _defineProperty.default)(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  (0, _defineProperty.default)(singletonRouter, field, {
    get() {
      var router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = function () {
    var router = getRouter();
    return router[field](...arguments);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, function () {
      var eventField = "on" + event.charAt(0).toUpperCase() + event.substring(1);
      var _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...arguments);
        } catch (err) {
          // tslint:disable-next-line:no-console
          console.error("Error when running the Router event: " + eventField); // tslint:disable-next-line:no-console

          console.error(err.message + "\n" + err.stack);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    var message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


var createRouter = function createRouter() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  var _router = router;
  var instance = {};

  for (var property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = (0, _extends2.default)({}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = function () {
      return _router[field](...arguments);
    };
  });
  return instance;
}

/***/ }),

/***/ "./node_modules/next/dist/client/with-router.js":
/*!******************************************************!*\
  !*** ./node_modules/next/dist/client/with-router.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.default = withRouter;

var _extends2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/helpers/extends */ "./node_modules/@babel/runtime-corejs2/helpers/extends.js"));

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router = __webpack_require__(/*! ./router */ "./node_modules/next/dist/client/router.js");

function withRouter(ComposedComponent) {
  function WithRouterWrapper(props) {
    return _react.default.createElement(ComposedComponent, (0, _extends2.default)({
      router: (0, _router.useRouter)()
    }, props));
  }

  WithRouterWrapper.getInitialProps = ComposedComponent.getInitialProps // This is needed to allow checking for custom getInitialProps in _app
  ;
  WithRouterWrapper.origGetInitialProps = ComposedComponent.origGetInitialProps;

  if (true) {
    var name = ComposedComponent.displayName || ComposedComponent.name || 'Unknown';
    WithRouterWrapper.displayName = "withRouter(" + name + ")";
  }

  return WithRouterWrapper;
}

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/mitt.js":
/*!********************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/mitt.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
MIT License

Copyright (c) Jason Miller (https://jasonformat.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

var _Object$create = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/create */ "./node_modules/@babel/runtime-corejs2/core-js/object/create.js");

var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

function mitt() {
  const all = _Object$create(null);

  return {
    on(type, handler) {
      ;
      (all[type] || (all[type] = [])).push(handler);
    },

    off(type, handler) {
      if (all[type]) {
        // tslint:disable-next-line:no-bitwise
        all[type].splice(all[type].indexOf(handler) >>> 0, 1);
      }
    },

    emit(type, ...evts) {
      // eslint-disable-next-line array-callback-return
      ;
      (all[type] || []).slice().map(handler => {
        handler(...evts);
      });
    }

  };
}

exports.default = mitt;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router-context.js":
/*!******************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router-context.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  result["default"] = mod;
  return result;
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const React = __importStar(__webpack_require__(/*! react */ "react"));

exports.RouterContext = React.createContext(null);

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/rewrite-url-for-export.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/rewrite-url-for-export.js ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

function rewriteUrlForNextExport(url) {
  const [pathname, hash] = url.split('#'); // tslint:disable-next-line

  let [path, qs] = pathname.split('?');
  path = path.replace(/\/$/, ''); // Append a trailing slash if this path does not have an extension

  if (!/\.[^/]+\/?$/.test(path)) path += `/`;
  if (qs) path += '?' + qs;
  if (hash) path += '#' + hash;
  return path;
}

exports.rewriteUrlForNextExport = rewriteUrlForNextExport;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/router.js":
/*!*****************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/router.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Promise = __webpack_require__(/*! @babel/runtime-corejs2/core-js/promise */ "./node_modules/@babel/runtime-corejs2/core-js/promise.js");

var _Object$assign = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/assign */ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js");

var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");

const mitt_1 = __importDefault(__webpack_require__(/*! ../mitt */ "./node_modules/next/dist/next-server/lib/mitt.js"));

const utils_1 = __webpack_require__(/*! ../utils */ "./node_modules/next/dist/next-server/lib/utils.js");

const rewrite_url_for_export_1 = __webpack_require__(/*! ./rewrite-url-for-export */ "./node_modules/next/dist/next-server/lib/router/rewrite-url-for-export.js");

const is_dynamic_1 = __webpack_require__(/*! ./utils/is-dynamic */ "./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js");

const route_matcher_1 = __webpack_require__(/*! ./utils/route-matcher */ "./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js");

const route_regex_1 = __webpack_require__(/*! ./utils/route-regex */ "./node_modules/next/dist/next-server/lib/router/utils/route-regex.js");

function toRoute(path) {
  return path.replace(/\/$/, '') || '/';
}

class Router {
  constructor(pathname, query, as, {
    initialProps,
    pageLoader,
    App,
    wrapApp,
    Component,
    err,
    subscription
  }) {
    this.onPopState = e => {
      if (!e.state) {
        // We get state as undefined for two reasons.
        //  1. With older safari (< 8) and older chrome (< 34)
        //  2. When the URL changed with #
        //
        // In the both cases, we don't need to proceed and change the route.
        // (as it's already changed)
        // But we can simply replace the state with the new changes.
        // Actually, for (1) we don't need to nothing. But it's hard to detect that event.
        // So, doing the following for (1) does no harm.
        const {
          pathname,
          query
        } = this;
        this.changeState('replaceState', utils_1.formatWithValidation({
          pathname,
          query
        }), utils_1.getURL());
        return;
      } // Make sure we don't re-render on initial load,
      // can be caused by navigating back from an external site


      if (e.state && this.isSsr && e.state.url === this.pathname && e.state.as === this.asPath) {
        return;
      } // If the downstream application returns falsy, return.
      // They will then be responsible for handling the event.


      if (this._bps && !this._bps(e.state)) {
        return;
      }

      const {
        url,
        as,
        options
      } = e.state;

      if (true) {
        if (typeof url === 'undefined' || typeof as === 'undefined') {
          console.warn('`popstate` event triggered but `event.state` did not have `url` or `as` https://err.sh/zeit/next.js/popstate-state-empty');
        }
      }

      this.replace(url, as, options);
    }; // represents the current component key


    this.route = toRoute(pathname); // set up the component cache (by route keys)

    this.components = {}; // We should not keep the cache, if there's an error
    // Otherwise, this cause issues when when going back and
    // come again to the errored page.

    if (pathname !== '/_error') {
      this.components[this.route] = {
        Component,
        props: initialProps,
        err
      };
    }

    this.components['/_app'] = {
      Component: App
    }; // Backwards compat for Router.router.events
    // TODO: Should be remove the following major version as it was never documented
    // @ts-ignore backwards compatibility

    this.events = Router.events;
    this.pageLoader = pageLoader;
    this.pathname = pathname;
    this.query = query; // if auto prerendered and dynamic route wait to update asPath
    // until after mount to prevent hydration mismatch

    this.asPath = // @ts-ignore this is temporarily global (attached to window)
    is_dynamic_1.isDynamicRoute(pathname) && __NEXT_DATA__.autoExport ? pathname : as;
    this.sub = subscription;
    this.clc = null;
    this._wrapApp = wrapApp; // make sure to ignore extra popState in safari on navigating
    // back from external site

    this.isSsr = true;

    if (false) {}
  } // @deprecated backwards compatibility even though it's a private method.


  static _rewriteUrlForNextExport(url) {
    return rewrite_url_for_export_1.rewriteUrlForNextExport(url);
  }

  update(route, mod) {
    const Component = mod.default || mod;
    const data = this.components[route];

    if (!data) {
      throw new Error(`Cannot update unavailable route: ${route}`);
    }

    const newData = _Object$assign({}, data, {
      Component
    });

    this.components[route] = newData; // pages/_app.js updated

    if (route === '/_app') {
      this.notify(this.components[this.route]);
      return;
    }

    if (route === this.route) {
      this.notify(newData);
    }
  }

  reload() {
    window.location.reload();
  }
  /**
   * Go back in history
   */


  back() {
    window.history.back();
  }
  /**
   * Performs a `pushState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  push(url, as = url, options = {}) {
    return this.change('pushState', url, as, options);
  }
  /**
   * Performs a `replaceState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  replace(url, as = url, options = {}) {
    return this.change('replaceState', url, as, options);
  }

  change(method, _url, _as, options) {
    return new _Promise((resolve, reject) => {
      if (!options._h) {
        this.isSsr = false;
      } // marking route changes as a navigation start entry


      if (utils_1.SUPPORTS_PERFORMANCE_USER_TIMING) {
        performance.mark('routeChange');
      } // If url and as provided as an object representation,
      // we'll format them into the string version here.


      const url = typeof _url === 'object' ? utils_1.formatWithValidation(_url) : _url;
      let as = typeof _as === 'object' ? utils_1.formatWithValidation(_as) : _as; // Add the ending slash to the paths. So, we can serve the
      // "<page>/index.html" directly for the SSR page.

      if (false) {}

      this.abortComponentLoad(as); // If the url change is only related to a hash change
      // We should not proceed. We should only change the state.
      // WARNING: `_h` is an internal option for handing Next.js client-side
      // hydration. Your app should _never_ use this property. It may change at
      // any time without notice.

      if (!options._h && this.onlyAHashChange(as)) {
        this.asPath = as;
        Router.events.emit('hashChangeStart', as);
        this.changeState(method, url, as);
        this.scrollToHash(as);
        Router.events.emit('hashChangeComplete', as);
        return resolve(true);
      }

      const {
        pathname,
        query,
        protocol
      } = url_1.parse(url, true);

      if (!pathname || protocol) {
        if (true) {
          throw new Error(`Invalid href passed to router: ${url} https://err.sh/zeit/next.js/invalid-href-passed`);
        }

        return resolve(false);
      } // If asked to change the current URL we should reload the current page
      // (not location.reload() but reload getInitialProps and other Next.js stuffs)
      // We also need to set the method = replaceState always
      // as this should not go into the history (That's how browsers work)
      // We should compare the new asPath to the current asPath, not the url


      if (!this.urlIsNew(as)) {
        method = 'replaceState';
      } // @ts-ignore pathname is always a string


      const route = toRoute(pathname);
      const {
        shallow = false
      } = options;

      if (is_dynamic_1.isDynamicRoute(route)) {
        const {
          pathname: asPathname
        } = url_1.parse(as);
        const routeMatch = route_matcher_1.getRouteMatcher(route_regex_1.getRouteRegex(route))(asPathname);

        if (!routeMatch) {
          const error = `The provided \`as\` value (${asPathname}) is incompatible with the \`href\` value (${route}). ` + `Read more: https://err.sh/zeit/next.js/incompatible-href-as`;

          if (true) {
            throw new Error(error);
          } else {}

          return resolve(false);
        } // Merge params into `query`, overwriting any specified in search


        _Object$assign(query, routeMatch);
      }

      Router.events.emit('routeChangeStart', as); // If shallow is true and the route exists in the router cache we reuse the previous result
      // @ts-ignore pathname is always a string

      this.getRouteInfo(route, pathname, query, as, shallow).then(routeInfo => {
        const {
          error
        } = routeInfo;

        if (error && error.cancelled) {
          return resolve(false);
        }

        Router.events.emit('beforeHistoryChange', as);
        this.changeState(method, url, as, options);
        const hash = window.location.hash.substring(1);

        if (true) {
          const appComp = this.components['/_app'].Component;
          window.next.isPrerendered = appComp.getInitialProps === appComp.origGetInitialProps && !routeInfo.Component.getInitialProps;
        } // @ts-ignore pathname is always defined


        this.set(route, pathname, query, as, _Object$assign({}, routeInfo, {
          hash
        }));

        if (error) {
          Router.events.emit('routeChangeError', error, as);
          throw error;
        }

        Router.events.emit('routeChangeComplete', as);
        return resolve(true);
      }, reject);
    });
  }

  changeState(method, url, as, options = {}) {
    if (true) {
      if (typeof window.history === 'undefined') {
        console.error(`Warning: window.history is not available.`);
        return;
      } // @ts-ignore method should always exist on history


      if (typeof window.history[method] === 'undefined') {
        console.error(`Warning: window.history.${method} is not available`);
        return;
      }
    }

    if (method !== 'pushState' || utils_1.getURL() !== as) {
      // @ts-ignore method should always exist on history
      window.history[method]({
        url,
        as,
        options
      }, null, as);
    }
  }

  getRouteInfo(route, pathname, query, as, shallow = false) {
    const cachedRouteInfo = this.components[route]; // If there is a shallow route transition possible
    // If the route is already rendered on the screen.

    if (shallow && cachedRouteInfo && this.route === route) {
      return _Promise.resolve(cachedRouteInfo);
    }

    return new _Promise((resolve, reject) => {
      if (cachedRouteInfo) {
        return resolve(cachedRouteInfo);
      }

      this.fetchComponent(route).then(Component => resolve({
        Component
      }), reject);
    }).then(routeInfo => {
      const {
        Component
      } = routeInfo;

      if (true) {
        const {
          isValidElementType
        } = __webpack_require__(/*! react-is */ "react-is");

        if (!isValidElementType(Component)) {
          throw new Error(`The default export is not a React Component in page: "${pathname}"`);
        }
      }

      return new _Promise((resolve, reject) => {
        // we provide AppTree later so this needs to be `any`
        this.getInitialProps(Component, {
          pathname,
          query,
          asPath: as
        }).then(props => {
          routeInfo.props = props;
          this.components[route] = routeInfo;
          resolve(routeInfo);
        }, reject);
      });
    }).catch(err => {
      return new _Promise(resolve => {
        if (err.code === 'PAGE_LOAD_ERROR') {
          // If we can't load the page it could be one of following reasons
          //  1. Page doesn't exists
          //  2. Page does exist in a different zone
          //  3. Internal error while loading the page
          // So, doing a hard reload is the proper way to deal with this.
          window.location.href = as; // Changing the URL doesn't block executing the current code path.
          // So, we need to mark it as a cancelled error and stop the routing logic.

          err.cancelled = true; // @ts-ignore TODO: fix the control flow here

          return resolve({
            error: err
          });
        }

        if (err.cancelled) {
          // @ts-ignore TODO: fix the control flow here
          return resolve({
            error: err
          });
        }

        resolve(this.fetchComponent('/_error').then(Component => {
          const routeInfo = {
            Component,
            err
          };
          return new _Promise(resolve => {
            this.getInitialProps(Component, {
              err,
              pathname,
              query
            }).then(props => {
              routeInfo.props = props;
              routeInfo.error = err;
              resolve(routeInfo);
            }, gipErr => {
              console.error('Error in error page `getInitialProps`: ', gipErr);
              routeInfo.error = err;
              routeInfo.props = {};
              resolve(routeInfo);
            });
          });
        }));
      });
    });
  }

  set(route, pathname, query, as, data) {
    this.route = route;
    this.pathname = pathname;
    this.query = query;
    this.asPath = as;
    this.notify(data);
  }
  /**
   * Callback to execute before replacing router state
   * @param cb callback to be executed
   */


  beforePopState(cb) {
    this._bps = cb;
  }

  onlyAHashChange(as) {
    if (!this.asPath) return false;
    const [oldUrlNoHash, oldHash] = this.asPath.split('#');
    const [newUrlNoHash, newHash] = as.split('#'); // Makes sure we scroll to the provided hash if the url/hash are the same

    if (newHash && oldUrlNoHash === newUrlNoHash && oldHash === newHash) {
      return true;
    } // If the urls are change, there's more than a hash change


    if (oldUrlNoHash !== newUrlNoHash) {
      return false;
    } // If the hash has changed, then it's a hash only change.
    // This check is necessary to handle both the enter and
    // leave hash === '' cases. The identity case falls through
    // and is treated as a next reload.


    return oldHash !== newHash;
  }

  scrollToHash(as) {
    const [, hash] = as.split('#'); // Scroll to top if the hash is just `#` with no value

    if (hash === '') {
      window.scrollTo(0, 0);
      return;
    } // First we check if the element by id is found


    const idEl = document.getElementById(hash);

    if (idEl) {
      idEl.scrollIntoView();
      return;
    } // If there's no element with the id, we check the `name` property
    // To mirror browsers


    const nameEl = document.getElementsByName(hash)[0];

    if (nameEl) {
      nameEl.scrollIntoView();
    }
  }

  urlIsNew(asPath) {
    return this.asPath !== asPath;
  }
  /**
   * Prefetch `page` code, you may wait for the data during `page` rendering.
   * This feature only works in production!
   * @param url of prefetched `page`
   */


  prefetch(url) {
    return new _Promise((resolve, reject) => {
      const {
        pathname,
        protocol
      } = url_1.parse(url);

      if (!pathname || protocol) {
        if (true) {
          throw new Error(`Invalid href passed to router: ${url} https://err.sh/zeit/next.js/invalid-href-passed`);
        }

        return;
      } // Prefetch is not supported in development mode because it would trigger on-demand-entries


      if (true) return; // @ts-ignore pathname is always defined

      const route = toRoute(pathname);
      this.pageLoader.prefetch(route).then(resolve, reject);
    });
  }

  async fetchComponent(route) {
    let cancelled = false;

    const cancel = this.clc = () => {
      cancelled = true;
    };

    const Component = await this.pageLoader.loadPage(route);

    if (cancelled) {
      const error = new Error(`Abort fetching component for route: "${route}"`);
      error.cancelled = true;
      throw error;
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    return Component;
  }

  async getInitialProps(Component, ctx) {
    let cancelled = false;

    const cancel = () => {
      cancelled = true;
    };

    this.clc = cancel;
    const {
      Component: App
    } = this.components['/_app'];
    let props;

    if (Component.__NEXT_SPR) {
      let status; // pathname should have leading slash

      let {
        pathname
      } = url_1.parse(ctx.asPath || ctx.pathname);
      pathname = !pathname || pathname === '/' ? '/index' : pathname;
      props = await fetch( // @ts-ignore __NEXT_DATA__
      `/_next/data/${__NEXT_DATA__.buildId}${pathname}.json`).then(res => {
        if (!res.ok) {
          status = res.status;
          throw new Error('failed to load prerender data');
        }

        return res.json();
      }).catch(err => {
        console.error(`Failed to load data`, status, err);
        window.location.href = pathname;
        return new _Promise(() => {});
      });
    } else {
      const AppTree = this._wrapApp(App);

      ctx.AppTree = AppTree;
      props = await utils_1.loadGetInitialProps(App, {
        AppTree,
        Component,
        router: this,
        ctx
      });
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    if (cancelled) {
      const err = new Error('Loading initial props cancelled');
      err.cancelled = true;
      throw err;
    }

    return props;
  }

  abortComponentLoad(as) {
    if (this.clc) {
      const e = new Error('Route Cancelled');
      e.cancelled = true;
      Router.events.emit('routeChangeError', e, as);
      this.clc();
      this.clc = null;
    }
  }

  notify(data) {
    this.sub(data, this.components['/_app'].Component);
  }

}

Router.events = mitt_1.default();
exports.default = Router;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js":
/*!***************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
}); // Identify /[param]/ in route string


const TEST_ROUTE = /\/\[[^/]+?\](?=\/|$)/;

function isDynamicRoute(route) {
  return TEST_ROUTE.test(route);
}

exports.isDynamicRoute = isDynamicRoute;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js":
/*!******************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$keys = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/keys */ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js");

var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

function getRouteMatcher(routeRegex) {
  const {
    re,
    groups
  } = routeRegex;
  return pathname => {
    const routeMatch = re.exec(pathname);

    if (!routeMatch) {
      return false;
    }

    const decode = decodeURIComponent;
    const params = {};

    _Object$keys(groups).forEach(slugName => {
      const g = groups[slugName];
      const m = routeMatch[g.pos];

      if (m !== undefined) {
        params[slugName] = ~m.indexOf('/') ? m.split('/').map(entry => decode(entry)) : g.repeat ? [decode(m)] : decode(m);
      }
    });

    return params;
  };
}

exports.getRouteMatcher = getRouteMatcher;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/route-regex.js":
/*!****************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/route-regex.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

function getRouteRegex(normalizedRoute) {
  // Escape all characters that could be considered RegEx
  const escapedRoute = (normalizedRoute.replace(/\/$/, '') || '/').replace(/[|\\{}()[\]^$+*?.-]/g, '\\$&');
  const groups = {};
  let groupIndex = 1;
  const parameterizedRoute = escapedRoute.replace(/\/\\\[([^/]+?)\\\](?=\/|$)/g, (_, $1) => {
    const isCatchAll = /^(\\\.){3}/.test($1);
    groups[$1 // Un-escape key
    .replace(/\\([|\\{}()[\]^$+*?.-])/g, '$1').replace(/^\.{3}/, '') // eslint-disable-next-line no-sequences
    ] = {
      pos: groupIndex++,
      repeat: isCatchAll
    };
    return isCatchAll ? '/(.+?)' : '/([^/]+?)';
  });
  return {
    re: new RegExp('^' + parameterizedRoute + '(?:/)?$', 'i'),
    groups
  };
}

exports.getRouteRegex = getRouteRegex;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/utils.js":
/*!*********************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/utils.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$keys = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/keys */ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js");

var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  let result = null;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn.apply(this, args);
    }

    return result;
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(App, ctx) {
  if (true) {
    if (App.prototype && App.prototype.getInitialProps) {
      const message = `"${getDisplayName(App)}.getInitialProps()" is defined as an instance method - visit https://err.sh/zeit/next.js/get-initial-props-as-an-instance-method for more information.`;
      throw new Error(message);
    }
  } // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (true) {
    if (_Object$keys(props).length === 0 && !ctx.ctx) {
      console.warn(`${getDisplayName(App)} returned an empty object from \`getInitialProps\`. This de-optimizes and prevents automatic static optimization. https://err.sh/zeit/next.js/empty-object-getInitialProps`);
    }
  }

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (true) {
    if (url !== null && typeof url === 'object') {
      _Object$keys(url).forEach(key => {
        if (exports.urlObjectKeys.indexOf(key) === -1) {
          console.warn(`Unknown key passed via urlObject into url.format: ${key}`);
        }
      });
    }
  }

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SUPPORTS_PERFORMANCE = typeof performance !== 'undefined';
exports.SUPPORTS_PERFORMANCE_USER_TIMING = exports.SUPPORTS_PERFORMANCE && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "./node_modules/next/link.js":
/*!***********************************!*\
  !*** ./node_modules/next/link.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/client/link */ "./node_modules/next/dist/client/link.js")


/***/ }),

/***/ "./node_modules/swiper/css/swiper.css":
/*!********************************************!*\
  !*** ./node_modules/swiper/css/swiper.css ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./pages/aboutus/index.js":
/*!********************************!*\
  !*** ./pages/aboutus/index.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-animate-on-scroll */ "react-animate-on-scroll");
/* harmony import */ var react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _static_images_image2_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../static/images/image2.png */ "./static/images/image2.png");
/* harmony import */ var _static_images_image2_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_static_images_image2_png__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _static_images_productBox_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../static/images/productBox.png */ "./static/images/productBox.png");
/* harmony import */ var _static_images_productBox_png__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_static_images_productBox_png__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _static_images_calenderIcon_png__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../static/images/calenderIcon.png */ "./static/images/calenderIcon.png");
/* harmony import */ var _static_images_calenderIcon_png__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_static_images_calenderIcon_png__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _layout_primarylayout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../layout/primarylayout */ "./layout/primarylayout.jsx");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;








class Aboutus extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return __jsx("section", null, __jsx(next_head__WEBPACK_IMPORTED_MODULE_1___default.a, null, __jsx("title", null, "Aboutus")), __jsx(_layout_primarylayout__WEBPACK_IMPORTED_MODULE_6__["default"], null, __jsx("div", null, __jsx("div", {
      className: "aboutus-container d-flex"
    }, __jsx(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2___default.a, {
      animateIn: "slideInLeft",
      duration: 1,
      animateOnce: true
    }, __jsx("h4", {
      className: "aboutus-heading"
    }, "AboutUs"))), __jsx("div", {
      className: "row no-gutters"
    }, __jsx("div", {
      className: "col-lg-6 p-0"
    }, __jsx("div", {
      className: "bg-primarys"
    }, __jsx("img", {
      src: _static_images_image2_png__WEBPACK_IMPORTED_MODULE_3___default.a,
      alt: ""
    }))), __jsx("div", {
      className: "col-lg-6 p-0"
    }, __jsx("div", {
      className: "aboutus-sub-menu"
    }, __jsx("a", {
      className: "extra"
    }), __jsx("h4", {
      className: "aboutus-sub-heading py-2"
    }, "ABOUT", __jsx("span", {
      className: "Text-primary pl-1"
    }, "SAGICOR 365")), __jsx("div", {
      className: "d-flex flex-wrap"
    }, __jsx(react_animate_on_scroll__WEBPACK_IMPORTED_MODULE_2___default.a, {
      animateIn: "slideInRight",
      duration: 1.5,
      animateOnce: true
    }, __jsx("div", {
      className: "card aboutus-card my-sm-2 my-xs-2"
    }, __jsx("div", {
      className: "aboutus-body d-flex flex-row"
    }, __jsx("div", null, __jsx("img", {
      className: "aboutus-image-list",
      src: _static_images_productBox_png__WEBPACK_IMPORTED_MODULE_4___default.a,
      alt: ""
    })), __jsx("div", null, __jsx("p", {
      className: "aboutus-text mb-0"
    }, "Sagicor 365 is a partnership between Express Fitness and Sagicor to offer Sagicor customers the option to become members of Express Fitness gyms island-wide")))), __jsx("div", {
      className: "card aboutus-card my-sm-2 my-xs-2"
    }, __jsx("div", {
      className: "aboutus-body d-flex flex-row"
    }, __jsx("div", {
      className: "aboutus-body d-flex flex-row"
    }, __jsx("div", null, __jsx("img", {
      className: "aboutus-image-list",
      src: _static_images_calenderIcon_png__WEBPACK_IMPORTED_MODULE_5___default.a,
      alt: ""
    })), __jsx("div", null, __jsx("p", {
      className: "aboutus-text mb-0 pl-4"
    }, "Discounted price for a wide range of services and products."))))), __jsx("div", {
      className: "card aboutus-card my-sm-2 my-xs-2"
    }, __jsx("div", {
      className: "aboutus-body d-flex flex-row"
    }, __jsx("div", {
      className: "aboutus-body d-flex flex-row"
    }, __jsx("div", null, __jsx("img", {
      className: "aboutus-image-list",
      src: _static_images_productBox_png__WEBPACK_IMPORTED_MODULE_4___default.a,
      alt: ""
    })), __jsx("div", null, __jsx("p", {
      className: "aboutus-text mb-0"
    }, "In addition to discounted fees, customers would be able to benefit from various specialized health and fitness products which Express may offer periodically. "))))), __jsx("div", {
      className: "card aboutus-card my-sm-2 my-xs-2"
    }, __jsx("div", {
      className: "aboutus-body d-flex flex-row"
    }, __jsx("div", {
      className: "aboutus-body d-flex flex-row"
    }, __jsx("div", null, __jsx("img", {
      className: "aboutus-image-list",
      src: _static_images_calenderIcon_png__WEBPACK_IMPORTED_MODULE_5___default.a,
      alt: ""
    })), __jsx("div", null, __jsx("p", {
      className: "aboutus-text mb-0 pl-4"
    }, "The partnership aims to redefine health and fitness in Jamaica with offerings including discounted rates, exclusive facilities & events, and health company premium rebates.")))))))))))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Aboutus);

/***/ }),

/***/ "./static/images/Rectangle1.png":
/*!**************************************!*\
  !*** ./static/images/Rectangle1.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/Rectangle1-ecee68bb07ad1526bda1e32b683a8ffd.png";

/***/ }),

/***/ "./static/images/Rectangle2.png":
/*!**************************************!*\
  !*** ./static/images/Rectangle2.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/Rectangle2-db327ab0a44c60ee2a8e99df4820ab5c.png";

/***/ }),

/***/ "./static/images/Rectangle3.png":
/*!**************************************!*\
  !*** ./static/images/Rectangle3.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/Rectangle3-42d6b10523994ee73e98f8d4591bd67d.png";

/***/ }),

/***/ "./static/images/Rectangle4.png":
/*!**************************************!*\
  !*** ./static/images/Rectangle4.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/Rectangle4-69c667d777723cf1ef52a77beca15ce0.png";

/***/ }),

/***/ "./static/images/Rectangle5.png":
/*!**************************************!*\
  !*** ./static/images/Rectangle5.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/Rectangle5-3535a33bca6059a4c57db225782f882d.png";

/***/ }),

/***/ "./static/images/Rectangle6.png":
/*!**************************************!*\
  !*** ./static/images/Rectangle6.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/Rectangle6-82319746845e31755224662c0e9e7c2b.png";

/***/ }),

/***/ "./static/images/Rectangle7.png":
/*!**************************************!*\
  !*** ./static/images/Rectangle7.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/Rectangle7-00f05d1ef7c1bc8c5720fc4e7acfb21f.png";

/***/ }),

/***/ "./static/images/calenderIcon.png":
/*!****************************************!*\
  !*** ./static/images/calenderIcon.png ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAABGCAYAAAB7ckzWAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAieSURBVHgB7ZtpbF3FFcfP8/O+vdi14zghxTROS1tBRaqqpYLGFW2RCkFV+wFUUJW0VVupIHaEgAQLFEDsRIAQWxCINSIfCJsgoIiITWEVIDYTh0UgUBIncYz9HC/8/i/zgvPy3r33+Y236P2lq5l758xyzsycOXNmrlkRRRRRRBFFHNSImR80lJaW/lMhz/DQ0ND9hB+ZJ5SVlf1ydHR0CdHSWCy2dc+ePauIj9h0AIxfVVJSMszTS+MGCTfrs/lBJWVupsykK3+U+laYB5SYB9ArP+L5bGRkpI7GXcenRGNjY7V5QG1tbT1lthC9QuUT7qCu+eYBXpi3LNOHXhq1CQLMmw9EHZqlzLtlSP5oKi5T/TGNP9cKgqN53eNe9b2qp6fnLg1VJdv3wtkvn4tnft9Hq3hfX18NYTnJ+xoD2XGUfV9mPrUhHo+vHxwcXMd7r3lgfh6FrkOJHUW4nfdvXQNVcboXEoRbxzS6iucY0of5vh+/Ln0/jGHsANkoDaHHM+jnEnRkyVeDMlyGAF4eHh7+M993WCGgoAelxCoqKpZYjtUBmjX0RLfihFfybGtqaqozP5hN/X2UealeiPdQ3x25iBmh/4amF5q7wgoOnPOJRKIBaZ5AQbckk0kNpSiTLSUgesvXMjp2ZISCnr8T+jXUf0JLS0tNEG3gsIfhBEEdBXVZMHbxzEVIj0J7hEbK9u3bk+YB9fX1Q7t37+6nE06h/CMpv4p44HzWUsuwT5Cvlte+nHQWDPX0KJUGip5192oa9CLPYVS8E+Yv4rMX5nft2iU9s5JnB4yr/PWEN9lEo7Kysk1LFs//bQaBzriEDuivqalpCaKLus57m7+ThEiGQNRhbzMMkTorbJ1XITGUR2t5efnPLH+MNVomLR86oSkKcWAFzPlDsZa6ZajYzMNAdXV1Gxbi17kIIpm3KLx7CJ62/S20TIEcYJpmxPPJN5Y2KJ4r31/p/ZMsBJGGPT3/GoU9bDMEaPt2glDmIyk8m2GgoyLRHZRLHdM0UnvDmI9Zjp3YNIeXdT5VUJh5O15UVVV58chkgZeeT2lR1vmJ6Hntve8mLLMpQpRhPyFgNJ2MUP/A/vs08w+v5q13IbB8nq8QzbzM/MOfwvM97PEK/RHmD2fn9TbMH0vv/8qmAFOy1OEP1JbzC4RwIuE3CGK5+UWkzgqz8Pbt6qIaDmm0trZW481pJV896+58lNsCPs+B2Tnqbcrs7O/v/wJr7G5G1oW8344QtkHzOSNhCzTbyNeDN2k8Jz+Fd1aBzow6GHvUeWBH3YnONnf68jqHEbNFRO//GMZf4dsnYpb0kXQevj/S3t5eYXmCei+O4swIhGN+pABPTjkM3CoBEq6dBTo7O3NNtRjr/jzoNjphrero6BjXkZcv5g8V8wzD0238iNOYa9wZ23rey3PQNcP446oPugusgKEb1Y0VCJ8+PBq03PXomznSXyZtAEH/zwpE1J6ftF0dGv5ymHuOaFZfOgpuLg1+EsV4mxWI6birK0Wb/xYGX3Tvs+nlfzU0NCRSFcRim0jv8FHXtNvV4QNs1wGmND1Mn0n4MaPhzp07d37I+1mkfcDTwFT7oRUOL+t8qiCZt/mu85mA0d8pZE1fCZNa+9fR26v5dB5pNxBP+dqo5zcEn4aVxxK5ENrjdULEa7NOcqQzKOdTym+1QuG0vReFhwBvdgrvQ0bBqWOSYryfRtoWl94ZUs5JPM+k7QGdEPH+KvHnib+hQ01nJ+i4+l6EtMDGA5/anoY8SznLZfllS29sbNRoWIGmXpujiCbKeNgxLMtwBdNlkR04dWtJOw7a+yQYGVY8l1i+usTTOi/EZOBEIZQQsnw+3FmGMrjOziXATGBFNtP2BzRKEMi6tra2SouK6XBWx7D9CY3vpg1fMz2OyEYDg7+mp7WMZr0TQN4zJABoNljUFc6DeVso4jT6ferXEfjCnETx+KmaDjKPA2iWOSvz2vS3KEaOTZQPLwwwfZ7tHfJLCT+2AsAqsxo+ric8l9dFqfJD8kyIMyMidO9G9sA91L/WPIDjq5UI8iuEoOty0by3NjVYovUa5m81T3AXHVZhE3SgJ34RadjbFBxa0Dt/oZfewtbfZB6B0nxIIYI9floeWsje190+XSkzzxgYGNhCuR/Q+4vy3dXV69qZTEviJWjOm3g6lEB4EVI9RXGG1H+0hVVcfjrd2bG9ltzP5d2xvQJto6yH2HbOIV4na4z0nyoPx8rzYD5BA1+yCQA65D2Chfnu6nRFo46GpV1LuoGVNkpa+J42PrRt1UVE2eoNDN9mF1ePNuChiSMUOTWaseuVRxuHQ7TxEV06lHPTwqFT5EWWByhXFymDL1ukLTwZCTaJYAQc6Xx4J4aQ1mjddrSPWUTdpIuV8PRVGN0siLYG3XicIMyn3iGZsgE0rTC+0XXO9ZYHENazPBvDbhoNULC2jn/XJeLFixdv6u7u1o6pZMOGDebu1abickwynGPZ4vnQKt7V1TXY29srq00OkDVZ2nWYGk/aAgRwPnP4MsuDd3jR/wHvRCGepSGVdidP8qM9fiKjPbrTLxf3l0zLYy1PoIyPctPk5Kjrd5nT6sfYXu/raPpurTY+Lq4NUMrp4e7aZ41HzQfm0LNLdZA5ODh4/5i2VNCWM1GUuiYT6vTIhFYr3ddhlTnEpjFKEcZH9NK7xCNth8Og63TSJQjvOpvuoJF/gvkkvaUfigq1MuPyH2rKEC/czTUZEONuK3qOFQDKeULTinLy1hNTiTIavjr2/V9V+R5htZL/BbckXmAzEFJyNzr/3Rswo19IAqeB3GYoy6VyjOqXNOL/tZkMGPgHzHQ5IbymMznpBW1PddoryxDB/E3b4DSdHKf6bgcJEq5HX3B++gPsA1mlhE8hmN8HnfTOqMuFmdCOMJlMtsOsNlU/kL3O++ZEIvElJ0E9VkQRRWTiO0IHlC80EvgHAAAAAElFTkSuQmCC"

/***/ }),

/***/ "./static/images/footer-logo.png":
/*!***************************************!*\
  !*** ./static/images/footer-logo.png ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/footer-logo-40e1254c4dbb85719fb84c00f6c5f333.png";

/***/ }),

/***/ "./static/images/image2.png":
/*!**********************************!*\
  !*** ./static/images/image2.png ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/image2-a66d5463f52e85aef962c849c6bc838a.png";

/***/ }),

/***/ "./static/images/image3.png":
/*!**********************************!*\
  !*** ./static/images/image3.png ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/image3-37cbbf0cee8b29b825fd40a4089963f0.png";

/***/ }),

/***/ "./static/images/logo1.png":
/*!*********************************!*\
  !*** ./static/images/logo1.png ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/logo1-64e09c0e3e1261a673a3ea516fa6f112.png";

/***/ }),

/***/ "./static/images/logo2.png":
/*!*********************************!*\
  !*** ./static/images/logo2.png ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADkAAAAyCAYAAADm33NGAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAA3vSURBVHgB5VoJVFTX+b/3vTc7s7DMDDuiiIpE/ggapREEtTmxLjEqMS5Ne0yinhib2JiYf5s67Wkbu2ljmpjFGBO1GtSImhKjBjVVYimg4LBKBGQZYGCYGWZ98967vW+QBAwwg6jxtD/Pk8fMu9+9v/d999suAPwPAILvEQgBmF+3K7rGsE8uFgaE2FxNwMWYAcO4AKAoIKZUIEAcCViXqWtC+AqHgog2JscuMoNh4nshyZM7Urkpzm6vf9rONMywuTujIKAjIOp9ggPo5vIgJPE91aKSRNuEUFkUpkjOFRjQ0cxMHePvfPec5LGyl9NbHBfXdjsbF3HILuY4FlCkDJCElMbErBCwCFOUAY6WelgnEJASPIoFHGLxy+G8Kw4NmP7FhPAljz8Y+WSnP3NS4B7h2rU8URVz8dma9pwXacahxdoiKELMBgeMvaqQxORGSTMqSo3vFgg5FxoTvmaixVU1rctRn9VmLUkAgNEQWKMAUpgoAgZbYYbMFLMAi/3An7n7kSy58XF6WevH6WGy1NNyi7gsLW2jE9wBfFnxTlip69TLXxuPbICAgxTWhpAKtAdLk7ZEhM38MDNiTQcAe/qMWNuC/zutb8/ZXtsRkd5hq9rU0X11JolJQggBwXGUzV2fiAnjX7818sFA9P3Fg1zTW6wFv75uzf3HdWHJztOVv3kgJ2cpCUYIE1u9sbb9yAYC4kVhe0OQMo8K+tHGFSnPvN5DcGAkarJtjya8maegorMDJNEnWeT2EkJ4z8oEgXObu6uC/Jm/3578qHjRhDZL4QUEmCCGcwORINgtpaIvRijT3hqtTDqbGJVtAsPE6aqtC0tad+6EHB0GIeGdMFyZvmv55ANPD0dOYcPh0VdbPzjRYb+cQGDdBIijmmPE85PnTv5/o6+x/TQZLksTCAUqgt/gAlIKGNYu6nZfm1nbeXRfYevuw7n6nz+Vo38hiPeOwA8gpBca7EWvIs4VxpsZws6DIEQGbUDKXjBMTIle3DAqcPZvCSDE0zPA6ekECqlW4M/YfiSTgn9SpxSNaurhgG2ff/OQI1jOLG6zFmZWt+9/u9XyzzP7i7PX6BuPxfkSfrb2i0cM1oIUAvRYPDY3ECJN1MdL0vRgmMAvidXSc45o5UlXEPa2+JWFSKWBC/wZ24+kWq3uBqRwG0GK0K2qwp6QJ0263E3JeN/u/Gfj1qtHy9a/kVe+OXkgwQWN2yQ3rPkZLA4D8KYwAgoBg7izUVFpwzZ7HomJiXSAdNxfOATsHs4Br1tO+WVRxK0fTNWsuxAoGqNnkGfAAd59BVlgdV0XV3ccWF/RkZP/zldZb+SUrs8qqt8b1vucnJgY4qK75pCEsO9oIJeEC8EIkBz+yEWFKLaNZRlgMOtvj+SESPN1uXjM297AOyh4UyYBCUWAY20qs1O/vsWaf6Ki85MD56rfmoGQjrC7LeNZzj3RG9/6jAPcUHJ9Y5RqZoNMHP0JSUpgYIDPHePFd5IBCLPZS9d27HN6OrJauwsfpQgKr5IBgyVHkI9d+KeH6ZI2mc9mGG1l+RVGTbFUUnHezXT2G4SwdXgYRyCObyK8x9zgNsDHxdLWj3Rt5rSPGZK62j++DjJmsC92FbwQ5EQX8+3uhiQS+h8q0c1IhqDAg0cJEPg2xeQdj1qeejk5YsOiyeE/agD3CMRgXzyVtt0UH/TYqypxwtcszi/9TXN5J+PNSgDTjyAP3rxNtoqEBtPJ+eAeghjqy4cnbD4xTvvEWoVkbLuHc4E7AQ7RIoO5cOVJ/Ta/spU7gQHV89ei5ujGbrl0HpDXZmZCpqnr8v/llq9+xepueYyCBAVHWLtwOM6pZVMKNOLEVfMmbb0O7jIG1KTHLY+oMDhzdnYaf6jTISIyMPmKMijh+digh/fhGOrk0Mg8JJ+/dtiK0prsF177vPb1FD7RBncRAwp/6ZhR/nmrZ4/RjjKXTQz4sNGiePFQNmQbGwsklzp2Pdbhqnnb7KgMEBAi4LMEGAIc9rYigaZNKZmwXatM3zl37AYruAsYkCR+s8T2fzue+t1503YLB0QpYYJ8gqI2FDwRXM278JyiZ6JtntZnTe6K5xiuW0J4a4teDzwc2tD7OINcIFiWWh8dkLKlnaWP/Djpz3ZwBzGomRQ0Ismazzreq+ryrOCXrRKi9unhog+iZODDt+aFVPLPHLv8yg/anJefxwVuOoAeNZ+49+xXzp8p+gEhBhCkhAuSTKzQSMYfJKDiaOFhUKXT6Ua2N3yt4NnPu9IP6B0HTBwK57evFC8lRkVemqIlfz9aHnJSh53S8aJnpGZom2GnG1+1uxun0Ww3KcR57rf6HJ5B8yZMQjHAHr1UIRz9DkNL9q+c9saIzNjna/5pXse6I5X0Diu6mR3h96rAljkhhCx8QC16f/poz4HV49XdRUXPCEyKmIkGyxWdlb42zeE2avkMhyRur8PCeXNniDt22jqpKHSXRpm8Z+H4P7aA24BPkjvyronea1K8XtPFrHETsGcEVg7EpYBKRDApIWSRUiHaES+Fp16breg8q38zwMgWR9Cs50mcxD/icLcmsMAu5HNW4jYI8z0d7M1puXh0daTyoX0mo3HHTzP3DCto+7VhjlXZw3Mq3YcO1TjT6F6i3hXgW7wIpZBwh0qJKwnB1KEHQ4R5L6UHVAFwiMitr5CTDkuGy2NdbHbWpVtctaE4qRDxaaK3Yh1GwGWxZgVkACLJwHdT1Cv/OGPcRr/jq9+zfKh3RO8vdXyQ3+TOYsgBhmHNKnCHaoyKbNNIiHfHq7jDCWpN5ZpU6EEohyxtAqGt3RWzjK7y+Q7aOM3mvhHpYWzevmpPpcLLZH2sggAsR4MQecoVlfLBxUvG6fwiOqwgfLzaGvL5dfZdTHiRDcCBR/N+hkVAKyG4+CDy6zGB5EERYvcuS1Q3ZsZCr5np9Xphl6wwua4zb46DMcxx0J0PMLg64YALByISAB8aZjgGBErG3UgN+1l2auySf4E7SZLH+VpH1JZLjt8XtriXuLBf4AZbEMd3AgCvXUYugnWRcqogM0L4pckDP+20mLuXgkgagGwgS5io7qDLY1XCiEfbbeULLK5roQxnV/HNKr6MG9g7Q75jx4Yq0r9cNfngLF9tydtKp5YuzSGVT85anVvjeq2DBkGA8iEG9VxSErBaKWEKFMOSh0LJkikxksOrJkgvg5uLvGB8X25oKZ0KOPdzLbav5jrcjQK+7TKQZnExh02XYcepfzLrsUl/Og/uNMlerPrUlNhmYd8/d8M9laagTzP7BticKUwrREY4Y+Rkk0QE/05C+I8YCaravRD3mTD+VftRVr31i5/VW07NAoiTEQNold+f4aofVKVFPT8/TpNZO9h0I06Md1zqVBS2gl/mN7qy250oiiEIwu/BN9fM9xUlAsIcLEI34gKpkwlKcv+YuMDq5+LOsZ+UHXu0zVa4w0Y3h0Hg+Y4ACCX2cZrs1fMTtn482DT+L2gQbJgWbN27MOjlqVrh4tlRwjMy/BlkEM5a8OUr2+npfAIPvqweTlXXjSZdaPZsOt1A59bVm5ZnZxvR4qS/Hh4buHgzLs+ciGO+I4DhbLIWS4nW1zR3DGcRok7mW+dcNLg3XDcxWQYHEiISDn8WHI6kJHTGKKhtjz+l1ukgZA6Xrdta13niJfxlP2kMLuYjFRnnx3g2PZKWljbg2c2INdkXmXgxf5il/Gx2hP2J5YmiVXNjhWfDKKJV5E1bgP9pLHbLDg5IGruZFxuOd63lP0pSL90rl4yp5I/w+oJPKOx0G447hwYXB+4CdJmx5j9nBee8kgQWToki58+JFv42TgHKcc5LQ+x0er3tkMD6sjFA9JWB/kXuVUdUUyisDhZP3M3eShJToPG5TRP+NxjuCslePIQT9+NL1EWfPq7+lRKiqfPipc9nRVIlo0SA9e5XzgdTvLoqExtabmHWnf/1TE6tmFQmxAe2CPUdh/c+wWdMkUOJuTcoXhPh+Ps81c4zyzWpK5OIsSvGS14aH0yd0VLQQfJkezV8K/CePlrjjOBvxwUvxHFT5o2RveBbMTKhGlOMGnTue3bS3Iub2Ukdvv607Jhxv10GkyQCcnVJG5vRZEEhbhJTIPr4Fvy4GTtVHb7dAES4kiFvkYczDJa9MH36Czhl3DjgnPecZF8cXKjm60P++mznZeeoCqNz7YUGZtXlTk/4N1kUfiWhEhKsxLdu1ghuDSMEFDAqSfTXQ6V298xcfWFdsqT+jR8GbX58PDV9YZwgT8g3fvhgj/3M7Fhhy5YtAFV1nadp1u7t9vHgzzulgvAbkFBdHEr2fUOyF5tnBN14Ol69Mj2KukjSHIoPIZsTtPJtxcXFlMVel4obZ9/UoSyigUaeeoIKeqhhKJn3HUke8ybBrvhA8qO4ANKZESV+PXssNLbIzykN9oJZBD5q6OnycQAX0I6Y4JlvLYhY4BhK3ve6J4fCTE3wMYLsLPzbw6qqHFx0u8ryVpidNTMpPlHHBDnIeRJDl7/3YOSyGl+y7luS2anQgn/wF+D0Z0Z12Mo2EYjj/8YF9w88HoUo7s2YoEmv+iPrviXJg9dgUGVdVnHrm1sBoiPwaT6Oi4xHLNDuC+iW/zJRk+1XE/q+3JM8+L8cIcvP/fyqcU8OAM7JfP8H4iMTrWLartGStPU/fviU3132+1aTpYbyzHrTuVdoj0nFtzIDpfFlCirutWWp7x8Ew8R9S1JJaUo10sm73cLmbJU0MSdanbE77x19Jfhvgw7piObm41IwQvwHcy3zX+V6V60AAAAASUVORK5CYII="

/***/ }),

/***/ "./static/images/productBox.png":
/*!**************************************!*\
  !*** ./static/images/productBox.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEsAAABLCAYAAAA4TnrqAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAmjSURBVHgB7ZsJkBxVGcf/O7ubVQFBQIlBExQ1Wl4RjYqUJ14xeAEeaEUjaJUHWoqggmhSWl5VUas88EhZBik1RVRUPCCAruKF3AQI9xHCfV+BwO4O32/ee9uve3ey092zM7NL/6v+1bMz/V6/9/X3vuu9lSpUqFChQoUKFSpUeBRioL+//0C7nmn8l3G7Ftvtbvyf8d/GNxr7NIvB5PYxrjeOGeueP2+x7bFRm1Hjb43P0SzEYuMpfX19YbLwYa72HYL73BTtvxG12xp9hican69ZgPnGXymtSfcZv2ScZ4K6PPrutU36eHsk5HOMzzJ+23hX1OcW4w+NT9QMxJ7G1cYH5bSHCd1qPNq4c3TffPvtNrkJ3yJnl2I813iT//2KzO+7yglti9La+h3jUzQDwAS+quitmzCYDILbs0mbdyvRvDOMA1Ff5/vvHzIubdJ+oT1jnV0fDhpo15vterhxSD2Ixxi/osxbrtVqCGn3FtofqURgvzDWjL+J+vrIVB3MmTPn2XZZp/SSv9v4aeMO6gE8wfgBY1hKkKX3J+NLcvSDBpyoZCn9Nervx8qH1xhPVVpol/pxdkXTeChv+xql7cVf5MKDItjeeJnSnu6/xp2UH4Qarzf+P9Pf1cb3GPvVAfT7QZybGcQFxncpsTd5weTeZtyY6XeDf15RsJwPMRt2RdQnGkcwnEfzc+NVckKJJ3Ox8a0qh6W+nwkxmOeYTRYNe4WKY9B4iPFaJd7Zrg3NXaw24qVK2xB4jfFTKqdJexlPUmRbbBJM5ijjXHMOK+x6s9JRO0b/ZSqe6jzW+l0pF4bEQe5aNffWrXeutPHeZA87VM77FcVeJhSEPxr1i1C+aNwxcy9e7JvGO5UIFK1DaM9Ucezon3dv1O8NKplvsuY3KFnrb1ZxPMO42gY1PkC5eOxrmjq84K3/RD408cvoAeN3fb9FsTwaCzlr6eSc+OV2P0Ci6fn5mmueaePP7DoapS0InvRkV+UDkfkv5YLUIDQEjtDmKR/IDu7x47nDuEBtwjK5N0nHw2otyCOtIQeMczi0quwSAtgtwpTsUv6s8UkttCdMCR6deb1JbcaR0cB4k81UlmT2y5ponH9vfIHaW4fCSfxN6QD0euNhah6AEv4c57WSdp/QNAD7dWr0kI9lft/OG/84VuK+9QMDA6/T9BXr8Mjv0MTYj3Dk/XJOKsbK6J61mkaw/C5UUhYhsEMIHzRep8Sz1P19r1bnEALbDZm62UVKAmYC3BDDUXUt49VbAga/scR8XHSG0ppEirFM3Sv7Yo8OlssHY6GxXG/xnzfL1cU6AtR7JDOY66y2jpAep97AHDl7FF5sGOcDAwPFDHpNBWBCabjuzNdbR0dHWZoj6g1gyPF0jEn1+vhwR0ZGGpXZjmChkjCCuGtjVHTjSuVgqTqU2TfB/sZQso6T8hAQ4zHzxmW5sZuSsgdpEAYco/9Rpcsr2K5TjEvUOduFsd5PE8syZxs/LreK3qIkzBhW8dx2SjDp30WDOCrzOy768KimHnia8UWaRgwODr5cbvJxUn6lnJCyAfSKyH6t0TThiOghP1Bze4f2UYcfDyXkqqfHGV+o9oJI/g9Kl3TweJSSmy0zHFBcsl6uNoPlFN4aat2KPUJo35Pf4fHE+FMmXqByIEecbIuNCsXjW2i/i3yZxhSAisYr1SaQ1d/hB7RJ+XeBcQgk0iFprful+n3jk5UP9PUtpTWJsa1W/urDYhvH/Urqc6UNPgbwP0qMdpmEk2D2j0rbFbwTOedUdXbyTZb2eF3LuKVWa2jX01Ucy6L+/q6SwGg3NMLbK6JiAr0ygec+1he7PyNRv1fJbeFn+2VJrZTT6NjTktPtreKeFiXgJcVR/q1qg3fE03DiJY6CL7LAdP8Sg6XdvtbfOZl+cQofMg5ZUk44EtKToImcoCnjWXnucrk4K+6XWvzeahNIHd4gd9YgDgsQIqlP0QC0ZkJf4vuJ+42XG5r0T5WL2Rg/W3dnKV02Gjbuq2mKBQkXEM6VSk+OyZTZfWEyJL83ZvolCn+fSrwMud2nszP9svwOUIc2XdmJXpcZAG/qeLkd4SKgwpmtFLC/t72KAWd0snFrpmSD5+z4dv4q/3AOZMSlY5LsXyt/iLFWaUEF/lT5lgnV2D8rXVtjuyuECZ9RF7DKD4bKKLsvK+QTbD9AgkRO6y1soa+VmlxQ9MVEj2ihDww/WcJDSkfzHFiZqySjOExdwCovlEui754qd+wxDhrvNM9GZD23ST9UMUNtjOpqKA+zIRHOZqEVzbzVHnI7RfEWG1nDj5RkCuxIB2F1XbOy4C2vkdt6DxPAeCO0XcJNQ0NDaN1t0e8L7H52qesmYIS+SEnkT1Ujjvj5fIxPV+J0CsE9LTMeHEj3haW0ZmXBOYL1/txomBAlXTY2mCz18bBsD/BtGkcFTFhr/N94ydD+dOMO9hsB5b2x4bbPVDee12QcsWZ1Zxn6h2+c4j4GynnR05S2R7FTODrcbJM+2X93bGhvwvm60jYstGO5nyB3aGVbQLM2q0eX4WTAo71Tye5QIDvM4+Uefw4iLMMA0q7hTEWWAJOK7ODUj26PZpXKhWzQ1LZbdesMFC1Awzgz8Xk5j/lJuWXmbqrXG4IbGxuL62V42YPttxPsmffZlcoDZxMeVOvo8+OtqyBKCYvn8pZzPh+Dfbwngk41Dv2ZZpm8xuKfSLYX+WflnXBfgXFOQKHdnRg2AEKCoodc65P019BYTR6I1pVfUGQbh1q/wQsXzgPLCGuTv+5kk+PsAxsF+6lkUupVgGVYNrllA+Mg6w7bxnl57F4o9HUcCPrAUGaJSNLKNnqhJd7EwOfqQu6Q7XmZkIU8E6/c1bPxvDF2Ua5W4qkYJMerFyk/sqFDHlD5yNbIsHUHqcf+i4xYhoJdiGcCh5XjZHAmgm+pidyGA5rT0CQvKDZVJztF01PAoPJfXGGTI2T9TL6VTYGTmGwUwW8LVDbwqvG5C9ImIvydNYNAfkbkTSwVJsLnYzQxdxtHFMGv2UbfC+TKQOM7RnIv5wtyL2vGAsEQpcdpyu1y2jfZxse2DDynjKl2xJpE0ZG9yN00i7CHCQChxQdxKb1w7jReMpMZeASBcO9WIiS0Ci2d9gMe3cSLjf9QVL6x61X9/fqwfa5lDDwlZRLf2GmgSWylTev5iV4Dbv70TH2cg/ub/XdsisSHTPB0CKnMpuqMBm6f/8QfP7SbEV74jv8ZWqIKDZANvFfp/6kJ51NLp1CzFQS2lG6waRwd75XzqRUqVKhQoUKFChUq9AYeAcPwnEX2o96LAAAAAElFTkSuQmCC"

/***/ }),

/***/ 4:
/*!**************************************!*\
  !*** multi ./pages/aboutus/index.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\react\reduxfolder\sagicor-Next\pages\aboutus\index.js */"./pages/aboutus/index.js");


/***/ }),

/***/ "core-js/library/fn/map":
/*!*****************************************!*\
  !*** external "core-js/library/fn/map" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/map");

/***/ }),

/***/ "core-js/library/fn/object/assign":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/assign" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/assign");

/***/ }),

/***/ "core-js/library/fn/object/create":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/create" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/create");

/***/ }),

/***/ "core-js/library/fn/object/define-property":
/*!************************************************************!*\
  !*** external "core-js/library/fn/object/define-property" ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-property");

/***/ }),

/***/ "core-js/library/fn/object/get-own-property-descriptor":
/*!************************************************************************!*\
  !*** external "core-js/library/fn/object/get-own-property-descriptor" ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "core-js/library/fn/object/keys":
/*!*************************************************!*\
  !*** external "core-js/library/fn/object/keys" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/keys");

/***/ }),

/***/ "core-js/library/fn/promise":
/*!*********************************************!*\
  !*** external "core-js/library/fn/promise" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/promise");

/***/ }),

/***/ "core-js/library/fn/symbol":
/*!********************************************!*\
  !*** external "core-js/library/fn/symbol" ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/symbol");

/***/ }),

/***/ "core-js/library/fn/symbol/iterator":
/*!*****************************************************!*\
  !*** external "core-js/library/fn/symbol/iterator" ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/symbol/iterator");

/***/ }),

/***/ "core-js/library/fn/weak-map":
/*!**********************************************!*\
  !*** external "core-js/library/fn/weak-map" ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/weak-map");

/***/ }),

/***/ "google-map-react":
/*!***********************************!*\
  !*** external "google-map-react" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("google-map-react");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "prop-types-exact":
/*!***********************************!*\
  !*** external "prop-types-exact" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types-exact");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-animate-on-scroll":
/*!******************************************!*\
  !*** external "react-animate-on-scroll" ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-animate-on-scroll");

/***/ }),

/***/ "react-id-swiper":
/*!**********************************!*\
  !*** external "react-id-swiper" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-id-swiper");

/***/ }),

/***/ "react-is":
/*!***************************!*\
  !*** external "react-is" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-is");

/***/ }),

/***/ "reactstrap":
/*!*****************************!*\
  !*** external "reactstrap" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("reactstrap");

/***/ }),

/***/ "url":
/*!**********************!*\
  !*** external "url" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ })

/******/ });
//# sourceMappingURL=aboutus.js.map