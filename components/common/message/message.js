import React, { Component } from 'react'
import "./message.scss"
export class Message extends Component {
    render() {
        return (
            <section className="message">
                <div className="my-5">
                    <div className="message-heading">
                        <h4>Send Message</h4>
                    </div>
                    <div className="container">
                        <form>
                            <div className="form-row">
                                <div className="col">
                                    <input type="text" className="form-control" placeholder="Full name" />
                                </div>
                                <div className="col">
                                    <input type="text" className="form-control" placeholder="Email Address" />
                                </div>
                            </div>
                            <div className="form-group pt-4 pb-3">
                                <input type="text" className="form-control" placeholder="Subject" />
                            </div>
                            <div className="form-group">
                                <textarea className="form-control" rows="3" placeholder="Message"></textarea>
                            </div>
                        </form>
                        <div className="d-flex justify-content-center pt-4">
                            <button className="btn btn-black">SEND MESSAGE</button>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}