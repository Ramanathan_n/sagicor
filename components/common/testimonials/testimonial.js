import React, { Component } from 'react'
import "./testimonial.scss"
import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css'
const params = {
    effect: 'coverflow',
    grabCursor: true,
    centeredSlides: false,
    loop: true,
    coverflowEffect: {
        rotate: 0,
        stretch: 10,
        depth: 200,
        modifier: 1,
        slideShadows: false
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        640: {
            slidesPerView: 1,
        },
        768: {
            slidesPerView: 3,
        },
        1024: {
            slidesPerView: 3,
        },
    }
}
export class Testimonial extends Component {
    render() {
        return (
            <section className="testimonial">
                <div className="testimonial-main-page">
                    <div className="testimonial-heading">
                        <h3>Testimonials</h3>
                    </div>
                    <div className="testimonial-page">
                        <Swiper {...params}>
                            <div className="testimonial-card">
                                <h6 className="text-center"><i className="fas fa-quote-left p-3"></i></h6>
                                <p className="testimonial-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit eiusmod tempor incididunt ut labore dolore magna aliqua. Ut enim ad sit minim veniam nostrud exercitation ullamco laboris nisi ut commodo consequat.</p>
                                <p className="testimonial-person-name mb-0">John Doe</p>
                                <p className="testimonial-person-company-name">Amazon</p>
                            </div>
                            <div className="testimonial-card">
                                <h6 className="text-center"><i className="fas fa-quote-left p-3"></i></h6>
                                <p className="testimonial-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit eiusmod tempor incididunt ut labore dolore magna aliqua. Ut enim ad sit minim veniam nostrud exercitation ullamco laboris nisi ut commodo consequat.</p>
                                <p className="testimonial-person-name mb-0">John Doe</p>
                                <p className="testimonial-person-company-name">Amazon</p>
                            </div>
                            <div className="testimonial-card">
                                <h6 className="text-center"><i className="fas fa-quote-left p-3"></i></h6>
                                <p className="testimonial-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit eiusmod tempor incididunt ut labore dolore magna aliqua. Ut enim ad sit minim veniam nostrud exercitation ullamco laboris nisi ut commodo consequat.</p>
                                <p className="testimonial-person-name mb-0">John Doe</p>
                                <p className="testimonial-person-company-name">Amazon</p>
                            </div>
                        </Swiper>
                    </div>
                </div>
            </section>
        );
    }
}
