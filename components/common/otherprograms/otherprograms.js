import React, { Component } from 'react'
import "./otherprograms.scss"
import image1 from "../../../static/images/Rectangle1.png"
import image2 from "../../../static/images/Rectangle2.png"
import image3 from "../../../static/images/Rectangle3.png"

export class Otherprograms extends Component {
    render() {
        return (
            <section className="other-programs">
                <div className="my-3">
                    <div className="container">
                        <h4 className="text-center otherPrograms-heading py-4">Other Programs</h4>
                        <div className="card-deck ">
                            <div className="card otherprogram-card" id="card">
                                <img className="card-img-top" src={image1} alt="" />
                                <div className="white-box mx-auto">
                                    <p className="text-center py-4 px-2 mb-0"> 365 fitness</p>
                                </div>
                            </div>
                            <div className="card otherprogram-card"id="card">
                                <img className="card-img-top" src={image2} alt="" />
                                <div className="white-box mx-auto">
                                    <p className="text-center py-4 px-2 mb-0">nutritionisist services</p>
                                </div>
                            </div>
                            <div className="card otherprogram-card" id="card">
                                <img className="card-img-top" src={image3} alt="" />
                                <div className="white-box mx-auto">
                                    <p className="text-center py-4 px-2 mb-0">express events</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}