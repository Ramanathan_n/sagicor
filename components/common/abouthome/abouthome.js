import React, { Component } from 'react'
import "./abouthome.scss"
import ScrollAnimation from 'react-animate-on-scroll';
import aboutPicture from "../../../static/images/image3.png"
export class Abouthome extends Component {
    render() {
        return (
            <section className="about">
                <div className="my-5 py-3 about-container">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-7">
                                <ScrollAnimation animateIn="slideInLeft" duration={1} animateOnce={true}>
                                    <h3 className="about-text">About<span className="Text-primary pl-2">Sagicor 365</span></h3>
                                    <p className="about-para">Stay healthier with Jamaica’s leader in fitness</p>
                                    <p>Sagicor 365 is a partnership between Express Fitness and Sagicor to offer Sagicor customers the option to become members of Express Fitness gyms island-wide, for a discounted price. In addition to discounted fees, customers would be able to benefit from various specialized health and fitness products which Express may offer periodically.</p>
                                    <p className="py-3 mb-0"><button className="btn btn-blue">Know More</button></p>
                                </ScrollAnimation>
                            </div>
                            <div className="col-md-4 offset-md-1">
                                <ScrollAnimation animateIn="zoomIn" duration={1} animateOnce={true}>
                                    <img className="about-pic" src={aboutPicture} alt="" />
                                </ScrollAnimation>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
