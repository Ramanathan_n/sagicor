import React, { Component } from 'react'
import "./getintouch.scss"
import GoogleMapReact from 'google-map-react';


export class Getintouch extends Component {
    static defaultProps = {
        center: {
            lat: 13.0619,
            lng: 80.2468
        },
        zoom: 15
    };

    render() {
        return (
            <div className="getintouch-container">
                <div className="container">
                    <h4 className="getintouch-heading">Get in Touch</h4>
                    <div className="row">
                        <div className="col-md-5 p-3">
                            <div className="d-flex flex-row py-3">
                                <div className="pr-4 t-icons"><i className="fas fa-map-marker-alt"></i></div>
                                <div className="flex-column">
                                    <p>Location</p>
                                    <p className="getintouch-para">24, Park Streeet, Austin, Texas, Dallas</p>
                                </div>
                            </div>
                            <div className="d-flex flex-row py-3">
                                <div className="pr-3 t-icons"><i className="fas fa-envelope"></i></div>
                                <div className="flex-column">
                                    <p>Have any Questions?</p>
                                    <p className="getintouch-para">info@profinch.com</p>
                                </div>
                            </div>
                            <div className="d-flex flex-row py-3">
                                <div className="pr-3 t-icons"><i className="fas fa-phone-alt"></i></div>
                                <div className="flex-column">
                                    <p>Call Us</p>
                                    <p className="getintouch-para">080 4256 4256</p>
                                </div>
                            </div>
                        </div>
                        <div className="offset-md-1 col-md-6 col-xs-12 map">
                            <GoogleMapReact
                                bootstrapURLKeys={{
                                    key: "AIzaSyAcaQwH9WxvTZgDwXCKAT73mjbRrg0Gj7w"
                                }}
                                defaultCenter={this.props.center}
                                defaultZoom={this.props.zoom}>
                            </GoogleMapReact>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
