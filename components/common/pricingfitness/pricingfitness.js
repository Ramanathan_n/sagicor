import React, { Component } from "react";
import "./pricingfitness.scss"
export class Pricingfitness extends Component {
    render() {
        return (
            <section>
                <div className="bg-primarys">
                    <div className="container py-5">
                        <div className="pricing-heading">EXPRESS FITNESS</div>
                        <p className="pricing-para py-2">Current fee : $10000/person</p>
                        <div className="table-responsive">
                            <table className="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">TIER</th>
                                        <th scope="col">PARTICIPANTS</th>
                                        <th scope="col">FEE/MEMBER</th>
                                        <th scope="col">DISCOUNT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">I</th>
                                        <td>25-50 MEMBERS</td>
                                        <td>5000</td>
                                        <td>50%</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">II</th>
                                        <td>50-200 MEMBERS</td>
                                        <td>4750</td>
                                        <td>53%</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">III</th>
                                        <td>200-400 MEMBERS</td>
                                        <td>4500</td>
                                        <td>55%</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">IV</th>
                                        <td>200 MEMBERS</td>
                                        <td>4250</td>
                                        <td>58%</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="pricing-heading pt-5">365 FITNESS</div>
                        <p className="pricing-para">Current fee : $7000/person</p>
                        <div className="table-responsive">
                            <table className="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">TIER</th>
                                        <th scope="col">PARTICIPANTS</th>
                                        <th scope="col">FEE/MEMBER</th>
                                        <th scope="col">DISCOUNT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">I</th>
                                        <td>25-50 MEMBERS</td>
                                        <td>4000</td>
                                        <td>40%</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">II</th>
                                        <td>50-200 MEMBERS</td>
                                        <td>3750</td>
                                        <td>43%</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">III</th>
                                        <td>200-400 MEMBERS</td>
                                        <td>3500</td>
                                        <td>45%</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">IV</th>
                                        <td>200 MEMBERS</td>
                                        <td>3250</td>
                                        <td>48%</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}