import React, { Component } from 'react';
import "./banner.scss"
import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css'

const params = {
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
    },
    spaceBetween: 0
}
export class Banner extends Component {
    render() {
        return (
            <section className="banner">
                <div className="banner-card">
                    <Swiper {...params}>
                        <div>
                            <div className="banner-list d-flex zoom-in">
                                <div className="bannerText align-items-center slide-in-left">
                                    <h1 className="">Jamaica’s Largest Health and Wellness Platform </h1>
                                    <p className="py-2 "> Lorem ipsum dolor sit amet, consectetur adipiscing sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <p className="mb-0 py-3"><button className="btn btn-white">Join Now</button></p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="banner-list d-flex zoom-in">
                                <div className="bannerText mx-auto slide-in-down">
                                    <h1>Jamaica’s Largest Health and Wellness Platform </h1>
                                    <p className="py-2"> Lorem ipsum dolor sit amet, consectetur adipiscing sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <p className="py-3 mb-0"><button className="btn btn-white">Join Now</button></p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="banner-list d-flex zoom-in">
                                <div className="bannerText mx-auto slide-in-right">
                                    <h1>Jamaica’s Largest Health and Wellness Platform </h1>
                                    <p className="py-2"> Lorem ipsum dolor sit amet, consectetur adipiscing sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <p className="py-3 mb-0"><button className="btn btn-white">Join Now</button></p>
                                </div>
                            </div>
                        </div>
                    </Swiper>
                </div>
            </section>
        );
    }
}