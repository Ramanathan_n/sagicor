import React, { Component } from 'react';
import "./infoheader.scss"

export class Infoheader extends Component {
    render() {
        return (
            <section className="infoHeader">
                <div className="sc-head">
                    <div className="container">
                        <div className="row">
                            <div className=" row-head">
                                <i className="ml-1 faIcons fas fa-tablet-alt"></i><span className="px-2">123-456-7899</span>
                                <i className="ml-1 faIcons fas fa-print"></i><span className="pl-2">123-456-7896</span>
                            </div>
                            <div className="ml-auto">
                                <a href=""><i className="fab fa-facebook-f mx-1 faIcons"></i></a>
                                <a href=""><i className="fab fa-twitter faIcons mx-1"></i></a>
                                <a href=""><i className="fab fa-google-plus-g faIcons mx-1"></i></a>
                                <a href=""><i className="fab fa-linkedin-in faIcons ml-1"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

