import React, { useState } from 'react'
import "./header.scss"
import Link from 'next/link'
import logo from "../../../static/images/logo2.png"
import companyName from "../../../static/images/logo1.png"
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, } from 'reactstrap';

export const Header = () => {
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);
    return (
        <header>
            <div className="head">
                <div className="container py-0 px-0">
                    <Navbar light expand="lg">
                        <Link href="/" prefetch={false}><NavbarBrand className="m-0 nav-brand"><img src={companyName} width="140px" height="45px" /><img src={logo} width="50px" height="45px" /></NavbarBrand></Link>
                        <NavbarToggler onClick={toggle} />
                        <Collapse isOpen={isOpen} navbar>
                            <Nav className="ml-auto" navbar>
                                <NavItem className="nav-item active margin">
                                    <Link href="/aboutus" prefetch={false}><NavLink className="nav-link">About Us</NavLink></Link>
                                </NavItem>
                                <NavItem className="nav-item">
                                    <Link href="/features" prefetch={false}><NavLink className="nav-link">Features</NavLink></Link>
                                </NavItem>
                                <NavItem className="nav-item">
                                    <Link href="/pricing" prefetch={false}><NavLink className="nav-link">Pricing</NavLink></Link>
                                </NavItem>
                                <NavItem className="nav-item">
                                    <Link href="/contact" prefetch={false}><NavLink className="nav-link">Contact</NavLink></Link>
                                </NavItem>
                            </Nav>
                            <div className="header-button">
                                <button className="btn btn-blue ml-3">Log In</button>
                            </div>
                        </Collapse>
                    </Navbar>
                </div>
            </div>
        </header>
    );
}