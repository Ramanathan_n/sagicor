import React, { Component } from 'react'
import "./FeauturedProduct.scss"
import image1 from "../../../static/images/Rectangle4.png"
import image2 from "../../../static/images/Rectangle5.png"
import image3 from "../../../static/images/Rectangle6.png"
import image4 from "../../../static/images/Rectangle7.png"
export class Featuredproduct extends Component {
    render() {
        return (
            <section className="featured-products">
                <div className="mb-5">
                        <h3 className="text-center featured-products-heading py-4"><span className="Text-primary">Featured</span> Programs</h3>
                    <div className="card-group">
                        <div className="card card-product">
                            <div className="position-relative">
                                <img className="card-img-top" src={image1} alt="" />
                            </div>
                            <p className="fitness-catagory text-center">Express Fitness</p>
                            <p className=" button-transparent"><button className="btn btn-transparents">Join Now</button></p>
                        </div>
                        <div className="card card-product">
                            <div className="position-relative">
                                <img className="card-img-top" src={image2} alt="" />
                            </div>
                            <p className="fitness-catagory text-center">365 fitness</p>
                            <p className=" button-transparent"><button className="btn btn-transparents">Join Now</button></p>
                        </div>
                        <div className="card card-product">
                            <div className="position-relative">
                                <img className="card-img-top" src={image3} alt="" />
                            </div>
                            <p className="fitness-catagory text-center">Fitplex</p>
                            <p className=" button-transparent"><button className="btn btn-transparents">Join Now</button></p>
                        </div>
                        <div className="card card-product">
                            <div className="position-relative">
                                <img className="card-img-top" src={image4} alt="" />
                            </div>
                            <p className="fitness-catagory text-center">Fitbox</p>
                            <p className="button-transparent"><button className="btn btn-transparents">Join Now</button></p>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
