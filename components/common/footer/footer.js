import React, { Component } from 'react'
import "./footer.scss"
import Link from 'next/link'
import footerLogo from "../../../static/images/footer-logo.png"
export class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="footer-page">
                    <div className="footer-background">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-5">
                                    <div className="footer-logo">
                                        <img src={footerLogo} alt="" />
                                    </div>
                                    <p className="footer-para"> Sagicor 365 is a partnership between Express Fitness and Sagicor to offer Sagicor customers the option to become members of Express Fitness gyms island-wide.</p>
                                    <p className="footer-para"> 24, Street, New york, United states</p>
                                </div>
                                <div className="col-md-3"></div>
                                <div className="col-md-4">
                                    <div className="footer-links-heading"><h6>Useful Links</h6>
                                        <div className="footer-links-list">
                                            <li><Link href="/aboutus" prefetch={false}><a href="">About Us</a></Link></li>
                                            <li><Link href="/features" prefetch={false}><a href="">Features</a></Link></li>
                                            <li><Link href="/pricing" prefetch={false}><a href="">Pricing</a></Link></li>
                                            <li><Link href="/contact" prefetch={false}><a href="">Contact</a></Link></li>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div className="row pl-3 pr-4">
                                <p className="footer-copy-rights"> &copy; Copyright Sagicor 2019</p>
                                <div className="ml-auto">
                                    <a href=""><i className="fab fa-facebook-f mx-1 faIcons"></i></a>
                                    <a href=""><i className="fab fa-twitter faIcons mx-1"></i></a>
                                    <a href=""><i className="fab fa-google-plus-g faIcons mx-1"></i></a>
                                    <a href=""><i className="fab fa-linkedin-in faIcons mx-1"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

