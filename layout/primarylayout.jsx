import React from 'react'
import { Infoheader, Header, Footer } from '../components/common/'
import '../assets/scss/main.scss'

const Primarylayout = ({ children }) => (
  <>
      <div className="sticky">
        <Infoheader />
        <Header />
      </div>
    {children}
    <Footer />
  </>
)

export default Primarylayout
